package br.com.icmbio.samge.repository;

import br.com.icmbio.samge.domain.UnidadeConservacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UnidadeConservacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnidadeConservacaoRepository extends JpaRepository<UnidadeConservacao, Long> {}
