package br.com.icmbio.samge.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A UnidadeConservacao.
 */
@Entity
@Table(name = "__unidadeconservacao", schema = "samge")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UnidadeConservacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "categoriamanejoid")
    private Long categoriamanejoid;

    @Column(name = "categoriaiucnid")
    private Long categoriaiucnid;

    @NotNull
    @Column(name = "situacao", nullable = false)
    private String situacao;

    @NotNull
    @Column(name = "tipoesfera", nullable = false)
    private String tipoesfera;

    @NotNull
    @Column(name = "esfera", nullable = false)
    private String esfera;

    @NotNull
    @Column(name = "codigouc", nullable = false)
    private Long codigouc;

    @NotNull
    @Column(name = "cnuc", nullable = false)
    private String cnuc;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "ufs", nullable = false)
    private String ufs;

    @Column(name = "anocriacao")
    private Long anocriacao;

    @Column(name = "atolegalcriacao")
    private String atolegalcriacao;

    @Column(name = "outrosatoslegais")
    private String outrosatoslegais;

    @Column(name = "municipios")
    private String municipios;

    @Column(name = "templanomanejo")
    private String templanomanejo;

    @Column(name = "temconselhogestor")
    private String temconselhogestor;

    @Column(name = "fontearea")
    private String fontearea;

    @Column(name = "areabioma", precision = 21, scale = 2)
    private BigDecimal areabioma;

    @Column(name = "areaatolegalcriacao", precision = 21, scale = 2)
    private BigDecimal areaatolegalcriacao;

    @Column(name = "areaamazonia", precision = 21, scale = 2)
    private BigDecimal areaamazonia;

    @Column(name = "areacaatinga", precision = 21, scale = 2)
    private BigDecimal areacaatinga;

    @Column(name = "areacerrado", precision = 21, scale = 2)
    private BigDecimal areacerrado;

    @Column(name = "areamataatlantica", precision = 21, scale = 2)
    private BigDecimal areamataatlantica;

    @Column(name = "areapampa", precision = 21, scale = 2)
    private BigDecimal areapampa;

    @Column(name = "areapantanal", precision = 21, scale = 2)
    private BigDecimal areapantanal;

    @Column(name = "areamarinha", precision = 21, scale = 2)
    private BigDecimal areamarinha;

    @Column(name = "biomaid")
    private Long biomaid;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UnidadeConservacao id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoriamanejoid() {
        return this.categoriamanejoid;
    }

    public UnidadeConservacao categoriamanejoid(Long categoriamanejoid) {
        this.setCategoriamanejoid(categoriamanejoid);
        return this;
    }

    public void setCategoriamanejoid(Long categoriamanejoid) {
        this.categoriamanejoid = categoriamanejoid;
    }

    public Long getCategoriaiucnid() {
        return this.categoriaiucnid;
    }

    public UnidadeConservacao categoriaiucnid(Long categoriaiucnid) {
        this.setCategoriaiucnid(categoriaiucnid);
        return this;
    }

    public void setCategoriaiucnid(Long categoriaiucnid) {
        this.categoriaiucnid = categoriaiucnid;
    }

    public String getSituacao() {
        return this.situacao;
    }

    public UnidadeConservacao situacao(String situacao) {
        this.setSituacao(situacao);
        return this;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getTipoesfera() {
        return this.tipoesfera;
    }

    public UnidadeConservacao tipoesfera(String tipoesfera) {
        this.setTipoesfera(tipoesfera);
        return this;
    }

    public void setTipoesfera(String tipoesfera) {
        this.tipoesfera = tipoesfera;
    }

    public String getEsfera() {
        return this.esfera;
    }

    public UnidadeConservacao esfera(String esfera) {
        this.setEsfera(esfera);
        return this;
    }

    public void setEsfera(String esfera) {
        this.esfera = esfera;
    }

    public Long getCodigouc() {
        return this.codigouc;
    }

    public UnidadeConservacao codigouc(Long codigouc) {
        this.setCodigouc(codigouc);
        return this;
    }

    public void setCodigouc(Long codigouc) {
        this.codigouc = codigouc;
    }

    public String getCnuc() {
        return this.cnuc;
    }

    public UnidadeConservacao cnuc(String cnuc) {
        this.setCnuc(cnuc);
        return this;
    }

    public void setCnuc(String cnuc) {
        this.cnuc = cnuc;
    }

    public String getNome() {
        return this.nome;
    }

    public UnidadeConservacao nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUfs() {
        return this.ufs;
    }

    public UnidadeConservacao ufs(String ufs) {
        this.setUfs(ufs);
        return this;
    }

    public void setUfs(String ufs) {
        this.ufs = ufs;
    }

    public Long getAnocriacao() {
        return this.anocriacao;
    }

    public UnidadeConservacao anocriacao(Long anocriacao) {
        this.setAnocriacao(anocriacao);
        return this;
    }

    public void setAnocriacao(Long anocriacao) {
        this.anocriacao = anocriacao;
    }

    public String getAtolegalcriacao() {
        return this.atolegalcriacao;
    }

    public UnidadeConservacao atolegalcriacao(String atolegalcriacao) {
        this.setAtolegalcriacao(atolegalcriacao);
        return this;
    }

    public void setAtolegalcriacao(String atolegalcriacao) {
        this.atolegalcriacao = atolegalcriacao;
    }

    public String getOutrosatoslegais() {
        return this.outrosatoslegais;
    }

    public UnidadeConservacao outrosatoslegais(String outrosatoslegais) {
        this.setOutrosatoslegais(outrosatoslegais);
        return this;
    }

    public void setOutrosatoslegais(String outrosatoslegais) {
        this.outrosatoslegais = outrosatoslegais;
    }

    public String getMunicipios() {
        return this.municipios;
    }

    public UnidadeConservacao municipios(String municipios) {
        this.setMunicipios(municipios);
        return this;
    }

    public void setMunicipios(String municipios) {
        this.municipios = municipios;
    }

    public String getTemplanomanejo() {
        return this.templanomanejo;
    }

    public UnidadeConservacao templanomanejo(String templanomanejo) {
        this.setTemplanomanejo(templanomanejo);
        return this;
    }

    public void setTemplanomanejo(String templanomanejo) {
        this.templanomanejo = templanomanejo;
    }

    public String getTemconselhogestor() {
        return this.temconselhogestor;
    }

    public UnidadeConservacao temconselhogestor(String temconselhogestor) {
        this.setTemconselhogestor(temconselhogestor);
        return this;
    }

    public void setTemconselhogestor(String temconselhogestor) {
        this.temconselhogestor = temconselhogestor;
    }

    public String getFontearea() {
        return this.fontearea;
    }

    public UnidadeConservacao fontearea(String fontearea) {
        this.setFontearea(fontearea);
        return this;
    }

    public void setFontearea(String fontearea) {
        this.fontearea = fontearea;
    }

    public BigDecimal getAreabioma() {
        return this.areabioma;
    }

    public UnidadeConservacao areabioma(BigDecimal areabioma) {
        this.setAreabioma(areabioma);
        return this;
    }

    public void setAreabioma(BigDecimal areabioma) {
        this.areabioma = areabioma;
    }

    public BigDecimal getAreaatolegalcriacao() {
        return this.areaatolegalcriacao;
    }

    public UnidadeConservacao areaatolegalcriacao(BigDecimal areaatolegalcriacao) {
        this.setAreaatolegalcriacao(areaatolegalcriacao);
        return this;
    }

    public void setAreaatolegalcriacao(BigDecimal areaatolegalcriacao) {
        this.areaatolegalcriacao = areaatolegalcriacao;
    }

    public BigDecimal getAreaamazonia() {
        return this.areaamazonia;
    }

    public UnidadeConservacao areaamazonia(BigDecimal areaamazonia) {
        this.setAreaamazonia(areaamazonia);
        return this;
    }

    public void setAreaamazonia(BigDecimal areaamazonia) {
        this.areaamazonia = areaamazonia;
    }

    public BigDecimal getAreacaatinga() {
        return this.areacaatinga;
    }

    public UnidadeConservacao areacaatinga(BigDecimal areacaatinga) {
        this.setAreacaatinga(areacaatinga);
        return this;
    }

    public void setAreacaatinga(BigDecimal areacaatinga) {
        this.areacaatinga = areacaatinga;
    }

    public BigDecimal getAreacerrado() {
        return this.areacerrado;
    }

    public UnidadeConservacao areacerrado(BigDecimal areacerrado) {
        this.setAreacerrado(areacerrado);
        return this;
    }

    public void setAreacerrado(BigDecimal areacerrado) {
        this.areacerrado = areacerrado;
    }

    public BigDecimal getAreamataatlantica() {
        return this.areamataatlantica;
    }

    public UnidadeConservacao areamataatlantica(BigDecimal areamataatlantica) {
        this.setAreamataatlantica(areamataatlantica);
        return this;
    }

    public void setAreamataatlantica(BigDecimal areamataatlantica) {
        this.areamataatlantica = areamataatlantica;
    }

    public BigDecimal getAreapampa() {
        return this.areapampa;
    }

    public UnidadeConservacao areapampa(BigDecimal areapampa) {
        this.setAreapampa(areapampa);
        return this;
    }

    public void setAreapampa(BigDecimal areapampa) {
        this.areapampa = areapampa;
    }

    public BigDecimal getAreapantanal() {
        return this.areapantanal;
    }

    public UnidadeConservacao areapantanal(BigDecimal areapantanal) {
        this.setAreapantanal(areapantanal);
        return this;
    }

    public void setAreapantanal(BigDecimal areapantanal) {
        this.areapantanal = areapantanal;
    }

    public BigDecimal getAreamarinha() {
        return this.areamarinha;
    }

    public UnidadeConservacao areamarinha(BigDecimal areamarinha) {
        this.setAreamarinha(areamarinha);
        return this;
    }

    public void setAreamarinha(BigDecimal areamarinha) {
        this.areamarinha = areamarinha;
    }

    public Long getBiomaid() {
        return this.biomaid;
    }

    public UnidadeConservacao biomaid(Long biomaid) {
        this.setBiomaid(biomaid);
        return this;
    }

    public void setBiomaid(Long biomaid) {
        this.biomaid = biomaid;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UnidadeConservacao)) {
            return false;
        }
        return id != null && id.equals(((UnidadeConservacao) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UnidadeConservacao{" +
            "id=" + getId() +
            ", categoriamanejoid=" + getCategoriamanejoid() +
            ", categoriaiucnid=" + getCategoriaiucnid() +
            ", situacao='" + getSituacao() + "'" +
            ", tipoesfera='" + getTipoesfera() + "'" +
            ", esfera='" + getEsfera() + "'" +
            ", codigouc=" + getCodigouc() +
            ", cnuc='" + getCnuc() + "'" +
            ", nome='" + getNome() + "'" +
            ", ufs='" + getUfs() + "'" +
            ", anocriacao=" + getAnocriacao() +
            ", atolegalcriacao='" + getAtolegalcriacao() + "'" +
            ", outrosatoslegais='" + getOutrosatoslegais() + "'" +
            ", municipios='" + getMunicipios() + "'" +
            ", templanomanejo='" + getTemplanomanejo() + "'" +
            ", temconselhogestor='" + getTemconselhogestor() + "'" +
            ", fontearea='" + getFontearea() + "'" +
            ", areabioma=" + getAreabioma() +
            ", areaatolegalcriacao=" + getAreaatolegalcriacao() +
            ", areaamazonia=" + getAreaamazonia() +
            ", areacaatinga=" + getAreacaatinga() +
            ", areacerrado=" + getAreacerrado() +
            ", areamataatlantica=" + getAreamataatlantica() +
            ", areapampa=" + getAreapampa() +
            ", areapantanal=" + getAreapantanal() +
            ", areamarinha=" + getAreamarinha() +
            ", biomaid=" + getBiomaid() +
            "}";
    }
}
