package br.com.icmbio.samge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A GcUsuarioLogin.
 */
@Entity
@Table(name = "_gc_usuario_login", schema = "samge")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GcUsuarioLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "_gc_usuariologinid", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "datahora", nullable = false)
    private ZonedDateTime dataHora;

    @NotNull
    @Column(name = "ip", nullable = false)
    private String ip;

    @ManyToOne
    @JsonIgnoreProperties(value = { "gcUsuarioLogins" }, allowSetters = true)
    private GcUsuario gcUsuario;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public GcUsuarioLogin id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDataHora() {
        return this.dataHora;
    }

    public GcUsuarioLogin dataHora(ZonedDateTime dataHora) {
        this.setDataHora(dataHora);
        return this;
    }

    public void setDataHora(ZonedDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public String getIp() {
        return this.ip;
    }

    public GcUsuarioLogin ip(String ip) {
        this.setIp(ip);
        return this;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public GcUsuario getGcUsuario() {
        return this.gcUsuario;
    }

    public void setGcUsuario(GcUsuario gcUsuario) {
        this.gcUsuario = gcUsuario;
    }

    public GcUsuarioLogin gcUsuario(GcUsuario gcUsuario) {
        this.setGcUsuario(gcUsuario);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GcUsuarioLogin)) {
            return false;
        }
        return id != null && id.equals(((GcUsuarioLogin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GcUsuarioLogin{" +
            "id=" + getId() +
            ", dataHora='" + getDataHora() + "'" +
            ", ip='" + getIp() + "'" +
            "}";
    }
}
