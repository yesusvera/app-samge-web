package br.com.icmbio.samge.web.rest;

import br.com.icmbio.samge.domain.GcUsuarioLogin;
import br.com.icmbio.samge.repository.GcUsuarioLoginRepository;
import br.com.icmbio.samge.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.com.icmbio.samge.domain.GcUsuarioLogin}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GcUsuarioLoginResource {

    private final Logger log = LoggerFactory.getLogger(GcUsuarioLoginResource.class);

    private static final String ENTITY_NAME = "samgeGcUsuarioLogin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GcUsuarioLoginRepository gcUsuarioLoginRepository;

    public GcUsuarioLoginResource(GcUsuarioLoginRepository gcUsuarioLoginRepository) {
        this.gcUsuarioLoginRepository = gcUsuarioLoginRepository;
    }

    /**
     * {@code POST  /gc-usuario-logins} : Create a new gcUsuarioLogin.
     *
     * @param gcUsuarioLogin the gcUsuarioLogin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gcUsuarioLogin, or with status {@code 400 (Bad Request)} if the gcUsuarioLogin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gc-usuario-logins")
    public ResponseEntity<GcUsuarioLogin> createGcUsuarioLogin(@Valid @RequestBody GcUsuarioLogin gcUsuarioLogin)
        throws URISyntaxException {
        log.debug("REST request to save GcUsuarioLogin : {}", gcUsuarioLogin);
        if (gcUsuarioLogin.getId() != null) {
            throw new BadRequestAlertException("A new gcUsuarioLogin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GcUsuarioLogin result = gcUsuarioLoginRepository.save(gcUsuarioLogin);
        return ResponseEntity
            .created(new URI("/api/gc-usuario-logins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gc-usuario-logins/:id} : Updates an existing gcUsuarioLogin.
     *
     * @param id the id of the gcUsuarioLogin to save.
     * @param gcUsuarioLogin the gcUsuarioLogin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gcUsuarioLogin,
     * or with status {@code 400 (Bad Request)} if the gcUsuarioLogin is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gcUsuarioLogin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gc-usuario-logins/{id}")
    public ResponseEntity<GcUsuarioLogin> updateGcUsuarioLogin(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GcUsuarioLogin gcUsuarioLogin
    ) throws URISyntaxException {
        log.debug("REST request to update GcUsuarioLogin : {}, {}", id, gcUsuarioLogin);
        if (gcUsuarioLogin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gcUsuarioLogin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gcUsuarioLoginRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GcUsuarioLogin result = gcUsuarioLoginRepository.save(gcUsuarioLogin);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gcUsuarioLogin.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /gc-usuario-logins/:id} : Partial updates given fields of an existing gcUsuarioLogin, field will ignore if it is null
     *
     * @param id the id of the gcUsuarioLogin to save.
     * @param gcUsuarioLogin the gcUsuarioLogin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gcUsuarioLogin,
     * or with status {@code 400 (Bad Request)} if the gcUsuarioLogin is not valid,
     * or with status {@code 404 (Not Found)} if the gcUsuarioLogin is not found,
     * or with status {@code 500 (Internal Server Error)} if the gcUsuarioLogin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/gc-usuario-logins/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GcUsuarioLogin> partialUpdateGcUsuarioLogin(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GcUsuarioLogin gcUsuarioLogin
    ) throws URISyntaxException {
        log.debug("REST request to partial update GcUsuarioLogin partially : {}, {}", id, gcUsuarioLogin);
        if (gcUsuarioLogin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gcUsuarioLogin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gcUsuarioLoginRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GcUsuarioLogin> result = gcUsuarioLoginRepository
            .findById(gcUsuarioLogin.getId())
            .map(existingGcUsuarioLogin -> {
                if (gcUsuarioLogin.getDataHora() != null) {
                    existingGcUsuarioLogin.setDataHora(gcUsuarioLogin.getDataHora());
                }
                if (gcUsuarioLogin.getIp() != null) {
                    existingGcUsuarioLogin.setIp(gcUsuarioLogin.getIp());
                }

                return existingGcUsuarioLogin;
            })
            .map(gcUsuarioLoginRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gcUsuarioLogin.getId().toString())
        );
    }

    /**
     * {@code GET  /gc-usuario-logins} : get all the gcUsuarioLogins.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gcUsuarioLogins in body.
     */
    @GetMapping("/gc-usuario-logins")
    public List<GcUsuarioLogin> getAllGcUsuarioLogins() {
        log.debug("REST request to get all GcUsuarioLogins");
        return gcUsuarioLoginRepository.findAll();
    }

    /**
     * {@code GET  /gc-usuario-logins/:id} : get the "id" gcUsuarioLogin.
     *
     * @param id the id of the gcUsuarioLogin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gcUsuarioLogin, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gc-usuario-logins/{id}")
    public ResponseEntity<GcUsuarioLogin> getGcUsuarioLogin(@PathVariable Long id) {
        log.debug("REST request to get GcUsuarioLogin : {}", id);
        Optional<GcUsuarioLogin> gcUsuarioLogin = gcUsuarioLoginRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(gcUsuarioLogin);
    }

    /**
     * {@code DELETE  /gc-usuario-logins/:id} : delete the "id" gcUsuarioLogin.
     *
     * @param id the id of the gcUsuarioLogin to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gc-usuario-logins/{id}")
    public ResponseEntity<Void> deleteGcUsuarioLogin(@PathVariable Long id) {
        log.debug("REST request to delete GcUsuarioLogin : {}", id);
        gcUsuarioLoginRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
