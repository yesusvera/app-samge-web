package br.com.icmbio.samge.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.icmbio.samge.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GcUsuarioTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GcUsuario.class);
        GcUsuario gcUsuario1 = new GcUsuario();
        gcUsuario1.setId(1L);
        GcUsuario gcUsuario2 = new GcUsuario();
        gcUsuario2.setId(gcUsuario1.getId());
        assertThat(gcUsuario1).isEqualTo(gcUsuario2);
        gcUsuario2.setId(2L);
        assertThat(gcUsuario1).isNotEqualTo(gcUsuario2);
        gcUsuario1.setId(null);
        assertThat(gcUsuario1).isNotEqualTo(gcUsuario2);
    }
}
