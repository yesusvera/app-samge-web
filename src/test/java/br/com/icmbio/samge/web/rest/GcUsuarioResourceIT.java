package br.com.icmbio.samge.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.icmbio.samge.IntegrationTest;
import br.com.icmbio.samge.domain.GcUsuario;
import br.com.icmbio.samge.repository.GcUsuarioRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GcUsuarioResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GcUsuarioResourceIT {

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_SENHA = "AAAAAAAAAA";
    private static final String UPDATED_SENHA = "BBBBBBBBBB";

    private static final String DEFAULT_SITUACAO = "AAAAAAAAAA";
    private static final String UPDATED_SITUACAO = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN_RECUPERAR_SENHA = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_RECUPERAR_SENHA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/gc-usuarios";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GcUsuarioRepository gcUsuarioRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGcUsuarioMockMvc;

    private GcUsuario gcUsuario;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GcUsuario createEntity(EntityManager em) {
        GcUsuario gcUsuario = new GcUsuario()
            .login(DEFAULT_LOGIN)
            .email(DEFAULT_EMAIL)
            .senha(DEFAULT_SENHA)
            .situacao(DEFAULT_SITUACAO)
            .tokenRecuperarSenha(DEFAULT_TOKEN_RECUPERAR_SENHA);
        return gcUsuario;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GcUsuario createUpdatedEntity(EntityManager em) {
        GcUsuario gcUsuario = new GcUsuario()
            .login(UPDATED_LOGIN)
            .email(UPDATED_EMAIL)
            .senha(UPDATED_SENHA)
            .situacao(UPDATED_SITUACAO)
            .tokenRecuperarSenha(UPDATED_TOKEN_RECUPERAR_SENHA);
        return gcUsuario;
    }

    @BeforeEach
    public void initTest() {
        gcUsuario = createEntity(em);
    }

    @Test
    @Transactional
    void createGcUsuario() throws Exception {
        int databaseSizeBeforeCreate = gcUsuarioRepository.findAll().size();
        // Create the GcUsuario
        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isCreated());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeCreate + 1);
        GcUsuario testGcUsuario = gcUsuarioList.get(gcUsuarioList.size() - 1);
        assertThat(testGcUsuario.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testGcUsuario.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testGcUsuario.getSenha()).isEqualTo(DEFAULT_SENHA);
        assertThat(testGcUsuario.getSituacao()).isEqualTo(DEFAULT_SITUACAO);
        assertThat(testGcUsuario.getTokenRecuperarSenha()).isEqualTo(DEFAULT_TOKEN_RECUPERAR_SENHA);
    }

    @Test
    @Transactional
    void createGcUsuarioWithExistingId() throws Exception {
        // Create the GcUsuario with an existing ID
        gcUsuario.setId(1L);

        int databaseSizeBeforeCreate = gcUsuarioRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isBadRequest());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLoginIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioRepository.findAll().size();
        // set the field null
        gcUsuario.setLogin(null);

        // Create the GcUsuario, which fails.

        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isBadRequest());

        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioRepository.findAll().size();
        // set the field null
        gcUsuario.setEmail(null);

        // Create the GcUsuario, which fails.

        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isBadRequest());

        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSenhaIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioRepository.findAll().size();
        // set the field null
        gcUsuario.setSenha(null);

        // Create the GcUsuario, which fails.

        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isBadRequest());

        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSituacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioRepository.findAll().size();
        // set the field null
        gcUsuario.setSituacao(null);

        // Create the GcUsuario, which fails.

        restGcUsuarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isBadRequest());

        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGcUsuarios() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        // Get all the gcUsuarioList
        restGcUsuarioMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gcUsuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].senha").value(hasItem(DEFAULT_SENHA)))
            .andExpect(jsonPath("$.[*].situacao").value(hasItem(DEFAULT_SITUACAO)))
            .andExpect(jsonPath("$.[*].tokenRecuperarSenha").value(hasItem(DEFAULT_TOKEN_RECUPERAR_SENHA)));
    }

    @Test
    @Transactional
    void getGcUsuario() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        // Get the gcUsuario
        restGcUsuarioMockMvc
            .perform(get(ENTITY_API_URL_ID, gcUsuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gcUsuario.getId().intValue()))
            .andExpect(jsonPath("$.login").value(DEFAULT_LOGIN))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.senha").value(DEFAULT_SENHA))
            .andExpect(jsonPath("$.situacao").value(DEFAULT_SITUACAO))
            .andExpect(jsonPath("$.tokenRecuperarSenha").value(DEFAULT_TOKEN_RECUPERAR_SENHA));
    }

    @Test
    @Transactional
    void getNonExistingGcUsuario() throws Exception {
        // Get the gcUsuario
        restGcUsuarioMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingGcUsuario() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();

        // Update the gcUsuario
        GcUsuario updatedGcUsuario = gcUsuarioRepository.findById(gcUsuario.getId()).get();
        // Disconnect from session so that the updates on updatedGcUsuario are not directly saved in db
        em.detach(updatedGcUsuario);
        updatedGcUsuario
            .login(UPDATED_LOGIN)
            .email(UPDATED_EMAIL)
            .senha(UPDATED_SENHA)
            .situacao(UPDATED_SITUACAO)
            .tokenRecuperarSenha(UPDATED_TOKEN_RECUPERAR_SENHA);

        restGcUsuarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGcUsuario.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGcUsuario))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
        GcUsuario testGcUsuario = gcUsuarioList.get(gcUsuarioList.size() - 1);
        assertThat(testGcUsuario.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testGcUsuario.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testGcUsuario.getSenha()).isEqualTo(UPDATED_SENHA);
        assertThat(testGcUsuario.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testGcUsuario.getTokenRecuperarSenha()).isEqualTo(UPDATED_TOKEN_RECUPERAR_SENHA);
    }

    @Test
    @Transactional
    void putNonExistingGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, gcUsuario.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuario))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuario))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuario)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGcUsuarioWithPatch() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();

        // Update the gcUsuario using partial update
        GcUsuario partialUpdatedGcUsuario = new GcUsuario();
        partialUpdatedGcUsuario.setId(gcUsuario.getId());

        partialUpdatedGcUsuario.login(UPDATED_LOGIN).senha(UPDATED_SENHA).tokenRecuperarSenha(UPDATED_TOKEN_RECUPERAR_SENHA);

        restGcUsuarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGcUsuario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGcUsuario))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
        GcUsuario testGcUsuario = gcUsuarioList.get(gcUsuarioList.size() - 1);
        assertThat(testGcUsuario.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testGcUsuario.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testGcUsuario.getSenha()).isEqualTo(UPDATED_SENHA);
        assertThat(testGcUsuario.getSituacao()).isEqualTo(DEFAULT_SITUACAO);
        assertThat(testGcUsuario.getTokenRecuperarSenha()).isEqualTo(UPDATED_TOKEN_RECUPERAR_SENHA);
    }

    @Test
    @Transactional
    void fullUpdateGcUsuarioWithPatch() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();

        // Update the gcUsuario using partial update
        GcUsuario partialUpdatedGcUsuario = new GcUsuario();
        partialUpdatedGcUsuario.setId(gcUsuario.getId());

        partialUpdatedGcUsuario
            .login(UPDATED_LOGIN)
            .email(UPDATED_EMAIL)
            .senha(UPDATED_SENHA)
            .situacao(UPDATED_SITUACAO)
            .tokenRecuperarSenha(UPDATED_TOKEN_RECUPERAR_SENHA);

        restGcUsuarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGcUsuario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGcUsuario))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
        GcUsuario testGcUsuario = gcUsuarioList.get(gcUsuarioList.size() - 1);
        assertThat(testGcUsuario.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testGcUsuario.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testGcUsuario.getSenha()).isEqualTo(UPDATED_SENHA);
        assertThat(testGcUsuario.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testGcUsuario.getTokenRecuperarSenha()).isEqualTo(UPDATED_TOKEN_RECUPERAR_SENHA);
    }

    @Test
    @Transactional
    void patchNonExistingGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, gcUsuario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuario))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuario))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGcUsuario() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioRepository.findAll().size();
        gcUsuario.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(gcUsuario))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GcUsuario in the database
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGcUsuario() throws Exception {
        // Initialize the database
        gcUsuarioRepository.saveAndFlush(gcUsuario);

        int databaseSizeBeforeDelete = gcUsuarioRepository.findAll().size();

        // Delete the gcUsuario
        restGcUsuarioMockMvc
            .perform(delete(ENTITY_API_URL_ID, gcUsuario.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GcUsuario> gcUsuarioList = gcUsuarioRepository.findAll();
        assertThat(gcUsuarioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
