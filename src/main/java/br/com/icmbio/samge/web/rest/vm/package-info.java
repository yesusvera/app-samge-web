/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.icmbio.samge.web.rest.vm;
