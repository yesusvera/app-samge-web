package br.com.icmbio.samge.web.rest;

import br.com.icmbio.samge.domain.GcUsuario;
import br.com.icmbio.samge.repository.GcUsuarioRepository;
import br.com.icmbio.samge.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.com.icmbio.samge.domain.GcUsuario}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GcUsuarioResource {

    private final Logger log = LoggerFactory.getLogger(GcUsuarioResource.class);

    private static final String ENTITY_NAME = "samgeGcUsuario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GcUsuarioRepository gcUsuarioRepository;

    public GcUsuarioResource(GcUsuarioRepository gcUsuarioRepository) {
        this.gcUsuarioRepository = gcUsuarioRepository;
    }

    /**
     * {@code POST  /gc-usuarios} : Create a new gcUsuario.
     *
     * @param gcUsuario the gcUsuario to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gcUsuario, or with status {@code 400 (Bad Request)} if the gcUsuario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/gc-usuarios")
    public ResponseEntity<GcUsuario> createGcUsuario(@Valid @RequestBody GcUsuario gcUsuario) throws URISyntaxException {
        log.debug("REST request to save GcUsuario : {}", gcUsuario);
        if (gcUsuario.getId() != null) {
            throw new BadRequestAlertException("A new gcUsuario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GcUsuario result = gcUsuarioRepository.save(gcUsuario);
        return ResponseEntity
            .created(new URI("/api/gc-usuarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /gc-usuarios/:id} : Updates an existing gcUsuario.
     *
     * @param id the id of the gcUsuario to save.
     * @param gcUsuario the gcUsuario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gcUsuario,
     * or with status {@code 400 (Bad Request)} if the gcUsuario is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gcUsuario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/gc-usuarios/{id}")
    public ResponseEntity<GcUsuario> updateGcUsuario(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GcUsuario gcUsuario
    ) throws URISyntaxException {
        log.debug("REST request to update GcUsuario : {}, {}", id, gcUsuario);
        if (gcUsuario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gcUsuario.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gcUsuarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GcUsuario result = gcUsuarioRepository.save(gcUsuario);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gcUsuario.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /gc-usuarios/:id} : Partial updates given fields of an existing gcUsuario, field will ignore if it is null
     *
     * @param id the id of the gcUsuario to save.
     * @param gcUsuario the gcUsuario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gcUsuario,
     * or with status {@code 400 (Bad Request)} if the gcUsuario is not valid,
     * or with status {@code 404 (Not Found)} if the gcUsuario is not found,
     * or with status {@code 500 (Internal Server Error)} if the gcUsuario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/gc-usuarios/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GcUsuario> partialUpdateGcUsuario(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GcUsuario gcUsuario
    ) throws URISyntaxException {
        log.debug("REST request to partial update GcUsuario partially : {}, {}", id, gcUsuario);
        if (gcUsuario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, gcUsuario.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!gcUsuarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GcUsuario> result = gcUsuarioRepository
            .findById(gcUsuario.getId())
            .map(existingGcUsuario -> {
                if (gcUsuario.getLogin() != null) {
                    existingGcUsuario.setLogin(gcUsuario.getLogin());
                }
                if (gcUsuario.getEmail() != null) {
                    existingGcUsuario.setEmail(gcUsuario.getEmail());
                }
                if (gcUsuario.getSenha() != null) {
                    existingGcUsuario.setSenha(gcUsuario.getSenha());
                }
                if (gcUsuario.getSituacao() != null) {
                    existingGcUsuario.setSituacao(gcUsuario.getSituacao());
                }
                if (gcUsuario.getTokenRecuperarSenha() != null) {
                    existingGcUsuario.setTokenRecuperarSenha(gcUsuario.getTokenRecuperarSenha());
                }

                return existingGcUsuario;
            })
            .map(gcUsuarioRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gcUsuario.getId().toString())
        );
    }

    /**
     * {@code GET  /gc-usuarios} : get all the gcUsuarios.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gcUsuarios in body.
     */
    @GetMapping("/gc-usuarios")
    public List<GcUsuario> getAllGcUsuarios() {
        log.debug("REST request to get all GcUsuarios");
        return gcUsuarioRepository.findAll();
    }

    /**
     * {@code GET  /gc-usuarios/:id} : get the "id" gcUsuario.
     *
     * @param id the id of the gcUsuario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gcUsuario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/gc-usuarios/{id}")
    public ResponseEntity<GcUsuario> getGcUsuario(@PathVariable Long id) {
        log.debug("REST request to get GcUsuario : {}", id);
        Optional<GcUsuario> gcUsuario = gcUsuarioRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(gcUsuario);
    }

    /**
     * {@code DELETE  /gc-usuarios/:id} : delete the "id" gcUsuario.
     *
     * @param id the id of the gcUsuario to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/gc-usuarios/{id}")
    public ResponseEntity<Void> deleteGcUsuario(@PathVariable Long id) {
        log.debug("REST request to delete GcUsuario : {}", id);
        gcUsuarioRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
