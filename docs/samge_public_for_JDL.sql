CREATE TABLE public."Teste_BD_SAMGe" (
    id bigint NOT NULL,
    geom public.geometry(MultiPolygon,4674),
    cnuc character varying(12),
    nome character varying(107),
    id_passo bigint,
    id_uso bigint,
    id_item bigint,
    ano bigint,
    excluido character varying(254),
    centroid_rv_samge_2018_cnuc character varying(12),
    centroid_rv_samge_2018_nome character varying(107),
    centroid_rv_samge_2018_id_passo bigint,
    centroid_rv_samge_2018_ano bigint
);


CREATE TABLE public.__i_ent_b_rv (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext
);


CREATE TABLE public.__i_ent_c_uso (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext,
    c_y public.citext,
    c_z public.citext,
    c_aa public.citext,
    c_ab public.citext,
    c_ac public.citext,
    c_ad public.citext,
    c_ae public.citext,
    c_af public.citext,
    c_ag public.citext,
    c_ah public.citext,
    c_ai public.citext,
    c_aj public.citext,
    c_ak public.citext
);


CREATE TABLE public.__i_ent_d_acao (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext,
    c_y public.citext,
    c_z public.citext
);

CREATE TABLE public.__i_ent_g_processo (
    id integer NOT NULL,
    c1 public.citext,
    c2 public.citext,
    c3 public.citext,
    c4 public.citext,
    c5 public.citext,
    c6 public.citext,
    c7 public.citext,
    c8 public.citext,
    c9 public.citext,
    c10 public.citext,
    c11 public.citext,
    c12 public.citext,
    c13 public.citext,
    c14 public.citext,
    c15 public.citext,
    c16 public.citext,
    c17 public.citext,
    c18 public.citext,
    c19 public.citext,
    c20 public.citext,
    c21 public.citext,
    c22 public.citext,
    c23 public.citext,
    c24 public.citext,
    c25 public.citext,
    c26 public.citext,
    c27 public.citext,
    c28 public.citext,
    c29 public.citext,
    c30 public.citext,
    c31 public.citext,
    c32 public.citext,
    c33 public.citext,
    c34 public.citext,
    c35 public.citext,
    c36 public.citext,
    c37 public.citext,
    c38 public.citext,
    c39 public.citext,
    c40 public.citext,
    c41 public.citext,
    c42 public.citext,
    c43 public.citext,
    c44 public.citext,
    c45 public.citext,
    c46 public.citext,
    c47 public.citext,
    c48 public.citext,
    c49 public.citext,
    c50 public.citext,
    c51 public.citext,
    c52 public.citext,
    c53 public.citext,
    c54 public.citext,
    c55 public.citext,
    c56 public.citext,
    c57 public.citext,
    c58 public.citext,
    c59 public.citext,
    c60 public.citext,
    c61 public.citext,
    c62 public.citext,
    c63 public.citext,
    c64 public.citext,
    c65 public.citext,
    c66 public.citext,
    c67 public.citext,
    c68 public.citext,
    c69 public.citext,
    c70 public.citext,
    c71 public.citext,
    c72 public.citext
);



CREATE TABLE public.__i_ngi_cr (
    cnuc public.citext,
    ngi public.citext,
    cr public.citext
);



CREATE TABLE public.__i_uc (
    ucid integer NOT NULL,
    c1 character varying(2500),
    c2 character varying(2500),
    c3 character varying(2500),
    c4 character varying(2500),
    c5 character varying(2500),
    c6 character varying(2500),
    c7 character varying(2500),
    c8 character varying(2500),
    c9 character varying(2500),
    c10 character varying(2500),
    c11 character varying(2500),
    c12 character varying(2500),
    c13 character varying(2500),
    c14 character varying(2500),
    c15 character varying(2500),
    c16 character varying(2500),
    c17 character varying(2500),
    c18 character varying(2500),
    c19 character varying(2500),
    c20 character varying(2500),
    c21 character varying(2500),
    c22 character varying(2500),
    c23 character varying(2500),
    c24 character varying(2500),
    c25 character varying(2500),
    c26 character varying(2500),
    c27 character varying(2500),
    c28 character varying(2500),
    c29 character varying(2500),
    c30 character varying(2500)
);



CREATE TABLE public.__i_uc_cr (
    __i_uc_crid integer NOT NULL,
    cnuc public.citext,
    cr public.citext
);

CREATE TABLE public.__i_uc_grupo (
    id integer NOT NULL,
    c1 public.citext,
    c2 public.citext,
    c3 public.citext,
    c4 public.citext
);


CREATE TABLE public.__unidadeconservacao (
    categoriamanejoid integer NOT NULL,
    categoriaiucnid integer NOT NULL,
    situacao public.citext NOT NULL,
    tipoesfera public.citext NOT NULL,
    esfera public.citext NOT NULL,
    codigouc integer NOT NULL,
    cnuc public.citext NOT NULL,
    nome public.citext NOT NULL,
    ufs public.citext NOT NULL,
    anocriacao integer,
    atolegalcriacao public.citext,
    outrosatoslegais public.citext,
    municipios public.citext,
    templanomanejo public.citext,
    temconselhogestor public.citext,
    fontearea public.citext,
    areabioma numeric(14,4),
    areaatolegalcriacao numeric(14,4),
    areaamazonia numeric(14,4),
    areacaatinga numeric(14,4),
    areacerrado numeric(14,4),
    areamataatlantica numeric(14,4),
    areapampa numeric(14,4),
    areapantanal numeric(14,4),
    areamarinha numeric(14,4),
    biomaid integer
);



CREATE TABLE public._gc_auditoria (
    _gc_auditoriaid integer NOT NULL,
    _gc_usuarioid integer,
    ip public.citext DEFAULT NULL::character varying,
    data timestamp(0) without time zone NOT NULL,
    tabela public.citext DEFAULT NULL::character varying,
    registroid integer,
    coluna public.citext DEFAULT NULL::character varying,
    valorantigo public.citext DEFAULT NULL::character varying,
    valornovo public.citext DEFAULT NULL::character varying,
    descricao public.citext NOT NULL
);


CREATE TABLE public._gc_metadados (
    dado public.citext NOT NULL,
    idiomadosite public.citext DEFAULT 'pt_BR'::character varying NOT NULL,
    valor public.citext NOT NULL
);



CREATE TABLE public._gc_paginaavulsa (
    _gc_paginaavulsaid integer NOT NULL,
    codigopaginafixa public.citext DEFAULT NULL::character varying,
    situacao public.citext NOT NULL,
    titulo_ptbr public.citext DEFAULT NULL::character varying,
    slug_ptbr public.citext DEFAULT NULL::character varying,
    texto_ptbr public.citext
);


CREATE TABLE public._gc_repositorio (
    _gc_repositorioid integer NOT NULL,
    datacriacao timestamp(0) without time zone NOT NULL,
    situacao public.citext NOT NULL,
    tipo public.citext NOT NULL,
    url public.citext NOT NULL,
    descricao public.citext DEFAULT NULL::character varying
);


CREATE TABLE public._gc_usuario (
    _gc_usuarioid integer NOT NULL,
    login public.citext NOT NULL,
    email public.citext NOT NULL,
    senha public.citext NOT NULL,
    situacao public.citext NOT NULL,
    tokenrecuperarsenha public.citext DEFAULT NULL::character varying
);


CREATE TABLE public._gc_usuariologin (
    _gc_usuariologinid integer NOT NULL,
    _gc_usuarioid integer NOT NULL,
    datahora timestamp(0) without time zone NOT NULL,
    ip public.citext NOT NULL
);


CREATE TABLE public._m_acaomanejo (
    _m_acaomanejoid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    acao public.citext NOT NULL
);



CREATE TABLE public._m_atividadeconsultoria (
    _m_atividadeconsultoriaid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    atividadeconsultoria public.citext NOT NULL
);


CREATE TABLE public._m_categoria (
    _m_categoriaid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    categoria public.citext NOT NULL,
    nomereduzido public.citext
);




CREATE TABLE public._m_categoriaobjetivo (
    _m_categoriaobjetivoid integer NOT NULL,
    _m_categoriaid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    categoriaobjetivo public.citext NOT NULL
);




CREATE TABLE public._m_efetividade (
    _m_efetividadeid integer NOT NULL,
    valorinicial numeric(18,8),
    valorfinal numeric(18,8),
    posicao numeric(18,8),
    escala numeric(18,8)
);




CREATE TABLE public._m_identificacaopassos (
    id integer NOT NULL,
    passo integer,
    identificacao character varying(25)
);




CREATE TABLE public._m_instrumentoservico (
    _m_instrumentoservicoid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    instrumentoservico public.citext NOT NULL
);



CREATE TABLE public._m_legalidademarcada (
    _m_legalidademarcadaid integer NOT NULL,
    situacao text NOT NULL,
    legalidade text NOT NULL,
    incentivadopermitido text,
    incentivadovedado text,
    incentivadoentorno text,
    permitidoincentivado text,
    permitidovedado text,
    permitidoentorno text,
    vedadopermitido text,
    vedadoincentivado text,
    vedadoentorno text,
    entornopermitido text,
    entornoincentivado text,
    entornovedado text
);


CREATE TABLE public._m_matriz (
    _m_matrizid integer NOT NULL,
    tipoid public.citext NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    valor public.citext NOT NULL,
    valor2 public.citext
);

CREATE TABLE public._m_processo (
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    processo public.citext NOT NULL
);


CREATE TABLE public._m_usoespecifico (
    _m_usoespecificoid integer NOT NULL,
    _m_usogenericoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    usoespecifico public.citext NOT NULL
);


CREATE TABLE public._m_usoespecificocategoriamanejo (
    _m_usoespecificocategoriamanejoid integer NOT NULL,
    _m_usoespecificoid integer NOT NULL,
    categoriamanejo public.citext,
    valor public.citext NOT NULL,
    _m_categoriaid integer
);


CREATE TABLE public._m_usogenerico (
    _m_usogenericoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    usogenerico public.citext NOT NULL
);


CREATE TABLE public.anosamge (
    anosamgeid integer NOT NULL,
    situacao public.citext NOT NULL,
    ano integer NOT NULL,
    datainicio timestamp(0) without time zone,
    datafim timestamp(0) without time zone
);

CREATE TABLE public.banner (
    bannerid integer NOT NULL,
    situacao public.citext,
    ordem integer,
    imagem public.citext,
    link public.citext,
    legenda public.citext
);

CREATE TABLE public.cadastro (
    cadastroid integer NOT NULL,
    cnuc public.citext,
    nome public.citext,
    email public.citext,
    cpf public.citext,
    senha public.citext,
    situacao public.citext,
    permissao_administrador public.citext,
    permissao_federal public.citext,
    token_recuperarsenha public.citext,
    id_perfil integer
);

CREATE TABLE public.cadastro_cnuc (
    cadastro_cnucid integer NOT NULL,
    cadastroid public.citext,
    cnuc public.citext
);


CREATE TABLE public.cadastro_cr (
    cadastro_crid integer NOT NULL,
    cadastroid integer NOT NULL,
    cr public.citext NOT NULL
);

CREATE TABLE public.cadastro_uf (
    cadastro_ufid integer NOT NULL,
    cadastroid integer NOT NULL,
    uf public.citext NOT NULL
);


CREATE TABLE public.entrada (
    entradaid integer NOT NULL,
    unidadeconservacaoid integer NOT NULL,
    situacao public.citext NOT NULL,
    anoreferencia integer NOT NULL,
    datapreenchimento timestamp(0) without time zone,
    dataatualizacao timestamp(0) without time zone,
    quempreenche public.citext,
    preenchedores public.citext,
    salvoupasso1 timestamp(0) without time zone,
    salvoupasso2 timestamp(0) without time zone,
    salvoupasso3 timestamp(0) without time zone,
    salvoupasso4 timestamp(0) without time zone,
    salvoupasso5 timestamp(0) without time zone,
    salvoupasso6 timestamp(0) without time zone,
    salvoupasso7 timestamp(0) without time zone,
    calculoresultados numeric(18,8),
    calculoprodutos numeric(18,8),
    calculocontexto numeric(18,8),
    calculoplanejamento numeric(18,8),
    calculoinsumos numeric(18,8),
    calculoprocessos numeric(18,8),
    calculoresultadosgrafico numeric(18,8),
    calculoprodutosgrafico numeric(18,8),
    calculocontextografico numeric(18,8),
    calculoplanejamentografico numeric(18,8),
    calculoinsumosgrafico numeric(18,8),
    calculoprocessosgrafico numeric(18,8),
    calculoefetividade numeric(18,8),
    justificativavalidacao public.citext,
    tipovalidacao integer
);


CREATE TABLE public.entradaacao (
    entradaacaoid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_acaomanejoid integer NOT NULL,
    _m_instrumentoamparaacaoid integer,
    situacao public.citext NOT NULL,
    referencia public.citext NOT NULL,
    acaodescricao public.citext,
    pessoal public.citext,
    capacidade public.citext,
    equipamento public.citext,
    recursofinanceiro public.citext,
    grauapoionecessario public.citext,
    tipoapoionecessario public.citext,
    origemapoio public.citext,
    governabilidade public.citext,
    apoioaoprocesso public.citext,
    esforco integer,
    consolidacaoprocesso public.citext,
    calculo numeric(18,8),
    calculoprocesso numeric(18,8),
    calculoprioridade numeric(18,8),
    referenciaprocesso public.citext,
    calculoordem numeric(18,8),
    datainsercao timestamp without time zone,
    dataatualizacao timestamp without time zone,
    situacaoexecucao public.citext,
    referenciaitem integer
);


CREATE TABLE public.entradarv (
    entradarvid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_alvosid integer,
    situacao public.citext NOT NULL,
    referencia public.citext,
    objetivoclasse public.citext NOT NULL,
    objetivodescricao public.citext NOT NULL,
    alvonome public.citext,
    conservacaointervencao public.citext NOT NULL,
    danoanterior public.citext,
    fator public.citext,
    naturalseminatural public.citext,
    fonterv public.citext,
    calculoprioridade_usosrelacionados numeric(18,8),
    entradarv public.citext,
    imagem public.citext,
    referenciaitem integer
);

CREATE TABLE public.entradarvacao (
    entradarvacaoid integer NOT NULL,
    entradarvid integer NOT NULL,
    entradaacaoid integer NOT NULL,
    ordem integer
);

CREATE TABLE public.entradarvuso (
    entradarvusoid integer NOT NULL,
    entradarvid integer NOT NULL,
    entradausoid integer NOT NULL,
    ordem integer
);

CREATE TABLE public.entradauso (
    entradausoid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_usoespecificoid integer NOT NULL,
    _m_legalidademarcadaid integer,
    situacao public.citext NOT NULL,
    referencia public.citext NOT NULL,
    descricao public.citext,
    legalidademarcada public.citext NOT NULL,
    legalidade public.citext NOT NULL,
    tbc smallint,
    ip_ec_individual smallint,
    ip_ec_entorno smallint,
    ip_ec_sociedade smallint,
    ip_so_individual smallint,
    ip_so_entorno smallint,
    ip_so_sociedade smallint,
    ip_co_populacoes smallint,
    ip_co_especies smallint,
    ip_co_rv smallint,
    ip_ma_uso smallint,
    ip_ma_unidade smallint,
    ip_ma_sistema smallint,
    in_severidade smallint,
    in_magnitude smallint,
    in_irreversibilidade smallint,
    calculo numeric(18,8),
    voluntariado public.citext,
    calculoprioridade numeric(18,8),
    legalidadecorrigida public.citext,
    datainsercao timestamp without time zone,
    dataatualizacao timestamp without time zone,
    referenciaitem integer
);

CREATE TABLE public.entradausoacao (
    entradausoacaoid integer NOT NULL,
    entradausoid integer NOT NULL,
    entradaacaoid integer NOT NULL,
    ordem integer
);

CREATE TABLE public.nivelacessoporperfil (
    id integer NOT NULL,
    nivel integer NOT NULL,
    id_perfil integer
);

CREATE TABLE public.parceiro (
    parceiroid integer NOT NULL,
    situacao public.citext,
    imagem public.citext,
    link public.citext,
    tipo public.citext
);

CREATE TABLE public.passos_anotacoes (
    passos_anotacoesid integer NOT NULL,
    entradaid integer NOT NULL,
    passo integer NOT NULL,
    declaracao public.citext,
    consideracoes public.citext,
    datavalidacao timestamp(0) without time zone,
    anotacao public.citext,
    dataanotacao timestamp(0) without time zone,
    id_cadastro integer,
    item integer
);


CREATE TABLE public.perfil (
    id integer NOT NULL,
    nome public.citext
);


CREATE TABLE public.relatorio (
    relatorioid integer NOT NULL,
    situacao public.citext,
    ano public.citext,
    excel public.citext,
    pdf public.citext,
    imagem public.citext
);


CREATE TABLE public.urlpowerbi (
    id integer NOT NULL,
    url public.citext NOT NULL,
    situacao public.citext,
    painel public.citext
);


ALTER TABLE public.urlpowerbi OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 65247)
-- Name: urlpowerbi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.urlpowerbi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- TOC entry 4710 (class 2606 OID 88879)
-- Name: espacializacao espacializacao_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.espacializacao
    ADD CONSTRAINT espacializacao_pkey PRIMARY KEY (id);


--
-- TOC entry 4712 (class 2606 OID 88881)
-- Name: estados estados_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.estados
    ADD CONSTRAINT estados_pkey PRIMARY KEY (gid);


--
-- TOC entry 4714 (class 2606 OID 88883)
-- Name: municipios municipios_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.municipios
    ADD CONSTRAINT municipios_pkey PRIMARY KEY (id);


--
-- TOC entry 4716 (class 2606 OID 88885)
-- Name: ucs_brasil ucs_brasil_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.ucs_brasil
    ADD CONSTRAINT ucs_brasil_pkey PRIMARY KEY (gid);


--
-- TOC entry 4721 (class 2606 OID 88887)
-- Name: unidades_conservacao_shp_old unidades_conservacao_shp_new_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp_old
    ADD CONSTRAINT unidades_conservacao_shp_new_pkey PRIMARY KEY (gid);


--
-- TOC entry 4719 (class 2606 OID 88889)
-- Name: unidades_conservacao_shp unidades_conservacao_shp_new_pkey1; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp
    ADD CONSTRAINT unidades_conservacao_shp_new_pkey1 PRIMARY KEY (gid);


--
-- TOC entry 4724 (class 2606 OID 88891)
-- Name: Teste_BD_SAMGe Teste_BD_SAMGe_pkey; Type: CONSTRAINT; Schema: public; Owner: samge_read
--

ALTER TABLE ONLY public."Teste_BD_SAMGe"
    ADD CONSTRAINT "Teste_BD_SAMGe_pkey" PRIMARY KEY (id);


--
-- TOC entry 4726 (class 2606 OID 88893)
-- Name: __i_ent_b_rv __i_ent_b_rv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_b_rv
    ADD CONSTRAINT __i_ent_b_rv_pkey PRIMARY KEY (id);


--
-- TOC entry 4728 (class 2606 OID 88895)
-- Name: __i_ent_c_uso __i_ent_c_uso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_c_uso
    ADD CONSTRAINT __i_ent_c_uso_pkey PRIMARY KEY (id);


--
-- TOC entry 4730 (class 2606 OID 88897)
-- Name: __i_ent_d_acao __i_ent_d_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_d_acao
    ADD CONSTRAINT __i_ent_d_acao_pkey PRIMARY KEY (id);


--
-- TOC entry 4732 (class 2606 OID 88899)
-- Name: __i_ent_g_processo __i_ent_g_processo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_g_processo
    ADD CONSTRAINT __i_ent_g_processo_pkey PRIMARY KEY (id);


--
-- TOC entry 4736 (class 2606 OID 88901)
-- Name: __i_uc_cr __i_uc_cr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_cr
    ADD CONSTRAINT __i_uc_cr_pkey PRIMARY KEY (__i_uc_crid);


--
-- TOC entry 4738 (class 2606 OID 88903)
-- Name: __i_uc_grupo __i_uc_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_grupo
    ADD CONSTRAINT __i_uc_grupo_pkey PRIMARY KEY (id);


--
-- TOC entry 4740 (class 2606 OID 88905)
-- Name: _gc_auditoria _gc_auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_auditoria
    ADD CONSTRAINT _gc_auditoria_pkey PRIMARY KEY (_gc_auditoriaid);


--
-- TOC entry 4746 (class 2606 OID 88907)
-- Name: _gc_metadados _gc_metadados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_metadados
    ADD CONSTRAINT _gc_metadados_pkey PRIMARY KEY (dado, idiomadosite);


--
-- TOC entry 4748 (class 2606 OID 88909)
-- Name: _gc_paginaavulsa _gc_paginaavulsa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_paginaavulsa
    ADD CONSTRAINT _gc_paginaavulsa_pkey PRIMARY KEY (_gc_paginaavulsaid);


--
-- TOC entry 4751 (class 2606 OID 88911)
-- Name: _gc_repositorio _gc_repositorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_repositorio
    ADD CONSTRAINT _gc_repositorio_pkey PRIMARY KEY (_gc_repositorioid);


--
-- TOC entry 4754 (class 2606 OID 88913)
-- Name: _gc_usuario _gc_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuario
    ADD CONSTRAINT _gc_usuario_pkey PRIMARY KEY (_gc_usuarioid);


--
-- TOC entry 4756 (class 2606 OID 88915)
-- Name: _gc_usuariologin _gc_usuariologin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuariologin
    ADD CONSTRAINT _gc_usuariologin_pkey PRIMARY KEY (_gc_usuariologinid);


--
-- TOC entry 4758 (class 2606 OID 88917)
-- Name: _m_acaomanejo _m_acaomanejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_acaomanejo
    ADD CONSTRAINT _m_acaomanejo_pkey PRIMARY KEY (_m_acaomanejoid);


--
-- TOC entry 4762 (class 2606 OID 88919)
-- Name: _m_atividadeconsultoria _m_atividadeconsultoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_atividadeconsultoria
    ADD CONSTRAINT _m_atividadeconsultoria_pkey PRIMARY KEY (_m_atividadeconsultoriaid);


--
-- TOC entry 4764 (class 2606 OID 88921)
-- Name: _m_categoria _m_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoria
    ADD CONSTRAINT _m_categoria_pkey PRIMARY KEY (_m_categoriaid);


--
-- TOC entry 4766 (class 2606 OID 88923)
-- Name: _m_categoriaobjetivo _m_categoriaobjetivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoriaobjetivo
    ADD CONSTRAINT _m_categoriaobjetivo_pkey PRIMARY KEY (_m_categoriaobjetivoid);


--
-- TOC entry 4768 (class 2606 OID 88925)
-- Name: _m_efetividade _m_efetividade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_efetividade
    ADD CONSTRAINT _m_efetividade_pkey PRIMARY KEY (_m_efetividadeid);


--
-- TOC entry 4770 (class 2606 OID 88927)
-- Name: _m_identificacaopassos _m_identificacaopassos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_identificacaopassos
    ADD CONSTRAINT _m_identificacaopassos_pkey PRIMARY KEY (id);


--
-- TOC entry 4773 (class 2606 OID 88929)
-- Name: _m_instrumentoservico _m_instrumentoservico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_instrumentoservico
    ADD CONSTRAINT _m_instrumentoservico_pkey PRIMARY KEY (_m_instrumentoservicoid);


--
-- TOC entry 4775 (class 2606 OID 88931)
-- Name: _m_legalidademarcada _m_legalidademarcada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_legalidademarcada
    ADD CONSTRAINT _m_legalidademarcada_pkey PRIMARY KEY (_m_legalidademarcadaid);


--
-- TOC entry 4777 (class 2606 OID 88933)
-- Name: _m_matriz _m_matriz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_matriz
    ADD CONSTRAINT _m_matriz_pkey PRIMARY KEY (_m_matrizid);


--
-- TOC entry 4780 (class 2606 OID 88935)
-- Name: _m_processo _m_processo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_processo
    ADD CONSTRAINT _m_processo_pkey PRIMARY KEY (_m_processoid);


--
-- TOC entry 4782 (class 2606 OID 88937)
-- Name: _m_usoespecifico _m_usoespecifico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecifico
    ADD CONSTRAINT _m_usoespecifico_pkey PRIMARY KEY (_m_usoespecificoid);


--
-- TOC entry 4784 (class 2606 OID 88939)
-- Name: _m_usoespecificocategoriamanejo _m_usoespecificocategoriamanejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecificocategoriamanejo
    ADD CONSTRAINT _m_usoespecificocategoriamanejo_pkey PRIMARY KEY (_m_usoespecificocategoriamanejoid);


--
-- TOC entry 4786 (class 2606 OID 88941)
-- Name: _m_usogenerico _m_usogenerico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usogenerico
    ADD CONSTRAINT _m_usogenerico_pkey PRIMARY KEY (_m_usogenericoid);


--
-- TOC entry 4734 (class 2606 OID 88943)
-- Name: __i_uc _uc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc
    ADD CONSTRAINT _uc_pkey PRIMARY KEY (ucid);


--
-- TOC entry 4788 (class 2606 OID 88945)
-- Name: anosamge anosamge_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anosamge
    ADD CONSTRAINT anosamge_pkey PRIMARY KEY (anosamgeid);


--
-- TOC entry 4790 (class 2606 OID 88947)
-- Name: banner banner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (bannerid);


--
-- TOC entry 4794 (class 2606 OID 88949)
-- Name: cadastro_cnuc cadastro_cnuc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cnuc
    ADD CONSTRAINT cadastro_cnuc_pkey PRIMARY KEY (cadastro_cnucid);


--
-- TOC entry 4796 (class 2606 OID 88951)
-- Name: cadastro_cr cadastro_cr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cr
    ADD CONSTRAINT cadastro_cr_pkey PRIMARY KEY (cadastro_crid);


--
-- TOC entry 4792 (class 2606 OID 88953)
-- Name: cadastro cadastro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro
    ADD CONSTRAINT cadastro_pkey PRIMARY KEY (cadastroid);


--
-- TOC entry 4798 (class 2606 OID 88955)
-- Name: cadastro_uf cadastro_uf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_uf
    ADD CONSTRAINT cadastro_uf_pkey PRIMARY KEY (cadastro_ufid);


--
-- TOC entry 4800 (class 2606 OID 88957)
-- Name: entrada entrada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entrada
    ADD CONSTRAINT entrada_pkey PRIMARY KEY (entradaid);


--
-- TOC entry 4803 (class 2606 OID 88959)
-- Name: entradaacao entradaacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT entradaacao_pkey PRIMARY KEY (entradaacaoid);


--
-- TOC entry 4805 (class 2606 OID 88961)
-- Name: entradarv entradarv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarv
    ADD CONSTRAINT entradarv_pkey PRIMARY KEY (entradarvid);


--
-- TOC entry 4807 (class 2606 OID 88963)
-- Name: entradarvacao entradarvacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT entradarvacao_pkey PRIMARY KEY (entradarvacaoid);


--
-- TOC entry 4809 (class 2606 OID 88965)
-- Name: entradarvuso entradarvuso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT entradarvuso_pkey PRIMARY KEY (entradarvusoid);


--
-- TOC entry 4811 (class 2606 OID 88967)
-- Name: entradauso entradauso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT entradauso_pkey PRIMARY KEY (entradausoid);


--
-- TOC entry 4814 (class 2606 OID 88969)
-- Name: entradausoacao entradausoacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT entradausoacao_pkey PRIMARY KEY (entradausoacaoid);


--
-- TOC entry 4816 (class 2606 OID 88971)
-- Name: nivelacessoporperfil nivelacessoporperfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivelacessoporperfil
    ADD CONSTRAINT nivelacessoporperfil_pkey PRIMARY KEY (id);


--
-- TOC entry 4818 (class 2606 OID 88973)
-- Name: parceiro parceiro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parceiro
    ADD CONSTRAINT parceiro_pkey PRIMARY KEY (parceiroid);


--
-- TOC entry 4820 (class 2606 OID 88975)
-- Name: passos_anotacoes passos_anotacoes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.passos_anotacoes
    ADD CONSTRAINT passos_anotacoes_pkey PRIMARY KEY (passos_anotacoesid);


--
-- TOC entry 4822 (class 2606 OID 88977)
-- Name: perfil perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 4824 (class 2606 OID 88979)
-- Name: relatorio relatorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relatorio
    ADD CONSTRAINT relatorio_pkey PRIMARY KEY (relatorioid);


--
-- TOC entry 4826 (class 2606 OID 88983)
-- Name: urlpowerbi urlpowerbi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urlpowerbi
    ADD CONSTRAINT urlpowerbi_pkey PRIMARY KEY (id);


--
-- TOC entry 4717 (class 1259 OID 88984)
-- Name: unidades_conservacao_shp_geom_geom_idx; Type: INDEX; Schema: geom; Owner: postgres
--

CREATE INDEX unidades_conservacao_shp_geom_geom_idx ON geom.unidades_conservacao_shp USING gist (geom);


--
-- TOC entry 4722 (class 1259 OID 88985)
-- Name: unidades_conservacao_shp_old_geom_geom_idx; Type: INDEX; Schema: geom; Owner: postgres
--

CREATE INDEX unidades_conservacao_shp_old_geom_geom_idx ON geom.unidades_conservacao_shp_old USING gist (geom);


--
-- TOC entry 4760 (class 1259 OID 88986)
-- Name: _m_atividadeconsultoria_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _m_atividadeconsultoria_m_processoid ON public._m_atividadeconsultoria USING btree (_m_atividadeconsultoriaid);


--
-- TOC entry 4771 (class 1259 OID 88987)
-- Name: _m_instrumentoservico_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _m_instrumentoservico_m_processoid ON public._m_instrumentoservico USING btree (_m_instrumentoservicoid);


--
-- TOC entry 4801 (class 1259 OID 88988)
-- Name: entrada_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX entrada_situacao_idx ON public.entrada USING btree (situacao);


--
-- TOC entry 4812 (class 1259 OID 88989)
-- Name: entradauso_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX entradauso_situacao_idx ON public.entradauso USING btree (situacao);


--
-- TOC entry 4741 (class 1259 OID 88990)
-- Name: idx__gc_auditoria__gc_usuarioid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria__gc_usuarioid ON public._gc_auditoria USING btree (_gc_usuarioid);


--
-- TOC entry 4742 (class 1259 OID 88991)
-- Name: idx__gc_auditoria_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_data ON public._gc_auditoria USING btree (data);


--
-- TOC entry 4743 (class 1259 OID 88992)
-- Name: idx__gc_auditoria_registroid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_registroid ON public._gc_auditoria USING btree (registroid);


--
-- TOC entry 4744 (class 1259 OID 88993)
-- Name: idx__gc_auditoria_tabela; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_tabela ON public._gc_auditoria USING btree (tabela);


--
-- TOC entry 4749 (class 1259 OID 88994)
-- Name: idx__gc_paginaavulsa_situacao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_paginaavulsa_situacao ON public._gc_paginaavulsa USING btree (situacao);


--
-- TOC entry 4752 (class 1259 OID 88995)
-- Name: idx__gc_repositorio_situacao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_repositorio_situacao ON public._gc_repositorio USING btree (situacao);


--
-- TOC entry 4759 (class 1259 OID 88996)
-- Name: idx__m_acaomanejo_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__m_acaomanejo_m_processoid ON public._m_acaomanejo USING btree (_m_acaomanejoid);


--
-- TOC entry 4778 (class 1259 OID 89001)
-- Name: ix_m_matriz_tipoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_m_matriz_tipoid ON public._m_matriz USING btree (tipoid);


--
-- TOC entry 4835 (class 2606 OID 89002)
-- Name: entradaacao fk_entradaacao_entradaid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_entradaid FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4836 (class 2606 OID 89007)
-- Name: entradaacao fk_entradaacao_m_acaomanejo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_m_acaomanejo FOREIGN KEY (_m_acaomanejoid) REFERENCES public._m_acaomanejo(_m_acaomanejoid);


--
-- TOC entry 4837 (class 2606 OID 89012)
-- Name: entradaacao fk_entradaacao_m_matriz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_m_matriz FOREIGN KEY (_m_instrumentoamparaacaoid) REFERENCES public._m_matriz(_m_matrizid);


--
-- TOC entry 4838 (class 2606 OID 89017)
-- Name: entradarv fk_entradarv_entrada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarv
    ADD CONSTRAINT fk_entradarv_entrada FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4839 (class 2606 OID 89022)
-- Name: entradarvacao fk_entradarvacao_entradaacao; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT fk_entradarvacao_entradaacao FOREIGN KEY (entradaacaoid) REFERENCES public.entradaacao(entradaacaoid);


--
-- TOC entry 4840 (class 2606 OID 89027)
-- Name: entradarvacao fk_entradarvacao_entradarv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT fk_entradarvacao_entradarv FOREIGN KEY (entradarvid) REFERENCES public.entradarv(entradarvid);


--
-- TOC entry 4841 (class 2606 OID 89032)
-- Name: entradarvuso fk_entradarvuso_entradarv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT fk_entradarvuso_entradarv FOREIGN KEY (entradarvid) REFERENCES public.entradarv(entradarvid);


--
-- TOC entry 4842 (class 2606 OID 89037)
-- Name: entradarvuso fk_entradarvuso_entradauso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT fk_entradarvuso_entradauso FOREIGN KEY (entradausoid) REFERENCES public.entradauso(entradausoid);


--
-- TOC entry 4843 (class 2606 OID 89042)
-- Name: entradauso fk_entradauso_entrada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT fk_entradauso_entrada FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4844 (class 2606 OID 89047)
-- Name: entradauso fk_entradauso_m_usoespecificoid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT fk_entradauso_m_usoespecificoid FOREIGN KEY (_m_usoespecificoid) REFERENCES public._m_usoespecifico(_m_usoespecificoid);


--
-- TOC entry 4845 (class 2606 OID 89052)
-- Name: entradausoacao fk_entradausoacao_entradaacao; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT fk_entradausoacao_entradaacao FOREIGN KEY (entradaacaoid) REFERENCES public.entradaacao(entradaacaoid);


--
-- TOC entry 4846 (class 2606 OID 89057)
-- Name: entradausoacao fk_entradausoacao_entradauso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT fk_entradausoacao_entradauso FOREIGN KEY (entradausoid) REFERENCES public.entradauso(entradausoid);


--
-- TOC entry 4827 (class 2606 OID 89062)
-- Name: _gc_auditoria fk_gc_auditoria_gc_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_auditoria
    ADD CONSTRAINT fk_gc_auditoria_gc_usuario FOREIGN KEY (_gc_usuarioid) REFERENCES public._gc_usuario(_gc_usuarioid);


--
-- TOC entry 4828 (class 2606 OID 89067)
-- Name: _gc_usuariologin fk_gc_usuariologin_gc_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuariologin
    ADD CONSTRAINT fk_gc_usuariologin_gc_usuario FOREIGN KEY (_gc_usuarioid) REFERENCES public._gc_usuario(_gc_usuarioid);


--
-- TOC entry 4829 (class 2606 OID 89072)
-- Name: _m_acaomanejo fk_m_acaomanejo_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_acaomanejo
    ADD CONSTRAINT fk_m_acaomanejo_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4830 (class 2606 OID 89077)
-- Name: _m_atividadeconsultoria fk_m_atividadeconsultoria_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_atividadeconsultoria
    ADD CONSTRAINT fk_m_atividadeconsultoria_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4831 (class 2606 OID 89082)
-- Name: _m_categoriaobjetivo fk_m_categoriaobjetivo_m_categoriaobjetivo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoriaobjetivo
    ADD CONSTRAINT fk_m_categoriaobjetivo_m_categoriaobjetivo FOREIGN KEY (_m_categoriaid) REFERENCES public._m_categoria(_m_categoriaid);


--
-- TOC entry 4832 (class 2606 OID 89087)
-- Name: _m_instrumentoservico fk_m_instrumentoservico_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_instrumentoservico
    ADD CONSTRAINT fk_m_instrumentoservico_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4833 (class 2606 OID 89092)
-- Name: _m_usoespecifico fk_m_usoespecifico_m_usogenerico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecifico
    ADD CONSTRAINT fk_m_usoespecifico_m_usogenerico FOREIGN KEY (_m_usogenericoid) REFERENCES public._m_usogenerico(_m_usogenericoid);


--
-- TOC entry 4834 (class 2606 OID 89097)
-- Name: _m_usoespecificocategoriamanejo fk_m_usoespecificocategoriamanejo_m_usoespecifico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecificocategoriamanejo
    ADD CONSTRAINT fk_m_usoespecificocategoriamanejo_m_usoespecifico FOREIGN KEY (_m_usoespecificoid) REFERENCES public._m_usoespecifico(_m_usoespecificoid);


--
-- TOC entry 4847 (class 2606 OID 89102)
-- Name: nivelacessoporperfil nivelacessoporperfil_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivelacessoporperfil
    ADD CONSTRAINT nivelacessoporperfil_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.perfil(id);

