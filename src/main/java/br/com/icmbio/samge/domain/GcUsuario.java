package br.com.icmbio.samge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A GcUsuario.
 */
@Entity
@Table(name = "_gc_usuario", schema = "samge")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GcUsuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "_gc_usuarioid", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "login", nullable = false)
    private String login;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "senha", nullable = false)
    private String senha;

    @NotNull
    @Column(name = "situacao", nullable = false)
    private String situacao;

    @Column(name = "tokenrecuperarsenha")
    private String tokenRecuperarSenha;

    @OneToMany(mappedBy = "gcUsuario")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "gcUsuario", "gcUsuario" }, allowSetters = true)
    private Set<GcUsuarioLogin> gcUsuarioLogins = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public GcUsuario id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public GcUsuario login(String login) {
        this.setLogin(login);
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return this.email;
    }

    public GcUsuario email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return this.senha;
    }

    public GcUsuario senha(String senha) {
        this.setSenha(senha);
        return this;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSituacao() {
        return this.situacao;
    }

    public GcUsuario situacao(String situacao) {
        this.setSituacao(situacao);
        return this;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getTokenRecuperarSenha() {
        return this.tokenRecuperarSenha;
    }

    public GcUsuario tokenRecuperarSenha(String tokenRecuperarSenha) {
        this.setTokenRecuperarSenha(tokenRecuperarSenha);
        return this;
    }

    public void setTokenRecuperarSenha(String tokenRecuperarSenha) {
        this.tokenRecuperarSenha = tokenRecuperarSenha;
    }

    public Set<GcUsuarioLogin> getGcUsuarioLogins() {
        return this.gcUsuarioLogins;
    }

    public void setGcUsuarioLogins(Set<GcUsuarioLogin> gcUsuarioLogins) {
        if (this.gcUsuarioLogins != null) {
            this.gcUsuarioLogins.forEach(i -> i.setGcUsuario(null));
        }
        if (gcUsuarioLogins != null) {
            gcUsuarioLogins.forEach(i -> i.setGcUsuario(this));
        }
        this.gcUsuarioLogins = gcUsuarioLogins;
    }

    public GcUsuario gcUsuarioLogins(Set<GcUsuarioLogin> gcUsuarioLogins) {
        this.setGcUsuarioLogins(gcUsuarioLogins);
        return this;
    }

    public GcUsuario addGcUsuarioLogin(GcUsuarioLogin gcUsuarioLogin) {
        this.gcUsuarioLogins.add(gcUsuarioLogin);
        gcUsuarioLogin.setGcUsuario(this);
        return this;
    }

    public GcUsuario removeGcUsuarioLogin(GcUsuarioLogin gcUsuarioLogin) {
        this.gcUsuarioLogins.remove(gcUsuarioLogin);
        gcUsuarioLogin.setGcUsuario(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GcUsuario)) {
            return false;
        }
        return id != null && id.equals(((GcUsuario) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GcUsuario{" +
            "id=" + getId() +
            ", login='" + getLogin() + "'" +
            ", email='" + getEmail() + "'" +
            ", senha='" + getSenha() + "'" +
            ", situacao='" + getSituacao() + "'" +
            ", tokenRecuperarSenha='" + getTokenRecuperarSenha() + "'" +
            "}";
    }
}
