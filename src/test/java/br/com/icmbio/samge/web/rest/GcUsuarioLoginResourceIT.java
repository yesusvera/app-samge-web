package br.com.icmbio.samge.web.rest;

import static br.com.icmbio.samge.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.icmbio.samge.IntegrationTest;
import br.com.icmbio.samge.domain.GcUsuarioLogin;
import br.com.icmbio.samge.repository.GcUsuarioLoginRepository;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GcUsuarioLoginResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GcUsuarioLoginResourceIT {

    private static final ZonedDateTime DEFAULT_DATA_HORA = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATA_HORA = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_IP = "AAAAAAAAAA";
    private static final String UPDATED_IP = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/gc-usuario-logins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GcUsuarioLoginRepository gcUsuarioLoginRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGcUsuarioLoginMockMvc;

    private GcUsuarioLogin gcUsuarioLogin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GcUsuarioLogin createEntity(EntityManager em) {
        GcUsuarioLogin gcUsuarioLogin = new GcUsuarioLogin().dataHora(DEFAULT_DATA_HORA).ip(DEFAULT_IP);
        return gcUsuarioLogin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GcUsuarioLogin createUpdatedEntity(EntityManager em) {
        GcUsuarioLogin gcUsuarioLogin = new GcUsuarioLogin().dataHora(UPDATED_DATA_HORA).ip(UPDATED_IP);
        return gcUsuarioLogin;
    }

    @BeforeEach
    public void initTest() {
        gcUsuarioLogin = createEntity(em);
    }

    @Test
    @Transactional
    void createGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeCreate = gcUsuarioLoginRepository.findAll().size();
        // Create the GcUsuarioLogin
        restGcUsuarioLoginMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isCreated());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeCreate + 1);
        GcUsuarioLogin testGcUsuarioLogin = gcUsuarioLoginList.get(gcUsuarioLoginList.size() - 1);
        assertThat(testGcUsuarioLogin.getDataHora()).isEqualTo(DEFAULT_DATA_HORA);
        assertThat(testGcUsuarioLogin.getIp()).isEqualTo(DEFAULT_IP);
    }

    @Test
    @Transactional
    void createGcUsuarioLoginWithExistingId() throws Exception {
        // Create the GcUsuarioLogin with an existing ID
        gcUsuarioLogin.setId(1L);

        int databaseSizeBeforeCreate = gcUsuarioLoginRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGcUsuarioLoginMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDataHoraIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioLoginRepository.findAll().size();
        // set the field null
        gcUsuarioLogin.setDataHora(null);

        // Create the GcUsuarioLogin, which fails.

        restGcUsuarioLoginMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIpIsRequired() throws Exception {
        int databaseSizeBeforeTest = gcUsuarioLoginRepository.findAll().size();
        // set the field null
        gcUsuarioLogin.setIp(null);

        // Create the GcUsuarioLogin, which fails.

        restGcUsuarioLoginMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGcUsuarioLogins() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        // Get all the gcUsuarioLoginList
        restGcUsuarioLoginMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gcUsuarioLogin.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataHora").value(hasItem(sameInstant(DEFAULT_DATA_HORA))))
            .andExpect(jsonPath("$.[*].ip").value(hasItem(DEFAULT_IP)));
    }

    @Test
    @Transactional
    void getGcUsuarioLogin() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        // Get the gcUsuarioLogin
        restGcUsuarioLoginMockMvc
            .perform(get(ENTITY_API_URL_ID, gcUsuarioLogin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gcUsuarioLogin.getId().intValue()))
            .andExpect(jsonPath("$.dataHora").value(sameInstant(DEFAULT_DATA_HORA)))
            .andExpect(jsonPath("$.ip").value(DEFAULT_IP));
    }

    @Test
    @Transactional
    void getNonExistingGcUsuarioLogin() throws Exception {
        // Get the gcUsuarioLogin
        restGcUsuarioLoginMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingGcUsuarioLogin() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();

        // Update the gcUsuarioLogin
        GcUsuarioLogin updatedGcUsuarioLogin = gcUsuarioLoginRepository.findById(gcUsuarioLogin.getId()).get();
        // Disconnect from session so that the updates on updatedGcUsuarioLogin are not directly saved in db
        em.detach(updatedGcUsuarioLogin);
        updatedGcUsuarioLogin.dataHora(UPDATED_DATA_HORA).ip(UPDATED_IP);

        restGcUsuarioLoginMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedGcUsuarioLogin.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedGcUsuarioLogin))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
        GcUsuarioLogin testGcUsuarioLogin = gcUsuarioLoginList.get(gcUsuarioLoginList.size() - 1);
        assertThat(testGcUsuarioLogin.getDataHora()).isEqualTo(UPDATED_DATA_HORA);
        assertThat(testGcUsuarioLogin.getIp()).isEqualTo(UPDATED_IP);
    }

    @Test
    @Transactional
    void putNonExistingGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(
                put(ENTITY_API_URL_ID, gcUsuarioLogin.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGcUsuarioLoginWithPatch() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();

        // Update the gcUsuarioLogin using partial update
        GcUsuarioLogin partialUpdatedGcUsuarioLogin = new GcUsuarioLogin();
        partialUpdatedGcUsuarioLogin.setId(gcUsuarioLogin.getId());

        partialUpdatedGcUsuarioLogin.dataHora(UPDATED_DATA_HORA).ip(UPDATED_IP);

        restGcUsuarioLoginMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGcUsuarioLogin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGcUsuarioLogin))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
        GcUsuarioLogin testGcUsuarioLogin = gcUsuarioLoginList.get(gcUsuarioLoginList.size() - 1);
        assertThat(testGcUsuarioLogin.getDataHora()).isEqualTo(UPDATED_DATA_HORA);
        assertThat(testGcUsuarioLogin.getIp()).isEqualTo(UPDATED_IP);
    }

    @Test
    @Transactional
    void fullUpdateGcUsuarioLoginWithPatch() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();

        // Update the gcUsuarioLogin using partial update
        GcUsuarioLogin partialUpdatedGcUsuarioLogin = new GcUsuarioLogin();
        partialUpdatedGcUsuarioLogin.setId(gcUsuarioLogin.getId());

        partialUpdatedGcUsuarioLogin.dataHora(UPDATED_DATA_HORA).ip(UPDATED_IP);

        restGcUsuarioLoginMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGcUsuarioLogin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGcUsuarioLogin))
            )
            .andExpect(status().isOk());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
        GcUsuarioLogin testGcUsuarioLogin = gcUsuarioLoginList.get(gcUsuarioLoginList.size() - 1);
        assertThat(testGcUsuarioLogin.getDataHora()).isEqualTo(UPDATED_DATA_HORA);
        assertThat(testGcUsuarioLogin.getIp()).isEqualTo(UPDATED_IP);
    }

    @Test
    @Transactional
    void patchNonExistingGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, gcUsuarioLogin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isBadRequest());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGcUsuarioLogin() throws Exception {
        int databaseSizeBeforeUpdate = gcUsuarioLoginRepository.findAll().size();
        gcUsuarioLogin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGcUsuarioLoginMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(gcUsuarioLogin))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GcUsuarioLogin in the database
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGcUsuarioLogin() throws Exception {
        // Initialize the database
        gcUsuarioLoginRepository.saveAndFlush(gcUsuarioLogin);

        int databaseSizeBeforeDelete = gcUsuarioLoginRepository.findAll().size();

        // Delete the gcUsuarioLogin
        restGcUsuarioLoginMockMvc
            .perform(delete(ENTITY_API_URL_ID, gcUsuarioLogin.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GcUsuarioLogin> gcUsuarioLoginList = gcUsuarioLoginRepository.findAll();
        assertThat(gcUsuarioLoginList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
