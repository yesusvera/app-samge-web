package br.com.icmbio.samge.repository;

import br.com.icmbio.samge.domain.GcUsuarioLogin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the GcUsuarioLogin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GcUsuarioLoginRepository extends JpaRepository<GcUsuarioLogin, Long> {}
