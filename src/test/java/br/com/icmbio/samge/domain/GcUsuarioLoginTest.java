package br.com.icmbio.samge.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.icmbio.samge.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GcUsuarioLoginTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GcUsuarioLogin.class);
        GcUsuarioLogin gcUsuarioLogin1 = new GcUsuarioLogin();
        gcUsuarioLogin1.setId(1L);
        GcUsuarioLogin gcUsuarioLogin2 = new GcUsuarioLogin();
        gcUsuarioLogin2.setId(gcUsuarioLogin1.getId());
        assertThat(gcUsuarioLogin1).isEqualTo(gcUsuarioLogin2);
        gcUsuarioLogin2.setId(2L);
        assertThat(gcUsuarioLogin1).isNotEqualTo(gcUsuarioLogin2);
        gcUsuarioLogin1.setId(null);
        assertThat(gcUsuarioLogin1).isNotEqualTo(gcUsuarioLogin2);
    }
}
