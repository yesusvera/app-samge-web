--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2 (Debian 11.2-1.pgdg90+1)
-- Dumped by pg_dump version 15.2

-- Started on 2023-05-26 15:22:30 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 9 (class 2615 OID 63142)
-- Name: geom; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA geom;


ALTER SCHEMA geom OWNER TO postgres;

--
-- TOC entry 10 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3 (class 3079 OID 63143)
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- TOC entry 4983 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- TOC entry 2 (class 3079 OID 63246)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 4984 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

--
-- TOC entry 214 (class 1259 OID 64824)
-- Name: espacializacao; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.espacializacao (
    id integer NOT NULL,
    cnuc character varying(12),
    nome character varying(100),
    id_passo integer,
    id_uso integer,
    id_item integer,
    ano integer,
    geom public.geometry,
    excluido boolean
);


ALTER TABLE geom.espacializacao OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 64830)
-- Name: espacializacao_id_seq; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.espacializacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.espacializacao_id_seq OWNER TO postgres;

--
-- TOC entry 4986 (class 0 OID 0)
-- Dependencies: 215
-- Name: espacializacao_id_seq; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.espacializacao_id_seq OWNED BY geom.espacializacao.id;


--
-- TOC entry 216 (class 1259 OID 64832)
-- Name: estados; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.estados (
    gid integer NOT NULL,
    geom public.geometry(MultiPolygon,4326),
    cd_geocodu character varying(2),
    uf character varying(2),
    nm_estado character varying(50),
    nm_regiao character varying(20)
);


ALTER TABLE geom.estados OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 64838)
-- Name: estados_gid_seq; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.estados_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.estados_gid_seq OWNER TO postgres;

--
-- TOC entry 4988 (class 0 OID 0)
-- Dependencies: 217
-- Name: estados_gid_seq; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.estados_gid_seq OWNED BY geom.estados.gid;


--
-- TOC entry 218 (class 1259 OID 64840)
-- Name: municipios; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.municipios (
    id integer NOT NULL,
    geom public.geometry(MultiPolygon,4326),
    nm_municip character varying(60),
    cd_geocmu character varying(7)
);


ALTER TABLE geom.municipios OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 64846)
-- Name: municipios_id_seq; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.municipios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.municipios_id_seq OWNER TO postgres;

--
-- TOC entry 4990 (class 0 OID 0)
-- Dependencies: 219
-- Name: municipios_id_seq; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.municipios_id_seq OWNED BY geom.municipios.id;


--
-- TOC entry 220 (class 1259 OID 64848)
-- Name: ucs_brasil; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.ucs_brasil (
    gid integer NOT NULL,
    geom public.geometry(MultiPolygon,4326),
    nome character varying(254),
    cnuc character varying(254)
);


ALTER TABLE geom.ucs_brasil OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 64854)
-- Name: ucs_brasil_gid_seq; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.ucs_brasil_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.ucs_brasil_gid_seq OWNER TO postgres;

--
-- TOC entry 4992 (class 0 OID 0)
-- Dependencies: 221
-- Name: ucs_brasil_gid_seq; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.ucs_brasil_gid_seq OWNED BY geom.ucs_brasil.gid;


--
-- TOC entry 222 (class 1259 OID 64856)
-- Name: unidades_conservacao_shp; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.unidades_conservacao_shp (
    gid integer NOT NULL,
    gml_id character varying(80),
    cnuc character varying(80),
    wdpa_pid character varying(80),
    nome character varying(116),
    cria_ano_data character varying(80),
    quali_pol character varying(80),
    ppgr character varying(80),
    org_gestor character varying(153),
    situacao character varying(80),
    limite character varying(80),
    geom public.geometry(Geometry,4326)
);


ALTER TABLE geom.unidades_conservacao_shp OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 64862)
-- Name: unidades_conservacao_shp_old; Type: TABLE; Schema: geom; Owner: postgres
--

CREATE TABLE geom.unidades_conservacao_shp_old (
    gid integer NOT NULL,
    gml_id character varying(80),
    cnuc character varying(80),
    wdpa_pid character varying(80),
    nome character varying(116),
    cria_ano_data character varying(80),
    quali_pol character varying(80),
    ppgr character varying(80),
    org_gestor character varying(153),
    situacao character varying(80),
    limite character varying(80),
    geom public.geometry(Geometry,4326)
);


ALTER TABLE geom.unidades_conservacao_shp_old OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 64868)
-- Name: unidades_conservacao_shp_new_ogc_fid_seq; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.unidades_conservacao_shp_new_ogc_fid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.unidades_conservacao_shp_new_ogc_fid_seq OWNER TO postgres;

--
-- TOC entry 4995 (class 0 OID 0)
-- Dependencies: 224
-- Name: unidades_conservacao_shp_new_ogc_fid_seq; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.unidades_conservacao_shp_new_ogc_fid_seq OWNED BY geom.unidades_conservacao_shp_old.gid;


--
-- TOC entry 225 (class 1259 OID 64870)
-- Name: unidades_conservacao_shp_new_ogc_fid_seq1; Type: SEQUENCE; Schema: geom; Owner: postgres
--

CREATE SEQUENCE geom.unidades_conservacao_shp_new_ogc_fid_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE geom.unidades_conservacao_shp_new_ogc_fid_seq1 OWNER TO postgres;

--
-- TOC entry 4996 (class 0 OID 0)
-- Dependencies: 225
-- Name: unidades_conservacao_shp_new_ogc_fid_seq1; Type: SEQUENCE OWNED BY; Schema: geom; Owner: postgres
--

ALTER SEQUENCE geom.unidades_conservacao_shp_new_ogc_fid_seq1 OWNED BY geom.unidades_conservacao_shp.gid;


--
-- TOC entry 226 (class 1259 OID 64872)
-- Name: Teste_BD_SAMGe; Type: TABLE; Schema: public; Owner: samge_read
--

CREATE TABLE public."Teste_BD_SAMGe" (
    id bigint NOT NULL,
    geom public.geometry(MultiPolygon,4674),
    cnuc character varying(12),
    nome character varying(107),
    id_passo bigint,
    id_uso bigint,
    id_item bigint,
    ano bigint,
    excluido character varying(254),
    centroid_rv_samge_2018_cnuc character varying(12),
    centroid_rv_samge_2018_nome character varying(107),
    centroid_rv_samge_2018_id_passo bigint,
    centroid_rv_samge_2018_ano bigint
);


ALTER TABLE public."Teste_BD_SAMGe" OWNER TO samge_read;

--
-- TOC entry 227 (class 1259 OID 64878)
-- Name: __i_ent_b_rv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_ent_b_rv (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext
);


ALTER TABLE public.__i_ent_b_rv OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 64884)
-- Name: __i_ent_b_rv_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_ent_b_rv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_ent_b_rv_id_seq OWNER TO postgres;

--
-- TOC entry 4998 (class 0 OID 0)
-- Dependencies: 228
-- Name: __i_ent_b_rv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_ent_b_rv_id_seq OWNED BY public.__i_ent_b_rv.id;


--
-- TOC entry 229 (class 1259 OID 64886)
-- Name: __i_ent_c_uso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_ent_c_uso (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext,
    c_y public.citext,
    c_z public.citext,
    c_aa public.citext,
    c_ab public.citext,
    c_ac public.citext,
    c_ad public.citext,
    c_ae public.citext,
    c_af public.citext,
    c_ag public.citext,
    c_ah public.citext,
    c_ai public.citext,
    c_aj public.citext,
    c_ak public.citext
);


ALTER TABLE public.__i_ent_c_uso OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 64892)
-- Name: __i_ent_c_uso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_ent_c_uso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_ent_c_uso_id_seq OWNER TO postgres;

--
-- TOC entry 5000 (class 0 OID 0)
-- Dependencies: 230
-- Name: __i_ent_c_uso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_ent_c_uso_id_seq OWNED BY public.__i_ent_c_uso.id;


--
-- TOC entry 231 (class 1259 OID 64894)
-- Name: __i_ent_d_acao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_ent_d_acao (
    id integer NOT NULL,
    c_a public.citext,
    c_b public.citext,
    c_c public.citext,
    c_d public.citext,
    c_e public.citext,
    c_f public.citext,
    c_g public.citext,
    c_h public.citext,
    c_i public.citext,
    c_j public.citext,
    c_k public.citext,
    c_l public.citext,
    c_m public.citext,
    c_n public.citext,
    c_o public.citext,
    c_p public.citext,
    c_q public.citext,
    c_r public.citext,
    c_s public.citext,
    c_t public.citext,
    c_u public.citext,
    c_v public.citext,
    c_w public.citext,
    c_x public.citext,
    c_y public.citext,
    c_z public.citext
);


ALTER TABLE public.__i_ent_d_acao OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 64900)
-- Name: __i_ent_d_acao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_ent_d_acao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_ent_d_acao_id_seq OWNER TO postgres;

--
-- TOC entry 5002 (class 0 OID 0)
-- Dependencies: 232
-- Name: __i_ent_d_acao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_ent_d_acao_id_seq OWNED BY public.__i_ent_d_acao.id;


--
-- TOC entry 233 (class 1259 OID 64902)
-- Name: __i_ent_g_processo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_ent_g_processo (
    id integer NOT NULL,
    c1 public.citext,
    c2 public.citext,
    c3 public.citext,
    c4 public.citext,
    c5 public.citext,
    c6 public.citext,
    c7 public.citext,
    c8 public.citext,
    c9 public.citext,
    c10 public.citext,
    c11 public.citext,
    c12 public.citext,
    c13 public.citext,
    c14 public.citext,
    c15 public.citext,
    c16 public.citext,
    c17 public.citext,
    c18 public.citext,
    c19 public.citext,
    c20 public.citext,
    c21 public.citext,
    c22 public.citext,
    c23 public.citext,
    c24 public.citext,
    c25 public.citext,
    c26 public.citext,
    c27 public.citext,
    c28 public.citext,
    c29 public.citext,
    c30 public.citext,
    c31 public.citext,
    c32 public.citext,
    c33 public.citext,
    c34 public.citext,
    c35 public.citext,
    c36 public.citext,
    c37 public.citext,
    c38 public.citext,
    c39 public.citext,
    c40 public.citext,
    c41 public.citext,
    c42 public.citext,
    c43 public.citext,
    c44 public.citext,
    c45 public.citext,
    c46 public.citext,
    c47 public.citext,
    c48 public.citext,
    c49 public.citext,
    c50 public.citext,
    c51 public.citext,
    c52 public.citext,
    c53 public.citext,
    c54 public.citext,
    c55 public.citext,
    c56 public.citext,
    c57 public.citext,
    c58 public.citext,
    c59 public.citext,
    c60 public.citext,
    c61 public.citext,
    c62 public.citext,
    c63 public.citext,
    c64 public.citext,
    c65 public.citext,
    c66 public.citext,
    c67 public.citext,
    c68 public.citext,
    c69 public.citext,
    c70 public.citext,
    c71 public.citext,
    c72 public.citext
);


ALTER TABLE public.__i_ent_g_processo OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 64908)
-- Name: __i_ent_g_processo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_ent_g_processo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_ent_g_processo_id_seq OWNER TO postgres;

--
-- TOC entry 5004 (class 0 OID 0)
-- Dependencies: 234
-- Name: __i_ent_g_processo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_ent_g_processo_id_seq OWNED BY public.__i_ent_g_processo.id;


--
-- TOC entry 235 (class 1259 OID 64910)
-- Name: __i_ngi_cr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_ngi_cr (
    cnuc public.citext,
    ngi public.citext,
    cr public.citext
);


ALTER TABLE public.__i_ngi_cr OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 64916)
-- Name: __i_uc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_uc (
    ucid integer NOT NULL,
    c1 character varying(2500),
    c2 character varying(2500),
    c3 character varying(2500),
    c4 character varying(2500),
    c5 character varying(2500),
    c6 character varying(2500),
    c7 character varying(2500),
    c8 character varying(2500),
    c9 character varying(2500),
    c10 character varying(2500),
    c11 character varying(2500),
    c12 character varying(2500),
    c13 character varying(2500),
    c14 character varying(2500),
    c15 character varying(2500),
    c16 character varying(2500),
    c17 character varying(2500),
    c18 character varying(2500),
    c19 character varying(2500),
    c20 character varying(2500),
    c21 character varying(2500),
    c22 character varying(2500),
    c23 character varying(2500),
    c24 character varying(2500),
    c25 character varying(2500),
    c26 character varying(2500),
    c27 character varying(2500),
    c28 character varying(2500),
    c29 character varying(2500),
    c30 character varying(2500)
);


ALTER TABLE public.__i_uc OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 64922)
-- Name: __i_uc_cr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_uc_cr (
    __i_uc_crid integer NOT NULL,
    cnuc public.citext,
    cr public.citext
);


ALTER TABLE public.__i_uc_cr OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 64928)
-- Name: __i_uc_cr___i_uc_crid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_uc_cr___i_uc_crid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_uc_cr___i_uc_crid_seq OWNER TO postgres;

--
-- TOC entry 5008 (class 0 OID 0)
-- Dependencies: 238
-- Name: __i_uc_cr___i_uc_crid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_uc_cr___i_uc_crid_seq OWNED BY public.__i_uc_cr.__i_uc_crid;


--
-- TOC entry 239 (class 1259 OID 64930)
-- Name: __i_uc_grupo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__i_uc_grupo (
    id integer NOT NULL,
    c1 public.citext,
    c2 public.citext,
    c3 public.citext,
    c4 public.citext
);


ALTER TABLE public.__i_uc_grupo OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 64936)
-- Name: __i_uc_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.__i_uc_grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.__i_uc_grupo_id_seq OWNER TO postgres;

--
-- TOC entry 5010 (class 0 OID 0)
-- Dependencies: 240
-- Name: __i_uc_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.__i_uc_grupo_id_seq OWNED BY public.__i_uc_grupo.id;


--
-- TOC entry 241 (class 1259 OID 64938)
-- Name: __unidadeconservacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.__unidadeconservacao (
    categoriamanejoid integer NOT NULL,
    categoriaiucnid integer NOT NULL,
    situacao public.citext NOT NULL,
    tipoesfera public.citext NOT NULL,
    esfera public.citext NOT NULL,
    codigouc integer NOT NULL,
    cnuc public.citext NOT NULL,
    nome public.citext NOT NULL,
    ufs public.citext NOT NULL,
    anocriacao integer,
    atolegalcriacao public.citext,
    outrosatoslegais public.citext,
    municipios public.citext,
    templanomanejo public.citext,
    temconselhogestor public.citext,
    fontearea public.citext,
    areabioma numeric(14,4),
    areaatolegalcriacao numeric(14,4),
    areaamazonia numeric(14,4),
    areacaatinga numeric(14,4),
    areacerrado numeric(14,4),
    areamataatlantica numeric(14,4),
    areapampa numeric(14,4),
    areapantanal numeric(14,4),
    areamarinha numeric(14,4),
    biomaid integer
);


ALTER TABLE public.__unidadeconservacao OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 64944)
-- Name: _gc_auditoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_auditoria (
    _gc_auditoriaid integer NOT NULL,
    _gc_usuarioid integer,
    ip public.citext DEFAULT NULL::character varying,
    data timestamp(0) without time zone NOT NULL,
    tabela public.citext DEFAULT NULL::character varying,
    registroid integer,
    coluna public.citext DEFAULT NULL::character varying,
    valorantigo public.citext DEFAULT NULL::character varying,
    valornovo public.citext DEFAULT NULL::character varying,
    descricao public.citext NOT NULL
);


ALTER TABLE public._gc_auditoria OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 64955)
-- Name: _gc_auditoria__gc_auditoriaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._gc_auditoria__gc_auditoriaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._gc_auditoria__gc_auditoriaid_seq OWNER TO postgres;

--
-- TOC entry 5013 (class 0 OID 0)
-- Dependencies: 243
-- Name: _gc_auditoria__gc_auditoriaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._gc_auditoria__gc_auditoriaid_seq OWNED BY public._gc_auditoria._gc_auditoriaid;


--
-- TOC entry 244 (class 1259 OID 64957)
-- Name: _gc_metadados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_metadados (
    dado public.citext NOT NULL,
    idiomadosite public.citext DEFAULT 'pt_BR'::character varying NOT NULL,
    valor public.citext NOT NULL
);


ALTER TABLE public._gc_metadados OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 64964)
-- Name: _gc_paginaavulsa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_paginaavulsa (
    _gc_paginaavulsaid integer NOT NULL,
    codigopaginafixa public.citext DEFAULT NULL::character varying,
    situacao public.citext NOT NULL,
    titulo_ptbr public.citext DEFAULT NULL::character varying,
    slug_ptbr public.citext DEFAULT NULL::character varying,
    texto_ptbr public.citext
);


ALTER TABLE public._gc_paginaavulsa OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 64973)
-- Name: _gc_paginaavulsa__gc_paginaavulsaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._gc_paginaavulsa__gc_paginaavulsaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._gc_paginaavulsa__gc_paginaavulsaid_seq OWNER TO postgres;

--
-- TOC entry 5016 (class 0 OID 0)
-- Dependencies: 246
-- Name: _gc_paginaavulsa__gc_paginaavulsaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._gc_paginaavulsa__gc_paginaavulsaid_seq OWNED BY public._gc_paginaavulsa._gc_paginaavulsaid;


--
-- TOC entry 247 (class 1259 OID 64975)
-- Name: _gc_repositorio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_repositorio (
    _gc_repositorioid integer NOT NULL,
    datacriacao timestamp(0) without time zone NOT NULL,
    situacao public.citext NOT NULL,
    tipo public.citext NOT NULL,
    url public.citext NOT NULL,
    descricao public.citext DEFAULT NULL::character varying
);


ALTER TABLE public._gc_repositorio OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 64982)
-- Name: _gc_repositorio__gc_repositorioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._gc_repositorio__gc_repositorioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._gc_repositorio__gc_repositorioid_seq OWNER TO postgres;

--
-- TOC entry 5018 (class 0 OID 0)
-- Dependencies: 248
-- Name: _gc_repositorio__gc_repositorioid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._gc_repositorio__gc_repositorioid_seq OWNED BY public._gc_repositorio._gc_repositorioid;


--
-- TOC entry 249 (class 1259 OID 64984)
-- Name: _gc_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_usuario (
    _gc_usuarioid integer NOT NULL,
    login public.citext NOT NULL,
    email public.citext NOT NULL,
    senha public.citext NOT NULL,
    situacao public.citext NOT NULL,
    tokenrecuperarsenha public.citext DEFAULT NULL::character varying
);


ALTER TABLE public._gc_usuario OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 64991)
-- Name: _gc_usuario__gc_usuarioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._gc_usuario__gc_usuarioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._gc_usuario__gc_usuarioid_seq OWNER TO postgres;

--
-- TOC entry 5020 (class 0 OID 0)
-- Dependencies: 250
-- Name: _gc_usuario__gc_usuarioid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._gc_usuario__gc_usuarioid_seq OWNED BY public._gc_usuario._gc_usuarioid;


--
-- TOC entry 251 (class 1259 OID 64993)
-- Name: _gc_usuariologin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._gc_usuariologin (
    _gc_usuariologinid integer NOT NULL,
    _gc_usuarioid integer NOT NULL,
    datahora timestamp(0) without time zone NOT NULL,
    ip public.citext NOT NULL
);


ALTER TABLE public._gc_usuariologin OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 64999)
-- Name: _gc_usuariologin__gc_usuariologinid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._gc_usuariologin__gc_usuariologinid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._gc_usuariologin__gc_usuariologinid_seq OWNER TO postgres;

--
-- TOC entry 5022 (class 0 OID 0)
-- Dependencies: 252
-- Name: _gc_usuariologin__gc_usuariologinid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._gc_usuariologin__gc_usuariologinid_seq OWNED BY public._gc_usuariologin._gc_usuariologinid;


--
-- TOC entry 253 (class 1259 OID 65001)
-- Name: _m_acaomanejo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_acaomanejo (
    _m_acaomanejoid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    acao public.citext NOT NULL
);


ALTER TABLE public._m_acaomanejo OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 65007)
-- Name: _m_acaomanejo__m_acaomanejoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_acaomanejo__m_acaomanejoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_acaomanejo__m_acaomanejoid_seq OWNER TO postgres;

--
-- TOC entry 5024 (class 0 OID 0)
-- Dependencies: 254
-- Name: _m_acaomanejo__m_acaomanejoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_acaomanejo__m_acaomanejoid_seq OWNED BY public._m_acaomanejo._m_acaomanejoid;


--
-- TOC entry 255 (class 1259 OID 65009)
-- Name: _m_atividadeconsultoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_atividadeconsultoria (
    _m_atividadeconsultoriaid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    atividadeconsultoria public.citext NOT NULL
);


ALTER TABLE public._m_atividadeconsultoria OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 65015)
-- Name: _m_atividadeconsultoria__m_atividadeconsultoriaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_atividadeconsultoria__m_atividadeconsultoriaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_atividadeconsultoria__m_atividadeconsultoriaid_seq OWNER TO postgres;

--
-- TOC entry 5026 (class 0 OID 0)
-- Dependencies: 256
-- Name: _m_atividadeconsultoria__m_atividadeconsultoriaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_atividadeconsultoria__m_atividadeconsultoriaid_seq OWNED BY public._m_atividadeconsultoria._m_atividadeconsultoriaid;


--
-- TOC entry 257 (class 1259 OID 65017)
-- Name: _m_categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_categoria (
    _m_categoriaid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    categoria public.citext NOT NULL,
    nomereduzido public.citext
);


ALTER TABLE public._m_categoria OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 65023)
-- Name: _m_categoria__m_categoriaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_categoria__m_categoriaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_categoria__m_categoriaid_seq OWNER TO postgres;

--
-- TOC entry 5028 (class 0 OID 0)
-- Dependencies: 258
-- Name: _m_categoria__m_categoriaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_categoria__m_categoriaid_seq OWNED BY public._m_categoria._m_categoriaid;


--
-- TOC entry 259 (class 1259 OID 65025)
-- Name: _m_categoriaobjetivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_categoriaobjetivo (
    _m_categoriaobjetivoid integer NOT NULL,
    _m_categoriaid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    categoriaobjetivo public.citext NOT NULL
);


ALTER TABLE public._m_categoriaobjetivo OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 65031)
-- Name: _m_categoriaobjetivo__m_categoriaobjetivoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_categoriaobjetivo__m_categoriaobjetivoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_categoriaobjetivo__m_categoriaobjetivoid_seq OWNER TO postgres;

--
-- TOC entry 5030 (class 0 OID 0)
-- Dependencies: 260
-- Name: _m_categoriaobjetivo__m_categoriaobjetivoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_categoriaobjetivo__m_categoriaobjetivoid_seq OWNED BY public._m_categoriaobjetivo._m_categoriaobjetivoid;


--
-- TOC entry 261 (class 1259 OID 65033)
-- Name: _m_efetividade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_efetividade (
    _m_efetividadeid integer NOT NULL,
    valorinicial numeric(18,8),
    valorfinal numeric(18,8),
    posicao numeric(18,8),
    escala numeric(18,8)
);


ALTER TABLE public._m_efetividade OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 65036)
-- Name: _m_efetividade__m_efetividadeid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_efetividade__m_efetividadeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_efetividade__m_efetividadeid_seq OWNER TO postgres;

--
-- TOC entry 5032 (class 0 OID 0)
-- Dependencies: 262
-- Name: _m_efetividade__m_efetividadeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_efetividade__m_efetividadeid_seq OWNED BY public._m_efetividade._m_efetividadeid;


--
-- TOC entry 263 (class 1259 OID 65038)
-- Name: _m_identificacaopassos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_identificacaopassos (
    id integer NOT NULL,
    passo integer,
    identificacao character varying(25)
);


ALTER TABLE public._m_identificacaopassos OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 65041)
-- Name: _m_identificacaopassos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_identificacaopassos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_identificacaopassos_id_seq OWNER TO postgres;

--
-- TOC entry 5034 (class 0 OID 0)
-- Dependencies: 264
-- Name: _m_identificacaopassos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_identificacaopassos_id_seq OWNED BY public._m_identificacaopassos.id;


--
-- TOC entry 265 (class 1259 OID 65043)
-- Name: _m_instrumentoservico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_instrumentoservico (
    _m_instrumentoservicoid integer NOT NULL,
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    instrumentoservico public.citext NOT NULL
);


ALTER TABLE public._m_instrumentoservico OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 65049)
-- Name: _m_instrumentoservico__m_instrumentoservicoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_instrumentoservico__m_instrumentoservicoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_instrumentoservico__m_instrumentoservicoid_seq OWNER TO postgres;

--
-- TOC entry 5036 (class 0 OID 0)
-- Dependencies: 266
-- Name: _m_instrumentoservico__m_instrumentoservicoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_instrumentoservico__m_instrumentoservicoid_seq OWNED BY public._m_instrumentoservico._m_instrumentoservicoid;


--
-- TOC entry 267 (class 1259 OID 65051)
-- Name: _m_legalidademarcada; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_legalidademarcada (
    _m_legalidademarcadaid integer NOT NULL,
    situacao text NOT NULL,
    legalidade text NOT NULL,
    incentivadopermitido text,
    incentivadovedado text,
    incentivadoentorno text,
    permitidoincentivado text,
    permitidovedado text,
    permitidoentorno text,
    vedadopermitido text,
    vedadoincentivado text,
    vedadoentorno text,
    entornopermitido text,
    entornoincentivado text,
    entornovedado text
);


ALTER TABLE public._m_legalidademarcada OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 65057)
-- Name: _m_legalidademarcada__m_legalidademarcadaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_legalidademarcada__m_legalidademarcadaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_legalidademarcada__m_legalidademarcadaid_seq OWNER TO postgres;

--
-- TOC entry 5038 (class 0 OID 0)
-- Dependencies: 268
-- Name: _m_legalidademarcada__m_legalidademarcadaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_legalidademarcada__m_legalidademarcadaid_seq OWNED BY public._m_legalidademarcada._m_legalidademarcadaid;


--
-- TOC entry 269 (class 1259 OID 65059)
-- Name: _m_matriz; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_matriz (
    _m_matrizid integer NOT NULL,
    tipoid public.citext NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    valor public.citext NOT NULL,
    valor2 public.citext
);


ALTER TABLE public._m_matriz OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 65065)
-- Name: _m_matriz__m_matrizid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_matriz__m_matrizid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_matriz__m_matrizid_seq OWNER TO postgres;

--
-- TOC entry 5040 (class 0 OID 0)
-- Dependencies: 270
-- Name: _m_matriz__m_matrizid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_matriz__m_matrizid_seq OWNED BY public._m_matriz._m_matrizid;


--
-- TOC entry 271 (class 1259 OID 65067)
-- Name: _m_processo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_processo (
    _m_processoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    processo public.citext NOT NULL
);


ALTER TABLE public._m_processo OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 65073)
-- Name: _m_processo__m_processoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_processo__m_processoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_processo__m_processoid_seq OWNER TO postgres;

--
-- TOC entry 5042 (class 0 OID 0)
-- Dependencies: 272
-- Name: _m_processo__m_processoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_processo__m_processoid_seq OWNED BY public._m_processo._m_processoid;


--
-- TOC entry 273 (class 1259 OID 65075)
-- Name: _m_usoespecifico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_usoespecifico (
    _m_usoespecificoid integer NOT NULL,
    _m_usogenericoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    usoespecifico public.citext NOT NULL
);


ALTER TABLE public._m_usoespecifico OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 65081)
-- Name: _m_usoespecifico__m_usoespecificoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_usoespecifico__m_usoespecificoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_usoespecifico__m_usoespecificoid_seq OWNER TO postgres;

--
-- TOC entry 5044 (class 0 OID 0)
-- Dependencies: 274
-- Name: _m_usoespecifico__m_usoespecificoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_usoespecifico__m_usoespecificoid_seq OWNED BY public._m_usoespecifico._m_usoespecificoid;


--
-- TOC entry 275 (class 1259 OID 65083)
-- Name: _m_usoespecificocategoriamanejo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_usoespecificocategoriamanejo (
    _m_usoespecificocategoriamanejoid integer NOT NULL,
    _m_usoespecificoid integer NOT NULL,
    categoriamanejo public.citext,
    valor public.citext NOT NULL,
    _m_categoriaid integer
);


ALTER TABLE public._m_usoespecificocategoriamanejo OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 65089)
-- Name: _m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq OWNER TO postgres;

--
-- TOC entry 5046 (class 0 OID 0)
-- Dependencies: 276
-- Name: _m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq OWNED BY public._m_usoespecificocategoriamanejo._m_usoespecificocategoriamanejoid;


--
-- TOC entry 277 (class 1259 OID 65091)
-- Name: _m_usogenerico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._m_usogenerico (
    _m_usogenericoid integer NOT NULL,
    situacao public.citext NOT NULL,
    ordem integer,
    usogenerico public.citext NOT NULL
);


ALTER TABLE public._m_usogenerico OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 65097)
-- Name: _m_usogenerico__m_usogenericoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._m_usogenerico__m_usogenericoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._m_usogenerico__m_usogenericoid_seq OWNER TO postgres;

--
-- TOC entry 5048 (class 0 OID 0)
-- Dependencies: 278
-- Name: _m_usogenerico__m_usogenericoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._m_usogenerico__m_usogenericoid_seq OWNED BY public._m_usogenerico._m_usogenericoid;


--
-- TOC entry 279 (class 1259 OID 65099)
-- Name: _uc_ucid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._uc_ucid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._uc_ucid_seq OWNER TO postgres;

--
-- TOC entry 5049 (class 0 OID 0)
-- Dependencies: 279
-- Name: _uc_ucid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._uc_ucid_seq OWNED BY public.__i_uc.ucid;


--
-- TOC entry 280 (class 1259 OID 65101)
-- Name: anosamge; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.anosamge (
    anosamgeid integer NOT NULL,
    situacao public.citext NOT NULL,
    ano integer NOT NULL,
    datainicio timestamp(0) without time zone,
    datafim timestamp(0) without time zone
);


ALTER TABLE public.anosamge OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 65107)
-- Name: anosamge_anosamgeid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.anosamge_anosamgeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.anosamge_anosamgeid_seq OWNER TO postgres;

--
-- TOC entry 5051 (class 0 OID 0)
-- Dependencies: 281
-- Name: anosamge_anosamgeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.anosamge_anosamgeid_seq OWNED BY public.anosamge.anosamgeid;


--
-- TOC entry 282 (class 1259 OID 65109)
-- Name: banner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banner (
    bannerid integer NOT NULL,
    situacao public.citext,
    ordem integer,
    imagem public.citext,
    link public.citext,
    legenda public.citext
);


ALTER TABLE public.banner OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 65115)
-- Name: banner_bannerid_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banner_bannerid_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banner_bannerid_seq1 OWNER TO postgres;

--
-- TOC entry 5053 (class 0 OID 0)
-- Dependencies: 283
-- Name: banner_bannerid_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banner_bannerid_seq1 OWNED BY public.banner.bannerid;


--
-- TOC entry 284 (class 1259 OID 65117)
-- Name: cadastro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cadastro (
    cadastroid integer NOT NULL,
    cnuc public.citext,
    nome public.citext,
    email public.citext,
    cpf public.citext,
    senha public.citext,
    situacao public.citext,
    permissao_administrador public.citext,
    permissao_federal public.citext,
    token_recuperarsenha public.citext,
    id_perfil integer
);


ALTER TABLE public.cadastro OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 65123)
-- Name: cadastro_cadastroid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cadastro_cadastroid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cadastro_cadastroid_seq OWNER TO postgres;

--
-- TOC entry 5055 (class 0 OID 0)
-- Dependencies: 285
-- Name: cadastro_cadastroid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cadastro_cadastroid_seq OWNED BY public.cadastro.cadastroid;


--
-- TOC entry 286 (class 1259 OID 65125)
-- Name: cadastro_cnuc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cadastro_cnuc (
    cadastro_cnucid integer NOT NULL,
    cadastroid public.citext,
    cnuc public.citext
);


ALTER TABLE public.cadastro_cnuc OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 65131)
-- Name: cadastro_cnuc_cadastro_cnucid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cadastro_cnuc_cadastro_cnucid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cadastro_cnuc_cadastro_cnucid_seq OWNER TO postgres;

--
-- TOC entry 5057 (class 0 OID 0)
-- Dependencies: 287
-- Name: cadastro_cnuc_cadastro_cnucid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cadastro_cnuc_cadastro_cnucid_seq OWNED BY public.cadastro_cnuc.cadastro_cnucid;


--
-- TOC entry 288 (class 1259 OID 65133)
-- Name: cadastro_cr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cadastro_cr (
    cadastro_crid integer NOT NULL,
    cadastroid integer NOT NULL,
    cr public.citext NOT NULL
);


ALTER TABLE public.cadastro_cr OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 65139)
-- Name: cadastro_cr_cadastro_crid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cadastro_cr_cadastro_crid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cadastro_cr_cadastro_crid_seq OWNER TO postgres;

--
-- TOC entry 5059 (class 0 OID 0)
-- Dependencies: 289
-- Name: cadastro_cr_cadastro_crid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cadastro_cr_cadastro_crid_seq OWNED BY public.cadastro_cr.cadastro_crid;


--
-- TOC entry 290 (class 1259 OID 65141)
-- Name: cadastro_uf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cadastro_uf (
    cadastro_ufid integer NOT NULL,
    cadastroid integer NOT NULL,
    uf public.citext NOT NULL
);


ALTER TABLE public.cadastro_uf OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 65147)
-- Name: cadastro_uf_cadastro_ufid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cadastro_uf_cadastro_ufid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cadastro_uf_cadastro_ufid_seq OWNER TO postgres;

--
-- TOC entry 5061 (class 0 OID 0)
-- Dependencies: 291
-- Name: cadastro_uf_cadastro_ufid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cadastro_uf_cadastro_ufid_seq OWNED BY public.cadastro_uf.cadastro_ufid;


--
-- TOC entry 292 (class 1259 OID 65149)
-- Name: entrada; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entrada (
    entradaid integer NOT NULL,
    unidadeconservacaoid integer NOT NULL,
    situacao public.citext NOT NULL,
    anoreferencia integer NOT NULL,
    datapreenchimento timestamp(0) without time zone,
    dataatualizacao timestamp(0) without time zone,
    quempreenche public.citext,
    preenchedores public.citext,
    salvoupasso1 timestamp(0) without time zone,
    salvoupasso2 timestamp(0) without time zone,
    salvoupasso3 timestamp(0) without time zone,
    salvoupasso4 timestamp(0) without time zone,
    salvoupasso5 timestamp(0) without time zone,
    salvoupasso6 timestamp(0) without time zone,
    salvoupasso7 timestamp(0) without time zone,
    calculoresultados numeric(18,8),
    calculoprodutos numeric(18,8),
    calculocontexto numeric(18,8),
    calculoplanejamento numeric(18,8),
    calculoinsumos numeric(18,8),
    calculoprocessos numeric(18,8),
    calculoresultadosgrafico numeric(18,8),
    calculoprodutosgrafico numeric(18,8),
    calculocontextografico numeric(18,8),
    calculoplanejamentografico numeric(18,8),
    calculoinsumosgrafico numeric(18,8),
    calculoprocessosgrafico numeric(18,8),
    calculoefetividade numeric(18,8),
    justificativavalidacao public.citext,
    tipovalidacao integer
);


ALTER TABLE public.entrada OWNER TO postgres;

--
-- TOC entry 5062 (class 0 OID 0)
-- Dependencies: 292
-- Name: COLUMN entrada.tipovalidacao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.entrada.tipovalidacao IS '0 ou vazio -> Não Validado
1 -> Parcialmente validado pela Coordenação Regional
2 -> Validado pela Coordenação Regional';


--
-- TOC entry 293 (class 1259 OID 65155)
-- Name: entrada_entradaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entrada_entradaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entrada_entradaid_seq OWNER TO postgres;

--
-- TOC entry 5064 (class 0 OID 0)
-- Dependencies: 293
-- Name: entrada_entradaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entrada_entradaid_seq OWNED BY public.entrada.entradaid;


--
-- TOC entry 294 (class 1259 OID 65157)
-- Name: entradaacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradaacao (
    entradaacaoid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_acaomanejoid integer NOT NULL,
    _m_instrumentoamparaacaoid integer,
    situacao public.citext NOT NULL,
    referencia public.citext NOT NULL,
    acaodescricao public.citext,
    pessoal public.citext,
    capacidade public.citext,
    equipamento public.citext,
    recursofinanceiro public.citext,
    grauapoionecessario public.citext,
    tipoapoionecessario public.citext,
    origemapoio public.citext,
    governabilidade public.citext,
    apoioaoprocesso public.citext,
    esforco integer,
    consolidacaoprocesso public.citext,
    calculo numeric(18,8),
    calculoprocesso numeric(18,8),
    calculoprioridade numeric(18,8),
    referenciaprocesso public.citext,
    calculoordem numeric(18,8),
    datainsercao timestamp without time zone,
    dataatualizacao timestamp without time zone,
    situacaoexecucao public.citext,
    referenciaitem integer
);


ALTER TABLE public.entradaacao OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 65163)
-- Name: entradaacao_entradaacaoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradaacao_entradaacaoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradaacao_entradaacaoid_seq OWNER TO postgres;

--
-- TOC entry 5066 (class 0 OID 0)
-- Dependencies: 295
-- Name: entradaacao_entradaacaoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradaacao_entradaacaoid_seq OWNED BY public.entradaacao.entradaacaoid;


--
-- TOC entry 296 (class 1259 OID 65165)
-- Name: entradarv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradarv (
    entradarvid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_alvosid integer,
    situacao public.citext NOT NULL,
    referencia public.citext,
    objetivoclasse public.citext NOT NULL,
    objetivodescricao public.citext NOT NULL,
    alvonome public.citext,
    conservacaointervencao public.citext NOT NULL,
    danoanterior public.citext,
    fator public.citext,
    naturalseminatural public.citext,
    fonterv public.citext,
    calculoprioridade_usosrelacionados numeric(18,8),
    entradarv public.citext,
    imagem public.citext,
    referenciaitem integer
);


ALTER TABLE public.entradarv OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 65171)
-- Name: entradarv_entradarvid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradarv_entradarvid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradarv_entradarvid_seq OWNER TO postgres;

--
-- TOC entry 5068 (class 0 OID 0)
-- Dependencies: 297
-- Name: entradarv_entradarvid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradarv_entradarvid_seq OWNED BY public.entradarv.entradarvid;


--
-- TOC entry 298 (class 1259 OID 65173)
-- Name: entradarvacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradarvacao (
    entradarvacaoid integer NOT NULL,
    entradarvid integer NOT NULL,
    entradaacaoid integer NOT NULL,
    ordem integer
);


ALTER TABLE public.entradarvacao OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 65176)
-- Name: entradarvacao_entradarvacaoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradarvacao_entradarvacaoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradarvacao_entradarvacaoid_seq OWNER TO postgres;

--
-- TOC entry 5070 (class 0 OID 0)
-- Dependencies: 299
-- Name: entradarvacao_entradarvacaoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradarvacao_entradarvacaoid_seq OWNED BY public.entradarvacao.entradarvacaoid;


--
-- TOC entry 300 (class 1259 OID 65178)
-- Name: entradarvuso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradarvuso (
    entradarvusoid integer NOT NULL,
    entradarvid integer NOT NULL,
    entradausoid integer NOT NULL,
    ordem integer
);


ALTER TABLE public.entradarvuso OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 65181)
-- Name: entradarvuso_entradarvusoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradarvuso_entradarvusoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradarvuso_entradarvusoid_seq OWNER TO postgres;

--
-- TOC entry 5072 (class 0 OID 0)
-- Dependencies: 301
-- Name: entradarvuso_entradarvusoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradarvuso_entradarvusoid_seq OWNED BY public.entradarvuso.entradarvusoid;


--
-- TOC entry 302 (class 1259 OID 65183)
-- Name: entradauso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradauso (
    entradausoid integer NOT NULL,
    entradaid integer NOT NULL,
    _m_usoespecificoid integer NOT NULL,
    _m_legalidademarcadaid integer,
    situacao public.citext NOT NULL,
    referencia public.citext NOT NULL,
    descricao public.citext,
    legalidademarcada public.citext NOT NULL,
    legalidade public.citext NOT NULL,
    tbc smallint,
    ip_ec_individual smallint,
    ip_ec_entorno smallint,
    ip_ec_sociedade smallint,
    ip_so_individual smallint,
    ip_so_entorno smallint,
    ip_so_sociedade smallint,
    ip_co_populacoes smallint,
    ip_co_especies smallint,
    ip_co_rv smallint,
    ip_ma_uso smallint,
    ip_ma_unidade smallint,
    ip_ma_sistema smallint,
    in_severidade smallint,
    in_magnitude smallint,
    in_irreversibilidade smallint,
    calculo numeric(18,8),
    voluntariado public.citext,
    calculoprioridade numeric(18,8),
    legalidadecorrigida public.citext,
    datainsercao timestamp without time zone,
    dataatualizacao timestamp without time zone,
    referenciaitem integer
);


ALTER TABLE public.entradauso OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 65189)
-- Name: entradauso_entradausoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradauso_entradausoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradauso_entradausoid_seq OWNER TO postgres;

--
-- TOC entry 5074 (class 0 OID 0)
-- Dependencies: 303
-- Name: entradauso_entradausoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradauso_entradausoid_seq OWNED BY public.entradauso.entradausoid;


--
-- TOC entry 304 (class 1259 OID 65191)
-- Name: entradausoacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradausoacao (
    entradausoacaoid integer NOT NULL,
    entradausoid integer NOT NULL,
    entradaacaoid integer NOT NULL,
    ordem integer
);


ALTER TABLE public.entradausoacao OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 65194)
-- Name: entradausoacao_entradausoacaoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradausoacao_entradausoacaoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradausoacao_entradausoacaoid_seq OWNER TO postgres;

--
-- TOC entry 5076 (class 0 OID 0)
-- Dependencies: 305
-- Name: entradausoacao_entradausoacaoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradausoacao_entradausoacaoid_seq OWNED BY public.entradausoacao.entradausoacaoid;


--
-- TOC entry 306 (class 1259 OID 65196)
-- Name: nivelacessoporperfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivelacessoporperfil (
    id integer NOT NULL,
    nivel integer NOT NULL,
    id_perfil integer
);


ALTER TABLE public.nivelacessoporperfil OWNER TO postgres;

--
-- TOC entry 5077 (class 0 OID 0)
-- Dependencies: 306
-- Name: TABLE nivelacessoporperfil; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.nivelacessoporperfil IS 'NIVEL 1	
//Visualização dos Indicadores e relatórios das UC - Nível ICMBio
//Visualização das Camadas geoespaciais - SNUC

NIVEL 2
//Visualização dos Indicadores e relatórios das UC Estaduais/Municipais/RPPN

NIVEL 3
//Visualização das Camadas geoespaciais - Estados/Municípios/RPPN

NIVEL 4
//Edição/Preenchimento do formulário da sua UC
//Validação do preenchimento - nível gestor UC

NIVEL 5
//Validação do preenchimento - nível CR/MMA/OEMA

NIVEL 6
//Download do arquivo excel completo
//Visualização dos formulários preenchidos';


--
-- TOC entry 307 (class 1259 OID 65199)
-- Name: nivelacessoporperfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivelacessoporperfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivelacessoporperfil_id_seq OWNER TO postgres;

--
-- TOC entry 5078 (class 0 OID 0)
-- Dependencies: 307
-- Name: nivelacessoporperfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivelacessoporperfil_id_seq OWNED BY public.nivelacessoporperfil.id;


--
-- TOC entry 308 (class 1259 OID 65201)
-- Name: parceiro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parceiro (
    parceiroid integer NOT NULL,
    situacao public.citext,
    imagem public.citext,
    link public.citext,
    tipo public.citext
);


ALTER TABLE public.parceiro OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 65207)
-- Name: parceiro_parceiroid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.parceiro_parceiroid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parceiro_parceiroid_seq OWNER TO postgres;

--
-- TOC entry 5080 (class 0 OID 0)
-- Dependencies: 309
-- Name: parceiro_parceiroid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.parceiro_parceiroid_seq OWNED BY public.parceiro.parceiroid;


--
-- TOC entry 310 (class 1259 OID 65209)
-- Name: passos_anotacoes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.passos_anotacoes (
    passos_anotacoesid integer NOT NULL,
    entradaid integer NOT NULL,
    passo integer NOT NULL,
    declaracao public.citext,
    consideracoes public.citext,
    datavalidacao timestamp(0) without time zone,
    anotacao public.citext,
    dataanotacao timestamp(0) without time zone,
    id_cadastro integer,
    item integer
);


ALTER TABLE public.passos_anotacoes OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 65215)
-- Name: passos_anotacoes_passos_anotacoesid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.passos_anotacoes_passos_anotacoesid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.passos_anotacoes_passos_anotacoesid_seq OWNER TO postgres;

--
-- TOC entry 5082 (class 0 OID 0)
-- Dependencies: 311
-- Name: passos_anotacoes_passos_anotacoesid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.passos_anotacoes_passos_anotacoesid_seq OWNED BY public.passos_anotacoes.passos_anotacoesid;


--
-- TOC entry 312 (class 1259 OID 65217)
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.perfil (
    id integer NOT NULL,
    nome public.citext
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 65223)
-- Name: perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perfil_id_seq OWNER TO postgres;

--
-- TOC entry 5083 (class 0 OID 0)
-- Dependencies: 313
-- Name: perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.perfil_id_seq OWNED BY public.perfil.id;


--
-- TOC entry 314 (class 1259 OID 65225)
-- Name: relatorio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.relatorio (
    relatorioid integer NOT NULL,
    situacao public.citext,
    ano public.citext,
    excel public.citext,
    pdf public.citext,
    imagem public.citext
);


ALTER TABLE public.relatorio OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 65231)
-- Name: relatorio_relatorioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.relatorio_relatorioid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relatorio_relatorioid_seq OWNER TO postgres;

--
-- TOC entry 5085 (class 0 OID 0)
-- Dependencies: 315
-- Name: relatorio_relatorioid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.relatorio_relatorioid_seq OWNED BY public.relatorio.relatorioid;


--
-- TOC entry 318 (class 1259 OID 94447)
-- Name: sequence_generator; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sequence_generator
    START WITH 1050
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_generator OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 65241)
-- Name: urlpowerbi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.urlpowerbi (
    id integer NOT NULL,
    url public.citext NOT NULL,
    situacao public.citext,
    painel public.citext
);


ALTER TABLE public.urlpowerbi OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 65247)
-- Name: urlpowerbi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.urlpowerbi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.urlpowerbi_id_seq OWNER TO postgres;

--
-- TOC entry 5086 (class 0 OID 0)
-- Dependencies: 317
-- Name: urlpowerbi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.urlpowerbi_id_seq OWNED BY public.urlpowerbi.id;


--
-- TOC entry 4645 (class 2604 OID 65249)
-- Name: espacializacao id; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.espacializacao ALTER COLUMN id SET DEFAULT nextval('geom.espacializacao_id_seq'::regclass);


--
-- TOC entry 4646 (class 2604 OID 65250)
-- Name: estados gid; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.estados ALTER COLUMN gid SET DEFAULT nextval('geom.estados_gid_seq'::regclass);


--
-- TOC entry 4647 (class 2604 OID 65251)
-- Name: municipios id; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.municipios ALTER COLUMN id SET DEFAULT nextval('geom.municipios_id_seq'::regclass);


--
-- TOC entry 4648 (class 2604 OID 65252)
-- Name: ucs_brasil gid; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.ucs_brasil ALTER COLUMN gid SET DEFAULT nextval('geom.ucs_brasil_gid_seq'::regclass);


--
-- TOC entry 4649 (class 2604 OID 65253)
-- Name: unidades_conservacao_shp gid; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp ALTER COLUMN gid SET DEFAULT nextval('geom.unidades_conservacao_shp_new_ogc_fid_seq1'::regclass);


--
-- TOC entry 4650 (class 2604 OID 65254)
-- Name: unidades_conservacao_shp_old gid; Type: DEFAULT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp_old ALTER COLUMN gid SET DEFAULT nextval('geom.unidades_conservacao_shp_new_ogc_fid_seq'::regclass);


--
-- TOC entry 4651 (class 2604 OID 65255)
-- Name: __i_ent_b_rv id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_b_rv ALTER COLUMN id SET DEFAULT nextval('public.__i_ent_b_rv_id_seq'::regclass);


--
-- TOC entry 4652 (class 2604 OID 65256)
-- Name: __i_ent_c_uso id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_c_uso ALTER COLUMN id SET DEFAULT nextval('public.__i_ent_c_uso_id_seq'::regclass);


--
-- TOC entry 4653 (class 2604 OID 65257)
-- Name: __i_ent_d_acao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_d_acao ALTER COLUMN id SET DEFAULT nextval('public.__i_ent_d_acao_id_seq'::regclass);


--
-- TOC entry 4654 (class 2604 OID 65258)
-- Name: __i_ent_g_processo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_g_processo ALTER COLUMN id SET DEFAULT nextval('public.__i_ent_g_processo_id_seq'::regclass);


--
-- TOC entry 4655 (class 2604 OID 65259)
-- Name: __i_uc ucid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc ALTER COLUMN ucid SET DEFAULT nextval('public._uc_ucid_seq'::regclass);


--
-- TOC entry 4656 (class 2604 OID 65260)
-- Name: __i_uc_cr __i_uc_crid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_cr ALTER COLUMN __i_uc_crid SET DEFAULT nextval('public.__i_uc_cr___i_uc_crid_seq'::regclass);


--
-- TOC entry 4657 (class 2604 OID 65261)
-- Name: __i_uc_grupo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_grupo ALTER COLUMN id SET DEFAULT nextval('public.__i_uc_grupo_id_seq'::regclass);


--
-- TOC entry 4658 (class 2604 OID 65262)
-- Name: _gc_auditoria _gc_auditoriaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_auditoria ALTER COLUMN _gc_auditoriaid SET DEFAULT nextval('public._gc_auditoria__gc_auditoriaid_seq'::regclass);


--
-- TOC entry 4665 (class 2604 OID 65263)
-- Name: _gc_paginaavulsa _gc_paginaavulsaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_paginaavulsa ALTER COLUMN _gc_paginaavulsaid SET DEFAULT nextval('public._gc_paginaavulsa__gc_paginaavulsaid_seq'::regclass);


--
-- TOC entry 4669 (class 2604 OID 65264)
-- Name: _gc_repositorio _gc_repositorioid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_repositorio ALTER COLUMN _gc_repositorioid SET DEFAULT nextval('public._gc_repositorio__gc_repositorioid_seq'::regclass);


--
-- TOC entry 4671 (class 2604 OID 65265)
-- Name: _gc_usuario _gc_usuarioid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuario ALTER COLUMN _gc_usuarioid SET DEFAULT nextval('public._gc_usuario__gc_usuarioid_seq'::regclass);


--
-- TOC entry 4673 (class 2604 OID 65266)
-- Name: _gc_usuariologin _gc_usuariologinid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuariologin ALTER COLUMN _gc_usuariologinid SET DEFAULT nextval('public._gc_usuariologin__gc_usuariologinid_seq'::regclass);


--
-- TOC entry 4674 (class 2604 OID 65267)
-- Name: _m_acaomanejo _m_acaomanejoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_acaomanejo ALTER COLUMN _m_acaomanejoid SET DEFAULT nextval('public._m_acaomanejo__m_acaomanejoid_seq'::regclass);


--
-- TOC entry 4675 (class 2604 OID 65268)
-- Name: _m_atividadeconsultoria _m_atividadeconsultoriaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_atividadeconsultoria ALTER COLUMN _m_atividadeconsultoriaid SET DEFAULT nextval('public._m_atividadeconsultoria__m_atividadeconsultoriaid_seq'::regclass);


--
-- TOC entry 4676 (class 2604 OID 65269)
-- Name: _m_categoria _m_categoriaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoria ALTER COLUMN _m_categoriaid SET DEFAULT nextval('public._m_categoria__m_categoriaid_seq'::regclass);


--
-- TOC entry 4677 (class 2604 OID 65270)
-- Name: _m_categoriaobjetivo _m_categoriaobjetivoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoriaobjetivo ALTER COLUMN _m_categoriaobjetivoid SET DEFAULT nextval('public._m_categoriaobjetivo__m_categoriaobjetivoid_seq'::regclass);


--
-- TOC entry 4678 (class 2604 OID 65271)
-- Name: _m_efetividade _m_efetividadeid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_efetividade ALTER COLUMN _m_efetividadeid SET DEFAULT nextval('public._m_efetividade__m_efetividadeid_seq'::regclass);


--
-- TOC entry 4679 (class 2604 OID 65272)
-- Name: _m_identificacaopassos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_identificacaopassos ALTER COLUMN id SET DEFAULT nextval('public._m_identificacaopassos_id_seq'::regclass);


--
-- TOC entry 4680 (class 2604 OID 65273)
-- Name: _m_instrumentoservico _m_instrumentoservicoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_instrumentoservico ALTER COLUMN _m_instrumentoservicoid SET DEFAULT nextval('public._m_instrumentoservico__m_instrumentoservicoid_seq'::regclass);


--
-- TOC entry 4681 (class 2604 OID 65274)
-- Name: _m_legalidademarcada _m_legalidademarcadaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_legalidademarcada ALTER COLUMN _m_legalidademarcadaid SET DEFAULT nextval('public._m_legalidademarcada__m_legalidademarcadaid_seq'::regclass);


--
-- TOC entry 4682 (class 2604 OID 65275)
-- Name: _m_matriz _m_matrizid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_matriz ALTER COLUMN _m_matrizid SET DEFAULT nextval('public._m_matriz__m_matrizid_seq'::regclass);


--
-- TOC entry 4683 (class 2604 OID 65276)
-- Name: _m_processo _m_processoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_processo ALTER COLUMN _m_processoid SET DEFAULT nextval('public._m_processo__m_processoid_seq'::regclass);


--
-- TOC entry 4684 (class 2604 OID 65277)
-- Name: _m_usoespecifico _m_usoespecificoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecifico ALTER COLUMN _m_usoespecificoid SET DEFAULT nextval('public._m_usoespecifico__m_usoespecificoid_seq'::regclass);


--
-- TOC entry 4685 (class 2604 OID 65278)
-- Name: _m_usoespecificocategoriamanejo _m_usoespecificocategoriamanejoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecificocategoriamanejo ALTER COLUMN _m_usoespecificocategoriamanejoid SET DEFAULT nextval('public._m_usoespecificocategoriamane__m_usoespecificocategoriamane_seq'::regclass);


--
-- TOC entry 4686 (class 2604 OID 65279)
-- Name: _m_usogenerico _m_usogenericoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usogenerico ALTER COLUMN _m_usogenericoid SET DEFAULT nextval('public._m_usogenerico__m_usogenericoid_seq'::regclass);


--
-- TOC entry 4687 (class 2604 OID 65280)
-- Name: anosamge anosamgeid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anosamge ALTER COLUMN anosamgeid SET DEFAULT nextval('public.anosamge_anosamgeid_seq'::regclass);


--
-- TOC entry 4688 (class 2604 OID 65281)
-- Name: banner bannerid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banner ALTER COLUMN bannerid SET DEFAULT nextval('public.banner_bannerid_seq1'::regclass);


--
-- TOC entry 4689 (class 2604 OID 65282)
-- Name: cadastro cadastroid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro ALTER COLUMN cadastroid SET DEFAULT nextval('public.cadastro_cadastroid_seq'::regclass);


--
-- TOC entry 4690 (class 2604 OID 65283)
-- Name: cadastro_cnuc cadastro_cnucid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cnuc ALTER COLUMN cadastro_cnucid SET DEFAULT nextval('public.cadastro_cnuc_cadastro_cnucid_seq'::regclass);


--
-- TOC entry 4691 (class 2604 OID 65284)
-- Name: cadastro_cr cadastro_crid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cr ALTER COLUMN cadastro_crid SET DEFAULT nextval('public.cadastro_cr_cadastro_crid_seq'::regclass);


--
-- TOC entry 4692 (class 2604 OID 65285)
-- Name: cadastro_uf cadastro_ufid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_uf ALTER COLUMN cadastro_ufid SET DEFAULT nextval('public.cadastro_uf_cadastro_ufid_seq'::regclass);


--
-- TOC entry 4693 (class 2604 OID 65286)
-- Name: entrada entradaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entrada ALTER COLUMN entradaid SET DEFAULT nextval('public.entrada_entradaid_seq'::regclass);


--
-- TOC entry 4694 (class 2604 OID 65287)
-- Name: entradaacao entradaacaoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao ALTER COLUMN entradaacaoid SET DEFAULT nextval('public.entradaacao_entradaacaoid_seq'::regclass);


--
-- TOC entry 4695 (class 2604 OID 65288)
-- Name: entradarv entradarvid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarv ALTER COLUMN entradarvid SET DEFAULT nextval('public.entradarv_entradarvid_seq'::regclass);


--
-- TOC entry 4696 (class 2604 OID 65289)
-- Name: entradarvacao entradarvacaoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao ALTER COLUMN entradarvacaoid SET DEFAULT nextval('public.entradarvacao_entradarvacaoid_seq'::regclass);


--
-- TOC entry 4697 (class 2604 OID 65290)
-- Name: entradarvuso entradarvusoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso ALTER COLUMN entradarvusoid SET DEFAULT nextval('public.entradarvuso_entradarvusoid_seq'::regclass);


--
-- TOC entry 4698 (class 2604 OID 65291)
-- Name: entradauso entradausoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso ALTER COLUMN entradausoid SET DEFAULT nextval('public.entradauso_entradausoid_seq'::regclass);


--
-- TOC entry 4699 (class 2604 OID 65292)
-- Name: entradausoacao entradausoacaoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao ALTER COLUMN entradausoacaoid SET DEFAULT nextval('public.entradausoacao_entradausoacaoid_seq'::regclass);


--
-- TOC entry 4700 (class 2604 OID 65293)
-- Name: nivelacessoporperfil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivelacessoporperfil ALTER COLUMN id SET DEFAULT nextval('public.nivelacessoporperfil_id_seq'::regclass);


--
-- TOC entry 4701 (class 2604 OID 65294)
-- Name: parceiro parceiroid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parceiro ALTER COLUMN parceiroid SET DEFAULT nextval('public.parceiro_parceiroid_seq'::regclass);


--
-- TOC entry 4702 (class 2604 OID 65295)
-- Name: passos_anotacoes passos_anotacoesid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.passos_anotacoes ALTER COLUMN passos_anotacoesid SET DEFAULT nextval('public.passos_anotacoes_passos_anotacoesid_seq'::regclass);


--
-- TOC entry 4703 (class 2604 OID 65296)
-- Name: perfil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perfil ALTER COLUMN id SET DEFAULT nextval('public.perfil_id_seq'::regclass);


--
-- TOC entry 4704 (class 2604 OID 65297)
-- Name: relatorio relatorioid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relatorio ALTER COLUMN relatorioid SET DEFAULT nextval('public.relatorio_relatorioid_seq'::regclass);


--
-- TOC entry 4705 (class 2604 OID 65299)
-- Name: urlpowerbi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urlpowerbi ALTER COLUMN id SET DEFAULT nextval('public.urlpowerbi_id_seq'::regclass);


--
-- TOC entry 4710 (class 2606 OID 88879)
-- Name: espacializacao espacializacao_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.espacializacao
    ADD CONSTRAINT espacializacao_pkey PRIMARY KEY (id);


--
-- TOC entry 4712 (class 2606 OID 88881)
-- Name: estados estados_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.estados
    ADD CONSTRAINT estados_pkey PRIMARY KEY (gid);


--
-- TOC entry 4714 (class 2606 OID 88883)
-- Name: municipios municipios_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.municipios
    ADD CONSTRAINT municipios_pkey PRIMARY KEY (id);


--
-- TOC entry 4716 (class 2606 OID 88885)
-- Name: ucs_brasil ucs_brasil_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.ucs_brasil
    ADD CONSTRAINT ucs_brasil_pkey PRIMARY KEY (gid);


--
-- TOC entry 4721 (class 2606 OID 88887)
-- Name: unidades_conservacao_shp_old unidades_conservacao_shp_new_pkey; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp_old
    ADD CONSTRAINT unidades_conservacao_shp_new_pkey PRIMARY KEY (gid);


--
-- TOC entry 4719 (class 2606 OID 88889)
-- Name: unidades_conservacao_shp unidades_conservacao_shp_new_pkey1; Type: CONSTRAINT; Schema: geom; Owner: postgres
--

ALTER TABLE ONLY geom.unidades_conservacao_shp
    ADD CONSTRAINT unidades_conservacao_shp_new_pkey1 PRIMARY KEY (gid);


--
-- TOC entry 4724 (class 2606 OID 88891)
-- Name: Teste_BD_SAMGe Teste_BD_SAMGe_pkey; Type: CONSTRAINT; Schema: public; Owner: samge_read
--

ALTER TABLE ONLY public."Teste_BD_SAMGe"
    ADD CONSTRAINT "Teste_BD_SAMGe_pkey" PRIMARY KEY (id);


--
-- TOC entry 4726 (class 2606 OID 88893)
-- Name: __i_ent_b_rv __i_ent_b_rv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_b_rv
    ADD CONSTRAINT __i_ent_b_rv_pkey PRIMARY KEY (id);


--
-- TOC entry 4728 (class 2606 OID 88895)
-- Name: __i_ent_c_uso __i_ent_c_uso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_c_uso
    ADD CONSTRAINT __i_ent_c_uso_pkey PRIMARY KEY (id);


--
-- TOC entry 4730 (class 2606 OID 88897)
-- Name: __i_ent_d_acao __i_ent_d_acao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_d_acao
    ADD CONSTRAINT __i_ent_d_acao_pkey PRIMARY KEY (id);


--
-- TOC entry 4732 (class 2606 OID 88899)
-- Name: __i_ent_g_processo __i_ent_g_processo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_ent_g_processo
    ADD CONSTRAINT __i_ent_g_processo_pkey PRIMARY KEY (id);


--
-- TOC entry 4736 (class 2606 OID 88901)
-- Name: __i_uc_cr __i_uc_cr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_cr
    ADD CONSTRAINT __i_uc_cr_pkey PRIMARY KEY (__i_uc_crid);


--
-- TOC entry 4738 (class 2606 OID 88903)
-- Name: __i_uc_grupo __i_uc_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc_grupo
    ADD CONSTRAINT __i_uc_grupo_pkey PRIMARY KEY (id);


--
-- TOC entry 4740 (class 2606 OID 88905)
-- Name: _gc_auditoria _gc_auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_auditoria
    ADD CONSTRAINT _gc_auditoria_pkey PRIMARY KEY (_gc_auditoriaid);


--
-- TOC entry 4746 (class 2606 OID 88907)
-- Name: _gc_metadados _gc_metadados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_metadados
    ADD CONSTRAINT _gc_metadados_pkey PRIMARY KEY (dado, idiomadosite);


--
-- TOC entry 4748 (class 2606 OID 88909)
-- Name: _gc_paginaavulsa _gc_paginaavulsa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_paginaavulsa
    ADD CONSTRAINT _gc_paginaavulsa_pkey PRIMARY KEY (_gc_paginaavulsaid);


--
-- TOC entry 4751 (class 2606 OID 88911)
-- Name: _gc_repositorio _gc_repositorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_repositorio
    ADD CONSTRAINT _gc_repositorio_pkey PRIMARY KEY (_gc_repositorioid);


--
-- TOC entry 4754 (class 2606 OID 88913)
-- Name: _gc_usuario _gc_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuario
    ADD CONSTRAINT _gc_usuario_pkey PRIMARY KEY (_gc_usuarioid);


--
-- TOC entry 4756 (class 2606 OID 88915)
-- Name: _gc_usuariologin _gc_usuariologin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuariologin
    ADD CONSTRAINT _gc_usuariologin_pkey PRIMARY KEY (_gc_usuariologinid);


--
-- TOC entry 4758 (class 2606 OID 88917)
-- Name: _m_acaomanejo _m_acaomanejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_acaomanejo
    ADD CONSTRAINT _m_acaomanejo_pkey PRIMARY KEY (_m_acaomanejoid);


--
-- TOC entry 4762 (class 2606 OID 88919)
-- Name: _m_atividadeconsultoria _m_atividadeconsultoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_atividadeconsultoria
    ADD CONSTRAINT _m_atividadeconsultoria_pkey PRIMARY KEY (_m_atividadeconsultoriaid);


--
-- TOC entry 4764 (class 2606 OID 88921)
-- Name: _m_categoria _m_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoria
    ADD CONSTRAINT _m_categoria_pkey PRIMARY KEY (_m_categoriaid);


--
-- TOC entry 4766 (class 2606 OID 88923)
-- Name: _m_categoriaobjetivo _m_categoriaobjetivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoriaobjetivo
    ADD CONSTRAINT _m_categoriaobjetivo_pkey PRIMARY KEY (_m_categoriaobjetivoid);


--
-- TOC entry 4768 (class 2606 OID 88925)
-- Name: _m_efetividade _m_efetividade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_efetividade
    ADD CONSTRAINT _m_efetividade_pkey PRIMARY KEY (_m_efetividadeid);


--
-- TOC entry 4770 (class 2606 OID 88927)
-- Name: _m_identificacaopassos _m_identificacaopassos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_identificacaopassos
    ADD CONSTRAINT _m_identificacaopassos_pkey PRIMARY KEY (id);


--
-- TOC entry 4773 (class 2606 OID 88929)
-- Name: _m_instrumentoservico _m_instrumentoservico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_instrumentoservico
    ADD CONSTRAINT _m_instrumentoservico_pkey PRIMARY KEY (_m_instrumentoservicoid);


--
-- TOC entry 4775 (class 2606 OID 88931)
-- Name: _m_legalidademarcada _m_legalidademarcada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_legalidademarcada
    ADD CONSTRAINT _m_legalidademarcada_pkey PRIMARY KEY (_m_legalidademarcadaid);


--
-- TOC entry 4777 (class 2606 OID 88933)
-- Name: _m_matriz _m_matriz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_matriz
    ADD CONSTRAINT _m_matriz_pkey PRIMARY KEY (_m_matrizid);


--
-- TOC entry 4780 (class 2606 OID 88935)
-- Name: _m_processo _m_processo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_processo
    ADD CONSTRAINT _m_processo_pkey PRIMARY KEY (_m_processoid);


--
-- TOC entry 4782 (class 2606 OID 88937)
-- Name: _m_usoespecifico _m_usoespecifico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecifico
    ADD CONSTRAINT _m_usoespecifico_pkey PRIMARY KEY (_m_usoespecificoid);


--
-- TOC entry 4784 (class 2606 OID 88939)
-- Name: _m_usoespecificocategoriamanejo _m_usoespecificocategoriamanejo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecificocategoriamanejo
    ADD CONSTRAINT _m_usoespecificocategoriamanejo_pkey PRIMARY KEY (_m_usoespecificocategoriamanejoid);


--
-- TOC entry 4786 (class 2606 OID 88941)
-- Name: _m_usogenerico _m_usogenerico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usogenerico
    ADD CONSTRAINT _m_usogenerico_pkey PRIMARY KEY (_m_usogenericoid);


--
-- TOC entry 4734 (class 2606 OID 88943)
-- Name: __i_uc _uc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.__i_uc
    ADD CONSTRAINT _uc_pkey PRIMARY KEY (ucid);


--
-- TOC entry 4788 (class 2606 OID 88945)
-- Name: anosamge anosamge_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.anosamge
    ADD CONSTRAINT anosamge_pkey PRIMARY KEY (anosamgeid);


--
-- TOC entry 4790 (class 2606 OID 88947)
-- Name: banner banner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (bannerid);


--
-- TOC entry 4794 (class 2606 OID 88949)
-- Name: cadastro_cnuc cadastro_cnuc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cnuc
    ADD CONSTRAINT cadastro_cnuc_pkey PRIMARY KEY (cadastro_cnucid);


--
-- TOC entry 4796 (class 2606 OID 88951)
-- Name: cadastro_cr cadastro_cr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_cr
    ADD CONSTRAINT cadastro_cr_pkey PRIMARY KEY (cadastro_crid);


--
-- TOC entry 4792 (class 2606 OID 88953)
-- Name: cadastro cadastro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro
    ADD CONSTRAINT cadastro_pkey PRIMARY KEY (cadastroid);


--
-- TOC entry 4798 (class 2606 OID 88955)
-- Name: cadastro_uf cadastro_uf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cadastro_uf
    ADD CONSTRAINT cadastro_uf_pkey PRIMARY KEY (cadastro_ufid);


--
-- TOC entry 4800 (class 2606 OID 88957)
-- Name: entrada entrada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entrada
    ADD CONSTRAINT entrada_pkey PRIMARY KEY (entradaid);


--
-- TOC entry 4803 (class 2606 OID 88959)
-- Name: entradaacao entradaacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT entradaacao_pkey PRIMARY KEY (entradaacaoid);


--
-- TOC entry 4805 (class 2606 OID 88961)
-- Name: entradarv entradarv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarv
    ADD CONSTRAINT entradarv_pkey PRIMARY KEY (entradarvid);


--
-- TOC entry 4807 (class 2606 OID 88963)
-- Name: entradarvacao entradarvacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT entradarvacao_pkey PRIMARY KEY (entradarvacaoid);


--
-- TOC entry 4809 (class 2606 OID 88965)
-- Name: entradarvuso entradarvuso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT entradarvuso_pkey PRIMARY KEY (entradarvusoid);


--
-- TOC entry 4811 (class 2606 OID 88967)
-- Name: entradauso entradauso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT entradauso_pkey PRIMARY KEY (entradausoid);


--
-- TOC entry 4814 (class 2606 OID 88969)
-- Name: entradausoacao entradausoacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT entradausoacao_pkey PRIMARY KEY (entradausoacaoid);


--
-- TOC entry 4816 (class 2606 OID 88971)
-- Name: nivelacessoporperfil nivelacessoporperfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivelacessoporperfil
    ADD CONSTRAINT nivelacessoporperfil_pkey PRIMARY KEY (id);


--
-- TOC entry 4818 (class 2606 OID 88973)
-- Name: parceiro parceiro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parceiro
    ADD CONSTRAINT parceiro_pkey PRIMARY KEY (parceiroid);


--
-- TOC entry 4820 (class 2606 OID 88975)
-- Name: passos_anotacoes passos_anotacoes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.passos_anotacoes
    ADD CONSTRAINT passos_anotacoes_pkey PRIMARY KEY (passos_anotacoesid);


--
-- TOC entry 4822 (class 2606 OID 88977)
-- Name: perfil perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 4824 (class 2606 OID 88979)
-- Name: relatorio relatorio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.relatorio
    ADD CONSTRAINT relatorio_pkey PRIMARY KEY (relatorioid);


--
-- TOC entry 4826 (class 2606 OID 88983)
-- Name: urlpowerbi urlpowerbi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urlpowerbi
    ADD CONSTRAINT urlpowerbi_pkey PRIMARY KEY (id);


--
-- TOC entry 4717 (class 1259 OID 88984)
-- Name: unidades_conservacao_shp_geom_geom_idx; Type: INDEX; Schema: geom; Owner: postgres
--

CREATE INDEX unidades_conservacao_shp_geom_geom_idx ON geom.unidades_conservacao_shp USING gist (geom);


--
-- TOC entry 4722 (class 1259 OID 88985)
-- Name: unidades_conservacao_shp_old_geom_geom_idx; Type: INDEX; Schema: geom; Owner: postgres
--

CREATE INDEX unidades_conservacao_shp_old_geom_geom_idx ON geom.unidades_conservacao_shp_old USING gist (geom);


--
-- TOC entry 4760 (class 1259 OID 88986)
-- Name: _m_atividadeconsultoria_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _m_atividadeconsultoria_m_processoid ON public._m_atividadeconsultoria USING btree (_m_atividadeconsultoriaid);


--
-- TOC entry 4771 (class 1259 OID 88987)
-- Name: _m_instrumentoservico_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _m_instrumentoservico_m_processoid ON public._m_instrumentoservico USING btree (_m_instrumentoservicoid);


--
-- TOC entry 4801 (class 1259 OID 88988)
-- Name: entrada_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX entrada_situacao_idx ON public.entrada USING btree (situacao);


--
-- TOC entry 4812 (class 1259 OID 88989)
-- Name: entradauso_situacao_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX entradauso_situacao_idx ON public.entradauso USING btree (situacao);


--
-- TOC entry 4741 (class 1259 OID 88990)
-- Name: idx__gc_auditoria__gc_usuarioid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria__gc_usuarioid ON public._gc_auditoria USING btree (_gc_usuarioid);


--
-- TOC entry 4742 (class 1259 OID 88991)
-- Name: idx__gc_auditoria_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_data ON public._gc_auditoria USING btree (data);


--
-- TOC entry 4743 (class 1259 OID 88992)
-- Name: idx__gc_auditoria_registroid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_registroid ON public._gc_auditoria USING btree (registroid);


--
-- TOC entry 4744 (class 1259 OID 88993)
-- Name: idx__gc_auditoria_tabela; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_auditoria_tabela ON public._gc_auditoria USING btree (tabela);


--
-- TOC entry 4749 (class 1259 OID 88994)
-- Name: idx__gc_paginaavulsa_situacao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_paginaavulsa_situacao ON public._gc_paginaavulsa USING btree (situacao);


--
-- TOC entry 4752 (class 1259 OID 88995)
-- Name: idx__gc_repositorio_situacao; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__gc_repositorio_situacao ON public._gc_repositorio USING btree (situacao);


--
-- TOC entry 4759 (class 1259 OID 88996)
-- Name: idx__m_acaomanejo_m_processoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx__m_acaomanejo_m_processoid ON public._m_acaomanejo USING btree (_m_acaomanejoid);


--
-- TOC entry 4778 (class 1259 OID 89001)
-- Name: ix_m_matriz_tipoid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_m_matriz_tipoid ON public._m_matriz USING btree (tipoid);


--
-- TOC entry 4835 (class 2606 OID 89002)
-- Name: entradaacao fk_entradaacao_entradaid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_entradaid FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4836 (class 2606 OID 89007)
-- Name: entradaacao fk_entradaacao_m_acaomanejo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_m_acaomanejo FOREIGN KEY (_m_acaomanejoid) REFERENCES public._m_acaomanejo(_m_acaomanejoid);


--
-- TOC entry 4837 (class 2606 OID 89012)
-- Name: entradaacao fk_entradaacao_m_matriz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradaacao
    ADD CONSTRAINT fk_entradaacao_m_matriz FOREIGN KEY (_m_instrumentoamparaacaoid) REFERENCES public._m_matriz(_m_matrizid);


--
-- TOC entry 4838 (class 2606 OID 89017)
-- Name: entradarv fk_entradarv_entrada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarv
    ADD CONSTRAINT fk_entradarv_entrada FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4839 (class 2606 OID 89022)
-- Name: entradarvacao fk_entradarvacao_entradaacao; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT fk_entradarvacao_entradaacao FOREIGN KEY (entradaacaoid) REFERENCES public.entradaacao(entradaacaoid);


--
-- TOC entry 4840 (class 2606 OID 89027)
-- Name: entradarvacao fk_entradarvacao_entradarv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvacao
    ADD CONSTRAINT fk_entradarvacao_entradarv FOREIGN KEY (entradarvid) REFERENCES public.entradarv(entradarvid);


--
-- TOC entry 4841 (class 2606 OID 89032)
-- Name: entradarvuso fk_entradarvuso_entradarv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT fk_entradarvuso_entradarv FOREIGN KEY (entradarvid) REFERENCES public.entradarv(entradarvid);


--
-- TOC entry 4842 (class 2606 OID 89037)
-- Name: entradarvuso fk_entradarvuso_entradauso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradarvuso
    ADD CONSTRAINT fk_entradarvuso_entradauso FOREIGN KEY (entradausoid) REFERENCES public.entradauso(entradausoid);


--
-- TOC entry 4843 (class 2606 OID 89042)
-- Name: entradauso fk_entradauso_entrada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT fk_entradauso_entrada FOREIGN KEY (entradaid) REFERENCES public.entrada(entradaid);


--
-- TOC entry 4844 (class 2606 OID 89047)
-- Name: entradauso fk_entradauso_m_usoespecificoid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradauso
    ADD CONSTRAINT fk_entradauso_m_usoespecificoid FOREIGN KEY (_m_usoespecificoid) REFERENCES public._m_usoespecifico(_m_usoespecificoid);


--
-- TOC entry 4845 (class 2606 OID 89052)
-- Name: entradausoacao fk_entradausoacao_entradaacao; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT fk_entradausoacao_entradaacao FOREIGN KEY (entradaacaoid) REFERENCES public.entradaacao(entradaacaoid);


--
-- TOC entry 4846 (class 2606 OID 89057)
-- Name: entradausoacao fk_entradausoacao_entradauso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradausoacao
    ADD CONSTRAINT fk_entradausoacao_entradauso FOREIGN KEY (entradausoid) REFERENCES public.entradauso(entradausoid);


--
-- TOC entry 4827 (class 2606 OID 89062)
-- Name: _gc_auditoria fk_gc_auditoria_gc_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_auditoria
    ADD CONSTRAINT fk_gc_auditoria_gc_usuario FOREIGN KEY (_gc_usuarioid) REFERENCES public._gc_usuario(_gc_usuarioid);


--
-- TOC entry 4828 (class 2606 OID 89067)
-- Name: _gc_usuariologin fk_gc_usuariologin_gc_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._gc_usuariologin
    ADD CONSTRAINT fk_gc_usuariologin_gc_usuario FOREIGN KEY (_gc_usuarioid) REFERENCES public._gc_usuario(_gc_usuarioid);


--
-- TOC entry 4829 (class 2606 OID 89072)
-- Name: _m_acaomanejo fk_m_acaomanejo_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_acaomanejo
    ADD CONSTRAINT fk_m_acaomanejo_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4830 (class 2606 OID 89077)
-- Name: _m_atividadeconsultoria fk_m_atividadeconsultoria_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_atividadeconsultoria
    ADD CONSTRAINT fk_m_atividadeconsultoria_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4831 (class 2606 OID 89082)
-- Name: _m_categoriaobjetivo fk_m_categoriaobjetivo_m_categoriaobjetivo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_categoriaobjetivo
    ADD CONSTRAINT fk_m_categoriaobjetivo_m_categoriaobjetivo FOREIGN KEY (_m_categoriaid) REFERENCES public._m_categoria(_m_categoriaid);


--
-- TOC entry 4832 (class 2606 OID 89087)
-- Name: _m_instrumentoservico fk_m_instrumentoservico_m_processo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_instrumentoservico
    ADD CONSTRAINT fk_m_instrumentoservico_m_processo FOREIGN KEY (_m_processoid) REFERENCES public._m_processo(_m_processoid);


--
-- TOC entry 4833 (class 2606 OID 89092)
-- Name: _m_usoespecifico fk_m_usoespecifico_m_usogenerico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecifico
    ADD CONSTRAINT fk_m_usoespecifico_m_usogenerico FOREIGN KEY (_m_usogenericoid) REFERENCES public._m_usogenerico(_m_usogenericoid);


--
-- TOC entry 4834 (class 2606 OID 89097)
-- Name: _m_usoespecificocategoriamanejo fk_m_usoespecificocategoriamanejo_m_usoespecifico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._m_usoespecificocategoriamanejo
    ADD CONSTRAINT fk_m_usoespecificocategoriamanejo_m_usoespecifico FOREIGN KEY (_m_usoespecificoid) REFERENCES public._m_usoespecifico(_m_usoespecificoid);


--
-- TOC entry 4847 (class 2606 OID 89102)
-- Name: nivelacessoporperfil nivelacessoporperfil_id_perfil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivelacessoporperfil
    ADD CONSTRAINT nivelacessoporperfil_id_perfil_fkey FOREIGN KEY (id_perfil) REFERENCES public.perfil(id);


--
-- TOC entry 4981 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA geom; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA geom TO samge_read;


--
-- TOC entry 4982 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT USAGE ON SCHEMA public TO samge_read;


--
-- TOC entry 4985 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE espacializacao; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.espacializacao TO samge_read;


--
-- TOC entry 4987 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE estados; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.estados TO samge_read;


--
-- TOC entry 4989 (class 0 OID 0)
-- Dependencies: 218
-- Name: TABLE municipios; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.municipios TO samge_read;


--
-- TOC entry 4991 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE ucs_brasil; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.ucs_brasil TO samge_read;


--
-- TOC entry 4993 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE unidades_conservacao_shp; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.unidades_conservacao_shp TO samge_read;


--
-- TOC entry 4994 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE unidades_conservacao_shp_old; Type: ACL; Schema: geom; Owner: postgres
--

GRANT SELECT ON TABLE geom.unidades_conservacao_shp_old TO samge_read;


--
-- TOC entry 4997 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE __i_ent_b_rv; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_ent_b_rv TO samge_read;


--
-- TOC entry 4999 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE __i_ent_c_uso; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_ent_c_uso TO samge_read;


--
-- TOC entry 5001 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE __i_ent_d_acao; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_ent_d_acao TO samge_read;


--
-- TOC entry 5003 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE __i_ent_g_processo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_ent_g_processo TO samge_read;


--
-- TOC entry 5005 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE __i_ngi_cr; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_ngi_cr TO samge_read;


--
-- TOC entry 5006 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE __i_uc; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_uc TO samge_read;


--
-- TOC entry 5007 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE __i_uc_cr; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_uc_cr TO samge_read;


--
-- TOC entry 5009 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE __i_uc_grupo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__i_uc_grupo TO samge_read;


--
-- TOC entry 5011 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE __unidadeconservacao; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.__unidadeconservacao TO samge_read;


--
-- TOC entry 5012 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE _gc_auditoria; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_auditoria TO samge_read;


--
-- TOC entry 5014 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE _gc_metadados; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_metadados TO samge_read;


--
-- TOC entry 5015 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE _gc_paginaavulsa; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_paginaavulsa TO samge_read;


--
-- TOC entry 5017 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE _gc_repositorio; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_repositorio TO samge_read;


--
-- TOC entry 5019 (class 0 OID 0)
-- Dependencies: 249
-- Name: TABLE _gc_usuario; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_usuario TO samge_read;


--
-- TOC entry 5021 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE _gc_usuariologin; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._gc_usuariologin TO samge_read;


--
-- TOC entry 5023 (class 0 OID 0)
-- Dependencies: 253
-- Name: TABLE _m_acaomanejo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_acaomanejo TO samge_read;


--
-- TOC entry 5025 (class 0 OID 0)
-- Dependencies: 255
-- Name: TABLE _m_atividadeconsultoria; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_atividadeconsultoria TO samge_read;


--
-- TOC entry 5027 (class 0 OID 0)
-- Dependencies: 257
-- Name: TABLE _m_categoria; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_categoria TO samge_read;


--
-- TOC entry 5029 (class 0 OID 0)
-- Dependencies: 259
-- Name: TABLE _m_categoriaobjetivo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_categoriaobjetivo TO samge_read;


--
-- TOC entry 5031 (class 0 OID 0)
-- Dependencies: 261
-- Name: TABLE _m_efetividade; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_efetividade TO samge_read;


--
-- TOC entry 5033 (class 0 OID 0)
-- Dependencies: 263
-- Name: TABLE _m_identificacaopassos; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_identificacaopassos TO samge_read;


--
-- TOC entry 5035 (class 0 OID 0)
-- Dependencies: 265
-- Name: TABLE _m_instrumentoservico; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_instrumentoservico TO samge_read;


--
-- TOC entry 5037 (class 0 OID 0)
-- Dependencies: 267
-- Name: TABLE _m_legalidademarcada; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_legalidademarcada TO samge_read;


--
-- TOC entry 5039 (class 0 OID 0)
-- Dependencies: 269
-- Name: TABLE _m_matriz; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_matriz TO samge_read;


--
-- TOC entry 5041 (class 0 OID 0)
-- Dependencies: 271
-- Name: TABLE _m_processo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_processo TO samge_read;


--
-- TOC entry 5043 (class 0 OID 0)
-- Dependencies: 273
-- Name: TABLE _m_usoespecifico; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_usoespecifico TO samge_read;


--
-- TOC entry 5045 (class 0 OID 0)
-- Dependencies: 275
-- Name: TABLE _m_usoespecificocategoriamanejo; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_usoespecificocategoriamanejo TO samge_read;


--
-- TOC entry 5047 (class 0 OID 0)
-- Dependencies: 277
-- Name: TABLE _m_usogenerico; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public._m_usogenerico TO samge_read;


--
-- TOC entry 5050 (class 0 OID 0)
-- Dependencies: 280
-- Name: TABLE anosamge; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.anosamge TO samge_read;


--
-- TOC entry 5052 (class 0 OID 0)
-- Dependencies: 282
-- Name: TABLE banner; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.banner TO samge_read;


--
-- TOC entry 5054 (class 0 OID 0)
-- Dependencies: 284
-- Name: TABLE cadastro; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.cadastro TO samge_read;


--
-- TOC entry 5056 (class 0 OID 0)
-- Dependencies: 286
-- Name: TABLE cadastro_cnuc; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.cadastro_cnuc TO samge_read;


--
-- TOC entry 5058 (class 0 OID 0)
-- Dependencies: 288
-- Name: TABLE cadastro_cr; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.cadastro_cr TO samge_read;


--
-- TOC entry 5060 (class 0 OID 0)
-- Dependencies: 290
-- Name: TABLE cadastro_uf; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.cadastro_uf TO samge_read;


--
-- TOC entry 5063 (class 0 OID 0)
-- Dependencies: 292
-- Name: TABLE entrada; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entrada TO samge_read;


--
-- TOC entry 5065 (class 0 OID 0)
-- Dependencies: 294
-- Name: TABLE entradaacao; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradaacao TO samge_read;


--
-- TOC entry 5067 (class 0 OID 0)
-- Dependencies: 296
-- Name: TABLE entradarv; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradarv TO samge_read;


--
-- TOC entry 5069 (class 0 OID 0)
-- Dependencies: 298
-- Name: TABLE entradarvacao; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradarvacao TO samge_read;


--
-- TOC entry 5071 (class 0 OID 0)
-- Dependencies: 300
-- Name: TABLE entradarvuso; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradarvuso TO samge_read;


--
-- TOC entry 5073 (class 0 OID 0)
-- Dependencies: 302
-- Name: TABLE entradauso; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradauso TO samge_read;


--
-- TOC entry 5075 (class 0 OID 0)
-- Dependencies: 304
-- Name: TABLE entradausoacao; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.entradausoacao TO samge_read;


--
-- TOC entry 5079 (class 0 OID 0)
-- Dependencies: 308
-- Name: TABLE parceiro; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.parceiro TO samge_read;


--
-- TOC entry 5081 (class 0 OID 0)
-- Dependencies: 310
-- Name: TABLE passos_anotacoes; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.passos_anotacoes TO samge_read;


--
-- TOC entry 5084 (class 0 OID 0)
-- Dependencies: 314
-- Name: TABLE relatorio; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.relatorio TO samge_read;


--
-- TOC entry 3545 (class 826 OID 89107)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: geom; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA geom GRANT SELECT ON TABLES  TO samge_read;


-- Completed on 2023-05-26 15:22:32 -03

--
-- PostgreSQL database dump complete
--

