package br.com.icmbio.samge.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM = "system";

    private Constants() {}
}
