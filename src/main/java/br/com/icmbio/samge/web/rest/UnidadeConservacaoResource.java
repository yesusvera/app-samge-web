package br.com.icmbio.samge.web.rest;

import br.com.icmbio.samge.domain.UnidadeConservacao;
import br.com.icmbio.samge.repository.UnidadeConservacaoRepository;
import br.com.icmbio.samge.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.com.icmbio.samge.domain.UnidadeConservacao}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class UnidadeConservacaoResource {

    private final Logger log = LoggerFactory.getLogger(UnidadeConservacaoResource.class);

    private static final String ENTITY_NAME = "samgeUnidadeConservacao";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UnidadeConservacaoRepository unidadeConservacaoRepository;

    public UnidadeConservacaoResource(UnidadeConservacaoRepository unidadeConservacaoRepository) {
        this.unidadeConservacaoRepository = unidadeConservacaoRepository;
    }

    /**
     * {@code POST  /unidade-conservacaos} : Create a new unidadeConservacao.
     *
     * @param unidadeConservacao the unidadeConservacao to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new unidadeConservacao, or with status {@code 400 (Bad Request)} if the unidadeConservacao has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/unidade-conservacaos")
    public ResponseEntity<UnidadeConservacao> createUnidadeConservacao(@Valid @RequestBody UnidadeConservacao unidadeConservacao)
        throws URISyntaxException {
        log.debug("REST request to save UnidadeConservacao : {}", unidadeConservacao);
        if (unidadeConservacao.getId() != null) {
            throw new BadRequestAlertException("A new unidadeConservacao cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnidadeConservacao result = unidadeConservacaoRepository.save(unidadeConservacao);
        return ResponseEntity
            .created(new URI("/api/unidade-conservacaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /unidade-conservacaos/:id} : Updates an existing unidadeConservacao.
     *
     * @param id the id of the unidadeConservacao to save.
     * @param unidadeConservacao the unidadeConservacao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unidadeConservacao,
     * or with status {@code 400 (Bad Request)} if the unidadeConservacao is not valid,
     * or with status {@code 500 (Internal Server Error)} if the unidadeConservacao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/unidade-conservacaos/{id}")
    public ResponseEntity<UnidadeConservacao> updateUnidadeConservacao(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UnidadeConservacao unidadeConservacao
    ) throws URISyntaxException {
        log.debug("REST request to update UnidadeConservacao : {}, {}", id, unidadeConservacao);
        if (unidadeConservacao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, unidadeConservacao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!unidadeConservacaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UnidadeConservacao result = unidadeConservacaoRepository.save(unidadeConservacao);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, unidadeConservacao.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /unidade-conservacaos/:id} : Partial updates given fields of an existing unidadeConservacao, field will ignore if it is null
     *
     * @param id the id of the unidadeConservacao to save.
     * @param unidadeConservacao the unidadeConservacao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unidadeConservacao,
     * or with status {@code 400 (Bad Request)} if the unidadeConservacao is not valid,
     * or with status {@code 404 (Not Found)} if the unidadeConservacao is not found,
     * or with status {@code 500 (Internal Server Error)} if the unidadeConservacao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/unidade-conservacaos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UnidadeConservacao> partialUpdateUnidadeConservacao(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody UnidadeConservacao unidadeConservacao
    ) throws URISyntaxException {
        log.debug("REST request to partial update UnidadeConservacao partially : {}, {}", id, unidadeConservacao);
        if (unidadeConservacao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, unidadeConservacao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!unidadeConservacaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UnidadeConservacao> result = unidadeConservacaoRepository
            .findById(unidadeConservacao.getId())
            .map(existingUnidadeConservacao -> {
                if (unidadeConservacao.getCategoriamanejoid() != null) {
                    existingUnidadeConservacao.setCategoriamanejoid(unidadeConservacao.getCategoriamanejoid());
                }
                if (unidadeConservacao.getCategoriaiucnid() != null) {
                    existingUnidadeConservacao.setCategoriaiucnid(unidadeConservacao.getCategoriaiucnid());
                }
                if (unidadeConservacao.getSituacao() != null) {
                    existingUnidadeConservacao.setSituacao(unidadeConservacao.getSituacao());
                }
                if (unidadeConservacao.getTipoesfera() != null) {
                    existingUnidadeConservacao.setTipoesfera(unidadeConservacao.getTipoesfera());
                }
                if (unidadeConservacao.getEsfera() != null) {
                    existingUnidadeConservacao.setEsfera(unidadeConservacao.getEsfera());
                }
                if (unidadeConservacao.getCodigouc() != null) {
                    existingUnidadeConservacao.setCodigouc(unidadeConservacao.getCodigouc());
                }
                if (unidadeConservacao.getCnuc() != null) {
                    existingUnidadeConservacao.setCnuc(unidadeConservacao.getCnuc());
                }
                if (unidadeConservacao.getNome() != null) {
                    existingUnidadeConservacao.setNome(unidadeConservacao.getNome());
                }
                if (unidadeConservacao.getUfs() != null) {
                    existingUnidadeConservacao.setUfs(unidadeConservacao.getUfs());
                }
                if (unidadeConservacao.getAnocriacao() != null) {
                    existingUnidadeConservacao.setAnocriacao(unidadeConservacao.getAnocriacao());
                }
                if (unidadeConservacao.getAtolegalcriacao() != null) {
                    existingUnidadeConservacao.setAtolegalcriacao(unidadeConservacao.getAtolegalcriacao());
                }
                if (unidadeConservacao.getOutrosatoslegais() != null) {
                    existingUnidadeConservacao.setOutrosatoslegais(unidadeConservacao.getOutrosatoslegais());
                }
                if (unidadeConservacao.getMunicipios() != null) {
                    existingUnidadeConservacao.setMunicipios(unidadeConservacao.getMunicipios());
                }
                if (unidadeConservacao.getTemplanomanejo() != null) {
                    existingUnidadeConservacao.setTemplanomanejo(unidadeConservacao.getTemplanomanejo());
                }
                if (unidadeConservacao.getTemconselhogestor() != null) {
                    existingUnidadeConservacao.setTemconselhogestor(unidadeConservacao.getTemconselhogestor());
                }
                if (unidadeConservacao.getFontearea() != null) {
                    existingUnidadeConservacao.setFontearea(unidadeConservacao.getFontearea());
                }
                if (unidadeConservacao.getAreabioma() != null) {
                    existingUnidadeConservacao.setAreabioma(unidadeConservacao.getAreabioma());
                }
                if (unidadeConservacao.getAreaatolegalcriacao() != null) {
                    existingUnidadeConservacao.setAreaatolegalcriacao(unidadeConservacao.getAreaatolegalcriacao());
                }
                if (unidadeConservacao.getAreaamazonia() != null) {
                    existingUnidadeConservacao.setAreaamazonia(unidadeConservacao.getAreaamazonia());
                }
                if (unidadeConservacao.getAreacaatinga() != null) {
                    existingUnidadeConservacao.setAreacaatinga(unidadeConservacao.getAreacaatinga());
                }
                if (unidadeConservacao.getAreacerrado() != null) {
                    existingUnidadeConservacao.setAreacerrado(unidadeConservacao.getAreacerrado());
                }
                if (unidadeConservacao.getAreamataatlantica() != null) {
                    existingUnidadeConservacao.setAreamataatlantica(unidadeConservacao.getAreamataatlantica());
                }
                if (unidadeConservacao.getAreapampa() != null) {
                    existingUnidadeConservacao.setAreapampa(unidadeConservacao.getAreapampa());
                }
                if (unidadeConservacao.getAreapantanal() != null) {
                    existingUnidadeConservacao.setAreapantanal(unidadeConservacao.getAreapantanal());
                }
                if (unidadeConservacao.getAreamarinha() != null) {
                    existingUnidadeConservacao.setAreamarinha(unidadeConservacao.getAreamarinha());
                }
                if (unidadeConservacao.getBiomaid() != null) {
                    existingUnidadeConservacao.setBiomaid(unidadeConservacao.getBiomaid());
                }

                return existingUnidadeConservacao;
            })
            .map(unidadeConservacaoRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, unidadeConservacao.getId().toString())
        );
    }

    /**
     * {@code GET  /unidade-conservacaos} : get all the unidadeConservacaos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of unidadeConservacaos in body.
     */
    @GetMapping("/unidade-conservacaos")
    public List<UnidadeConservacao> getAllUnidadeConservacaos() {
        log.debug("REST request to get all UnidadeConservacaos");
        return unidadeConservacaoRepository.findAll();
    }

    /**
     * {@code GET  /unidade-conservacaos/:id} : get the "id" unidadeConservacao.
     *
     * @param id the id of the unidadeConservacao to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the unidadeConservacao, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/unidade-conservacaos/{id}")
    public ResponseEntity<UnidadeConservacao> getUnidadeConservacao(@PathVariable Long id) {
        log.debug("REST request to get UnidadeConservacao : {}", id);
        Optional<UnidadeConservacao> unidadeConservacao = unidadeConservacaoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(unidadeConservacao);
    }

    /**
     * {@code DELETE  /unidade-conservacaos/:id} : delete the "id" unidadeConservacao.
     *
     * @param id the id of the unidadeConservacao to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/unidade-conservacaos/{id}")
    public ResponseEntity<Void> deleteUnidadeConservacao(@PathVariable Long id) {
        log.debug("REST request to delete UnidadeConservacao : {}", id);
        unidadeConservacaoRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
