package br.com.icmbio.samge.repository;

import br.com.icmbio.samge.domain.GcUsuario;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the GcUsuario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GcUsuarioRepository extends JpaRepository<GcUsuario, Long> {}
