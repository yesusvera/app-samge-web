package br.com.icmbio.samge.web.rest;

import static br.com.icmbio.samge.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.icmbio.samge.IntegrationTest;
import br.com.icmbio.samge.domain.UnidadeConservacao;
import br.com.icmbio.samge.repository.UnidadeConservacaoRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UnidadeConservacaoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UnidadeConservacaoResourceIT {

    private static final Long DEFAULT_CATEGORIAMANEJOID = 1L;
    private static final Long UPDATED_CATEGORIAMANEJOID = 2L;

    private static final Long DEFAULT_CATEGORIAIUCNID = 1L;
    private static final Long UPDATED_CATEGORIAIUCNID = 2L;

    private static final String DEFAULT_SITUACAO = "AAAAAAAAAA";
    private static final String UPDATED_SITUACAO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPOESFERA = "AAAAAAAAAA";
    private static final String UPDATED_TIPOESFERA = "BBBBBBBBBB";

    private static final String DEFAULT_ESFERA = "AAAAAAAAAA";
    private static final String UPDATED_ESFERA = "BBBBBBBBBB";

    private static final Long DEFAULT_CODIGOUC = 1L;
    private static final Long UPDATED_CODIGOUC = 2L;

    private static final String DEFAULT_CNUC = "AAAAAAAAAA";
    private static final String UPDATED_CNUC = "BBBBBBBBBB";

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_UFS = "AAAAAAAAAA";
    private static final String UPDATED_UFS = "BBBBBBBBBB";

    private static final Long DEFAULT_ANOCRIACAO = 1L;
    private static final Long UPDATED_ANOCRIACAO = 2L;

    private static final String DEFAULT_ATOLEGALCRIACAO = "AAAAAAAAAA";
    private static final String UPDATED_ATOLEGALCRIACAO = "BBBBBBBBBB";

    private static final String DEFAULT_OUTROSATOSLEGAIS = "AAAAAAAAAA";
    private static final String UPDATED_OUTROSATOSLEGAIS = "BBBBBBBBBB";

    private static final String DEFAULT_MUNICIPIOS = "AAAAAAAAAA";
    private static final String UPDATED_MUNICIPIOS = "BBBBBBBBBB";

    private static final String DEFAULT_TEMPLANOMANEJO = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLANOMANEJO = "BBBBBBBBBB";

    private static final String DEFAULT_TEMCONSELHOGESTOR = "AAAAAAAAAA";
    private static final String UPDATED_TEMCONSELHOGESTOR = "BBBBBBBBBB";

    private static final String DEFAULT_FONTEAREA = "AAAAAAAAAA";
    private static final String UPDATED_FONTEAREA = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AREABIOMA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREABIOMA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAATOLEGALCRIACAO = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAATOLEGALCRIACAO = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAAMAZONIA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAAMAZONIA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREACAATINGA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREACAATINGA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREACERRADO = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREACERRADO = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAMATAATLANTICA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAMATAATLANTICA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAPAMPA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAPAMPA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAPANTANAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAPANTANAL = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AREAMARINHA = new BigDecimal(1);
    private static final BigDecimal UPDATED_AREAMARINHA = new BigDecimal(2);

    private static final Long DEFAULT_BIOMAID = 1L;
    private static final Long UPDATED_BIOMAID = 2L;

    private static final String ENTITY_API_URL = "/api/unidade-conservacaos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UnidadeConservacaoRepository unidadeConservacaoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUnidadeConservacaoMockMvc;

    private UnidadeConservacao unidadeConservacao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnidadeConservacao createEntity(EntityManager em) {
        UnidadeConservacao unidadeConservacao = new UnidadeConservacao()
            .categoriamanejoid(DEFAULT_CATEGORIAMANEJOID)
            .categoriaiucnid(DEFAULT_CATEGORIAIUCNID)
            .situacao(DEFAULT_SITUACAO)
            .tipoesfera(DEFAULT_TIPOESFERA)
            .esfera(DEFAULT_ESFERA)
            .codigouc(DEFAULT_CODIGOUC)
            .cnuc(DEFAULT_CNUC)
            .nome(DEFAULT_NOME)
            .ufs(DEFAULT_UFS)
            .anocriacao(DEFAULT_ANOCRIACAO)
            .atolegalcriacao(DEFAULT_ATOLEGALCRIACAO)
            .outrosatoslegais(DEFAULT_OUTROSATOSLEGAIS)
            .municipios(DEFAULT_MUNICIPIOS)
            .templanomanejo(DEFAULT_TEMPLANOMANEJO)
            .temconselhogestor(DEFAULT_TEMCONSELHOGESTOR)
            .fontearea(DEFAULT_FONTEAREA)
            .areabioma(DEFAULT_AREABIOMA)
            .areaatolegalcriacao(DEFAULT_AREAATOLEGALCRIACAO)
            .areaamazonia(DEFAULT_AREAAMAZONIA)
            .areacaatinga(DEFAULT_AREACAATINGA)
            .areacerrado(DEFAULT_AREACERRADO)
            .areamataatlantica(DEFAULT_AREAMATAATLANTICA)
            .areapampa(DEFAULT_AREAPAMPA)
            .areapantanal(DEFAULT_AREAPANTANAL)
            .areamarinha(DEFAULT_AREAMARINHA)
            .biomaid(DEFAULT_BIOMAID);
        return unidadeConservacao;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnidadeConservacao createUpdatedEntity(EntityManager em) {
        UnidadeConservacao unidadeConservacao = new UnidadeConservacao()
            .categoriamanejoid(UPDATED_CATEGORIAMANEJOID)
            .categoriaiucnid(UPDATED_CATEGORIAIUCNID)
            .situacao(UPDATED_SITUACAO)
            .tipoesfera(UPDATED_TIPOESFERA)
            .esfera(UPDATED_ESFERA)
            .codigouc(UPDATED_CODIGOUC)
            .cnuc(UPDATED_CNUC)
            .nome(UPDATED_NOME)
            .ufs(UPDATED_UFS)
            .anocriacao(UPDATED_ANOCRIACAO)
            .atolegalcriacao(UPDATED_ATOLEGALCRIACAO)
            .outrosatoslegais(UPDATED_OUTROSATOSLEGAIS)
            .municipios(UPDATED_MUNICIPIOS)
            .templanomanejo(UPDATED_TEMPLANOMANEJO)
            .temconselhogestor(UPDATED_TEMCONSELHOGESTOR)
            .fontearea(UPDATED_FONTEAREA)
            .areabioma(UPDATED_AREABIOMA)
            .areaatolegalcriacao(UPDATED_AREAATOLEGALCRIACAO)
            .areaamazonia(UPDATED_AREAAMAZONIA)
            .areacaatinga(UPDATED_AREACAATINGA)
            .areacerrado(UPDATED_AREACERRADO)
            .areamataatlantica(UPDATED_AREAMATAATLANTICA)
            .areapampa(UPDATED_AREAPAMPA)
            .areapantanal(UPDATED_AREAPANTANAL)
            .areamarinha(UPDATED_AREAMARINHA)
            .biomaid(UPDATED_BIOMAID);
        return unidadeConservacao;
    }

    @BeforeEach
    public void initTest() {
        unidadeConservacao = createEntity(em);
    }

    @Test
    @Transactional
    void createUnidadeConservacao() throws Exception {
        int databaseSizeBeforeCreate = unidadeConservacaoRepository.findAll().size();
        // Create the UnidadeConservacao
        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isCreated());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeCreate + 1);
        UnidadeConservacao testUnidadeConservacao = unidadeConservacaoList.get(unidadeConservacaoList.size() - 1);
        assertThat(testUnidadeConservacao.getCategoriamanejoid()).isEqualTo(DEFAULT_CATEGORIAMANEJOID);
        assertThat(testUnidadeConservacao.getCategoriaiucnid()).isEqualTo(DEFAULT_CATEGORIAIUCNID);
        assertThat(testUnidadeConservacao.getSituacao()).isEqualTo(DEFAULT_SITUACAO);
        assertThat(testUnidadeConservacao.getTipoesfera()).isEqualTo(DEFAULT_TIPOESFERA);
        assertThat(testUnidadeConservacao.getEsfera()).isEqualTo(DEFAULT_ESFERA);
        assertThat(testUnidadeConservacao.getCodigouc()).isEqualTo(DEFAULT_CODIGOUC);
        assertThat(testUnidadeConservacao.getCnuc()).isEqualTo(DEFAULT_CNUC);
        assertThat(testUnidadeConservacao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testUnidadeConservacao.getUfs()).isEqualTo(DEFAULT_UFS);
        assertThat(testUnidadeConservacao.getAnocriacao()).isEqualTo(DEFAULT_ANOCRIACAO);
        assertThat(testUnidadeConservacao.getAtolegalcriacao()).isEqualTo(DEFAULT_ATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getOutrosatoslegais()).isEqualTo(DEFAULT_OUTROSATOSLEGAIS);
        assertThat(testUnidadeConservacao.getMunicipios()).isEqualTo(DEFAULT_MUNICIPIOS);
        assertThat(testUnidadeConservacao.getTemplanomanejo()).isEqualTo(DEFAULT_TEMPLANOMANEJO);
        assertThat(testUnidadeConservacao.getTemconselhogestor()).isEqualTo(DEFAULT_TEMCONSELHOGESTOR);
        assertThat(testUnidadeConservacao.getFontearea()).isEqualTo(DEFAULT_FONTEAREA);
        assertThat(testUnidadeConservacao.getAreabioma()).isEqualByComparingTo(DEFAULT_AREABIOMA);
        assertThat(testUnidadeConservacao.getAreaatolegalcriacao()).isEqualByComparingTo(DEFAULT_AREAATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getAreaamazonia()).isEqualByComparingTo(DEFAULT_AREAAMAZONIA);
        assertThat(testUnidadeConservacao.getAreacaatinga()).isEqualByComparingTo(DEFAULT_AREACAATINGA);
        assertThat(testUnidadeConservacao.getAreacerrado()).isEqualByComparingTo(DEFAULT_AREACERRADO);
        assertThat(testUnidadeConservacao.getAreamataatlantica()).isEqualByComparingTo(DEFAULT_AREAMATAATLANTICA);
        assertThat(testUnidadeConservacao.getAreapampa()).isEqualByComparingTo(DEFAULT_AREAPAMPA);
        assertThat(testUnidadeConservacao.getAreapantanal()).isEqualByComparingTo(DEFAULT_AREAPANTANAL);
        assertThat(testUnidadeConservacao.getAreamarinha()).isEqualByComparingTo(DEFAULT_AREAMARINHA);
        assertThat(testUnidadeConservacao.getBiomaid()).isEqualTo(DEFAULT_BIOMAID);
    }

    @Test
    @Transactional
    void createUnidadeConservacaoWithExistingId() throws Exception {
        // Create the UnidadeConservacao with an existing ID
        unidadeConservacao.setId(1L);

        int databaseSizeBeforeCreate = unidadeConservacaoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSituacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setSituacao(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTipoesferaIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setTipoesfera(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEsferaIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setEsfera(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodigoucIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setCodigouc(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCnucIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setCnuc(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setNome(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUfsIsRequired() throws Exception {
        int databaseSizeBeforeTest = unidadeConservacaoRepository.findAll().size();
        // set the field null
        unidadeConservacao.setUfs(null);

        // Create the UnidadeConservacao, which fails.

        restUnidadeConservacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllUnidadeConservacaos() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        // Get all the unidadeConservacaoList
        restUnidadeConservacaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unidadeConservacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoriamanejoid").value(hasItem(DEFAULT_CATEGORIAMANEJOID.intValue())))
            .andExpect(jsonPath("$.[*].categoriaiucnid").value(hasItem(DEFAULT_CATEGORIAIUCNID.intValue())))
            .andExpect(jsonPath("$.[*].situacao").value(hasItem(DEFAULT_SITUACAO)))
            .andExpect(jsonPath("$.[*].tipoesfera").value(hasItem(DEFAULT_TIPOESFERA)))
            .andExpect(jsonPath("$.[*].esfera").value(hasItem(DEFAULT_ESFERA)))
            .andExpect(jsonPath("$.[*].codigouc").value(hasItem(DEFAULT_CODIGOUC.intValue())))
            .andExpect(jsonPath("$.[*].cnuc").value(hasItem(DEFAULT_CNUC)))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].ufs").value(hasItem(DEFAULT_UFS)))
            .andExpect(jsonPath("$.[*].anocriacao").value(hasItem(DEFAULT_ANOCRIACAO.intValue())))
            .andExpect(jsonPath("$.[*].atolegalcriacao").value(hasItem(DEFAULT_ATOLEGALCRIACAO)))
            .andExpect(jsonPath("$.[*].outrosatoslegais").value(hasItem(DEFAULT_OUTROSATOSLEGAIS)))
            .andExpect(jsonPath("$.[*].municipios").value(hasItem(DEFAULT_MUNICIPIOS)))
            .andExpect(jsonPath("$.[*].templanomanejo").value(hasItem(DEFAULT_TEMPLANOMANEJO)))
            .andExpect(jsonPath("$.[*].temconselhogestor").value(hasItem(DEFAULT_TEMCONSELHOGESTOR)))
            .andExpect(jsonPath("$.[*].fontearea").value(hasItem(DEFAULT_FONTEAREA)))
            .andExpect(jsonPath("$.[*].areabioma").value(hasItem(sameNumber(DEFAULT_AREABIOMA))))
            .andExpect(jsonPath("$.[*].areaatolegalcriacao").value(hasItem(sameNumber(DEFAULT_AREAATOLEGALCRIACAO))))
            .andExpect(jsonPath("$.[*].areaamazonia").value(hasItem(sameNumber(DEFAULT_AREAAMAZONIA))))
            .andExpect(jsonPath("$.[*].areacaatinga").value(hasItem(sameNumber(DEFAULT_AREACAATINGA))))
            .andExpect(jsonPath("$.[*].areacerrado").value(hasItem(sameNumber(DEFAULT_AREACERRADO))))
            .andExpect(jsonPath("$.[*].areamataatlantica").value(hasItem(sameNumber(DEFAULT_AREAMATAATLANTICA))))
            .andExpect(jsonPath("$.[*].areapampa").value(hasItem(sameNumber(DEFAULT_AREAPAMPA))))
            .andExpect(jsonPath("$.[*].areapantanal").value(hasItem(sameNumber(DEFAULT_AREAPANTANAL))))
            .andExpect(jsonPath("$.[*].areamarinha").value(hasItem(sameNumber(DEFAULT_AREAMARINHA))))
            .andExpect(jsonPath("$.[*].biomaid").value(hasItem(DEFAULT_BIOMAID.intValue())));
    }

    @Test
    @Transactional
    void getUnidadeConservacao() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        // Get the unidadeConservacao
        restUnidadeConservacaoMockMvc
            .perform(get(ENTITY_API_URL_ID, unidadeConservacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(unidadeConservacao.getId().intValue()))
            .andExpect(jsonPath("$.categoriamanejoid").value(DEFAULT_CATEGORIAMANEJOID.intValue()))
            .andExpect(jsonPath("$.categoriaiucnid").value(DEFAULT_CATEGORIAIUCNID.intValue()))
            .andExpect(jsonPath("$.situacao").value(DEFAULT_SITUACAO))
            .andExpect(jsonPath("$.tipoesfera").value(DEFAULT_TIPOESFERA))
            .andExpect(jsonPath("$.esfera").value(DEFAULT_ESFERA))
            .andExpect(jsonPath("$.codigouc").value(DEFAULT_CODIGOUC.intValue()))
            .andExpect(jsonPath("$.cnuc").value(DEFAULT_CNUC))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.ufs").value(DEFAULT_UFS))
            .andExpect(jsonPath("$.anocriacao").value(DEFAULT_ANOCRIACAO.intValue()))
            .andExpect(jsonPath("$.atolegalcriacao").value(DEFAULT_ATOLEGALCRIACAO))
            .andExpect(jsonPath("$.outrosatoslegais").value(DEFAULT_OUTROSATOSLEGAIS))
            .andExpect(jsonPath("$.municipios").value(DEFAULT_MUNICIPIOS))
            .andExpect(jsonPath("$.templanomanejo").value(DEFAULT_TEMPLANOMANEJO))
            .andExpect(jsonPath("$.temconselhogestor").value(DEFAULT_TEMCONSELHOGESTOR))
            .andExpect(jsonPath("$.fontearea").value(DEFAULT_FONTEAREA))
            .andExpect(jsonPath("$.areabioma").value(sameNumber(DEFAULT_AREABIOMA)))
            .andExpect(jsonPath("$.areaatolegalcriacao").value(sameNumber(DEFAULT_AREAATOLEGALCRIACAO)))
            .andExpect(jsonPath("$.areaamazonia").value(sameNumber(DEFAULT_AREAAMAZONIA)))
            .andExpect(jsonPath("$.areacaatinga").value(sameNumber(DEFAULT_AREACAATINGA)))
            .andExpect(jsonPath("$.areacerrado").value(sameNumber(DEFAULT_AREACERRADO)))
            .andExpect(jsonPath("$.areamataatlantica").value(sameNumber(DEFAULT_AREAMATAATLANTICA)))
            .andExpect(jsonPath("$.areapampa").value(sameNumber(DEFAULT_AREAPAMPA)))
            .andExpect(jsonPath("$.areapantanal").value(sameNumber(DEFAULT_AREAPANTANAL)))
            .andExpect(jsonPath("$.areamarinha").value(sameNumber(DEFAULT_AREAMARINHA)))
            .andExpect(jsonPath("$.biomaid").value(DEFAULT_BIOMAID.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingUnidadeConservacao() throws Exception {
        // Get the unidadeConservacao
        restUnidadeConservacaoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUnidadeConservacao() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();

        // Update the unidadeConservacao
        UnidadeConservacao updatedUnidadeConservacao = unidadeConservacaoRepository.findById(unidadeConservacao.getId()).get();
        // Disconnect from session so that the updates on updatedUnidadeConservacao are not directly saved in db
        em.detach(updatedUnidadeConservacao);
        updatedUnidadeConservacao
            .categoriamanejoid(UPDATED_CATEGORIAMANEJOID)
            .categoriaiucnid(UPDATED_CATEGORIAIUCNID)
            .situacao(UPDATED_SITUACAO)
            .tipoesfera(UPDATED_TIPOESFERA)
            .esfera(UPDATED_ESFERA)
            .codigouc(UPDATED_CODIGOUC)
            .cnuc(UPDATED_CNUC)
            .nome(UPDATED_NOME)
            .ufs(UPDATED_UFS)
            .anocriacao(UPDATED_ANOCRIACAO)
            .atolegalcriacao(UPDATED_ATOLEGALCRIACAO)
            .outrosatoslegais(UPDATED_OUTROSATOSLEGAIS)
            .municipios(UPDATED_MUNICIPIOS)
            .templanomanejo(UPDATED_TEMPLANOMANEJO)
            .temconselhogestor(UPDATED_TEMCONSELHOGESTOR)
            .fontearea(UPDATED_FONTEAREA)
            .areabioma(UPDATED_AREABIOMA)
            .areaatolegalcriacao(UPDATED_AREAATOLEGALCRIACAO)
            .areaamazonia(UPDATED_AREAAMAZONIA)
            .areacaatinga(UPDATED_AREACAATINGA)
            .areacerrado(UPDATED_AREACERRADO)
            .areamataatlantica(UPDATED_AREAMATAATLANTICA)
            .areapampa(UPDATED_AREAPAMPA)
            .areapantanal(UPDATED_AREAPANTANAL)
            .areamarinha(UPDATED_AREAMARINHA)
            .biomaid(UPDATED_BIOMAID);

        restUnidadeConservacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedUnidadeConservacao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedUnidadeConservacao))
            )
            .andExpect(status().isOk());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
        UnidadeConservacao testUnidadeConservacao = unidadeConservacaoList.get(unidadeConservacaoList.size() - 1);
        assertThat(testUnidadeConservacao.getCategoriamanejoid()).isEqualTo(UPDATED_CATEGORIAMANEJOID);
        assertThat(testUnidadeConservacao.getCategoriaiucnid()).isEqualTo(UPDATED_CATEGORIAIUCNID);
        assertThat(testUnidadeConservacao.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testUnidadeConservacao.getTipoesfera()).isEqualTo(UPDATED_TIPOESFERA);
        assertThat(testUnidadeConservacao.getEsfera()).isEqualTo(UPDATED_ESFERA);
        assertThat(testUnidadeConservacao.getCodigouc()).isEqualTo(UPDATED_CODIGOUC);
        assertThat(testUnidadeConservacao.getCnuc()).isEqualTo(UPDATED_CNUC);
        assertThat(testUnidadeConservacao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testUnidadeConservacao.getUfs()).isEqualTo(UPDATED_UFS);
        assertThat(testUnidadeConservacao.getAnocriacao()).isEqualTo(UPDATED_ANOCRIACAO);
        assertThat(testUnidadeConservacao.getAtolegalcriacao()).isEqualTo(UPDATED_ATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getOutrosatoslegais()).isEqualTo(UPDATED_OUTROSATOSLEGAIS);
        assertThat(testUnidadeConservacao.getMunicipios()).isEqualTo(UPDATED_MUNICIPIOS);
        assertThat(testUnidadeConservacao.getTemplanomanejo()).isEqualTo(UPDATED_TEMPLANOMANEJO);
        assertThat(testUnidadeConservacao.getTemconselhogestor()).isEqualTo(UPDATED_TEMCONSELHOGESTOR);
        assertThat(testUnidadeConservacao.getFontearea()).isEqualTo(UPDATED_FONTEAREA);
        assertThat(testUnidadeConservacao.getAreabioma()).isEqualByComparingTo(UPDATED_AREABIOMA);
        assertThat(testUnidadeConservacao.getAreaatolegalcriacao()).isEqualByComparingTo(UPDATED_AREAATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getAreaamazonia()).isEqualByComparingTo(UPDATED_AREAAMAZONIA);
        assertThat(testUnidadeConservacao.getAreacaatinga()).isEqualByComparingTo(UPDATED_AREACAATINGA);
        assertThat(testUnidadeConservacao.getAreacerrado()).isEqualByComparingTo(UPDATED_AREACERRADO);
        assertThat(testUnidadeConservacao.getAreamataatlantica()).isEqualByComparingTo(UPDATED_AREAMATAATLANTICA);
        assertThat(testUnidadeConservacao.getAreapampa()).isEqualByComparingTo(UPDATED_AREAPAMPA);
        assertThat(testUnidadeConservacao.getAreapantanal()).isEqualByComparingTo(UPDATED_AREAPANTANAL);
        assertThat(testUnidadeConservacao.getAreamarinha()).isEqualByComparingTo(UPDATED_AREAMARINHA);
        assertThat(testUnidadeConservacao.getBiomaid()).isEqualTo(UPDATED_BIOMAID);
    }

    @Test
    @Transactional
    void putNonExistingUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, unidadeConservacao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUnidadeConservacaoWithPatch() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();

        // Update the unidadeConservacao using partial update
        UnidadeConservacao partialUpdatedUnidadeConservacao = new UnidadeConservacao();
        partialUpdatedUnidadeConservacao.setId(unidadeConservacao.getId());

        partialUpdatedUnidadeConservacao
            .categoriamanejoid(UPDATED_CATEGORIAMANEJOID)
            .categoriaiucnid(UPDATED_CATEGORIAIUCNID)
            .situacao(UPDATED_SITUACAO)
            .esfera(UPDATED_ESFERA)
            .codigouc(UPDATED_CODIGOUC)
            .anocriacao(UPDATED_ANOCRIACAO)
            .areabioma(UPDATED_AREABIOMA)
            .areaatolegalcriacao(UPDATED_AREAATOLEGALCRIACAO)
            .areaamazonia(UPDATED_AREAAMAZONIA)
            .areacaatinga(UPDATED_AREACAATINGA)
            .areacerrado(UPDATED_AREACERRADO)
            .areapantanal(UPDATED_AREAPANTANAL);

        restUnidadeConservacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUnidadeConservacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUnidadeConservacao))
            )
            .andExpect(status().isOk());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
        UnidadeConservacao testUnidadeConservacao = unidadeConservacaoList.get(unidadeConservacaoList.size() - 1);
        assertThat(testUnidadeConservacao.getCategoriamanejoid()).isEqualTo(UPDATED_CATEGORIAMANEJOID);
        assertThat(testUnidadeConservacao.getCategoriaiucnid()).isEqualTo(UPDATED_CATEGORIAIUCNID);
        assertThat(testUnidadeConservacao.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testUnidadeConservacao.getTipoesfera()).isEqualTo(DEFAULT_TIPOESFERA);
        assertThat(testUnidadeConservacao.getEsfera()).isEqualTo(UPDATED_ESFERA);
        assertThat(testUnidadeConservacao.getCodigouc()).isEqualTo(UPDATED_CODIGOUC);
        assertThat(testUnidadeConservacao.getCnuc()).isEqualTo(DEFAULT_CNUC);
        assertThat(testUnidadeConservacao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testUnidadeConservacao.getUfs()).isEqualTo(DEFAULT_UFS);
        assertThat(testUnidadeConservacao.getAnocriacao()).isEqualTo(UPDATED_ANOCRIACAO);
        assertThat(testUnidadeConservacao.getAtolegalcriacao()).isEqualTo(DEFAULT_ATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getOutrosatoslegais()).isEqualTo(DEFAULT_OUTROSATOSLEGAIS);
        assertThat(testUnidadeConservacao.getMunicipios()).isEqualTo(DEFAULT_MUNICIPIOS);
        assertThat(testUnidadeConservacao.getTemplanomanejo()).isEqualTo(DEFAULT_TEMPLANOMANEJO);
        assertThat(testUnidadeConservacao.getTemconselhogestor()).isEqualTo(DEFAULT_TEMCONSELHOGESTOR);
        assertThat(testUnidadeConservacao.getFontearea()).isEqualTo(DEFAULT_FONTEAREA);
        assertThat(testUnidadeConservacao.getAreabioma()).isEqualByComparingTo(UPDATED_AREABIOMA);
        assertThat(testUnidadeConservacao.getAreaatolegalcriacao()).isEqualByComparingTo(UPDATED_AREAATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getAreaamazonia()).isEqualByComparingTo(UPDATED_AREAAMAZONIA);
        assertThat(testUnidadeConservacao.getAreacaatinga()).isEqualByComparingTo(UPDATED_AREACAATINGA);
        assertThat(testUnidadeConservacao.getAreacerrado()).isEqualByComparingTo(UPDATED_AREACERRADO);
        assertThat(testUnidadeConservacao.getAreamataatlantica()).isEqualByComparingTo(DEFAULT_AREAMATAATLANTICA);
        assertThat(testUnidadeConservacao.getAreapampa()).isEqualByComparingTo(DEFAULT_AREAPAMPA);
        assertThat(testUnidadeConservacao.getAreapantanal()).isEqualByComparingTo(UPDATED_AREAPANTANAL);
        assertThat(testUnidadeConservacao.getAreamarinha()).isEqualByComparingTo(DEFAULT_AREAMARINHA);
        assertThat(testUnidadeConservacao.getBiomaid()).isEqualTo(DEFAULT_BIOMAID);
    }

    @Test
    @Transactional
    void fullUpdateUnidadeConservacaoWithPatch() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();

        // Update the unidadeConservacao using partial update
        UnidadeConservacao partialUpdatedUnidadeConservacao = new UnidadeConservacao();
        partialUpdatedUnidadeConservacao.setId(unidadeConservacao.getId());

        partialUpdatedUnidadeConservacao
            .categoriamanejoid(UPDATED_CATEGORIAMANEJOID)
            .categoriaiucnid(UPDATED_CATEGORIAIUCNID)
            .situacao(UPDATED_SITUACAO)
            .tipoesfera(UPDATED_TIPOESFERA)
            .esfera(UPDATED_ESFERA)
            .codigouc(UPDATED_CODIGOUC)
            .cnuc(UPDATED_CNUC)
            .nome(UPDATED_NOME)
            .ufs(UPDATED_UFS)
            .anocriacao(UPDATED_ANOCRIACAO)
            .atolegalcriacao(UPDATED_ATOLEGALCRIACAO)
            .outrosatoslegais(UPDATED_OUTROSATOSLEGAIS)
            .municipios(UPDATED_MUNICIPIOS)
            .templanomanejo(UPDATED_TEMPLANOMANEJO)
            .temconselhogestor(UPDATED_TEMCONSELHOGESTOR)
            .fontearea(UPDATED_FONTEAREA)
            .areabioma(UPDATED_AREABIOMA)
            .areaatolegalcriacao(UPDATED_AREAATOLEGALCRIACAO)
            .areaamazonia(UPDATED_AREAAMAZONIA)
            .areacaatinga(UPDATED_AREACAATINGA)
            .areacerrado(UPDATED_AREACERRADO)
            .areamataatlantica(UPDATED_AREAMATAATLANTICA)
            .areapampa(UPDATED_AREAPAMPA)
            .areapantanal(UPDATED_AREAPANTANAL)
            .areamarinha(UPDATED_AREAMARINHA)
            .biomaid(UPDATED_BIOMAID);

        restUnidadeConservacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUnidadeConservacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUnidadeConservacao))
            )
            .andExpect(status().isOk());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
        UnidadeConservacao testUnidadeConservacao = unidadeConservacaoList.get(unidadeConservacaoList.size() - 1);
        assertThat(testUnidadeConservacao.getCategoriamanejoid()).isEqualTo(UPDATED_CATEGORIAMANEJOID);
        assertThat(testUnidadeConservacao.getCategoriaiucnid()).isEqualTo(UPDATED_CATEGORIAIUCNID);
        assertThat(testUnidadeConservacao.getSituacao()).isEqualTo(UPDATED_SITUACAO);
        assertThat(testUnidadeConservacao.getTipoesfera()).isEqualTo(UPDATED_TIPOESFERA);
        assertThat(testUnidadeConservacao.getEsfera()).isEqualTo(UPDATED_ESFERA);
        assertThat(testUnidadeConservacao.getCodigouc()).isEqualTo(UPDATED_CODIGOUC);
        assertThat(testUnidadeConservacao.getCnuc()).isEqualTo(UPDATED_CNUC);
        assertThat(testUnidadeConservacao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testUnidadeConservacao.getUfs()).isEqualTo(UPDATED_UFS);
        assertThat(testUnidadeConservacao.getAnocriacao()).isEqualTo(UPDATED_ANOCRIACAO);
        assertThat(testUnidadeConservacao.getAtolegalcriacao()).isEqualTo(UPDATED_ATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getOutrosatoslegais()).isEqualTo(UPDATED_OUTROSATOSLEGAIS);
        assertThat(testUnidadeConservacao.getMunicipios()).isEqualTo(UPDATED_MUNICIPIOS);
        assertThat(testUnidadeConservacao.getTemplanomanejo()).isEqualTo(UPDATED_TEMPLANOMANEJO);
        assertThat(testUnidadeConservacao.getTemconselhogestor()).isEqualTo(UPDATED_TEMCONSELHOGESTOR);
        assertThat(testUnidadeConservacao.getFontearea()).isEqualTo(UPDATED_FONTEAREA);
        assertThat(testUnidadeConservacao.getAreabioma()).isEqualByComparingTo(UPDATED_AREABIOMA);
        assertThat(testUnidadeConservacao.getAreaatolegalcriacao()).isEqualByComparingTo(UPDATED_AREAATOLEGALCRIACAO);
        assertThat(testUnidadeConservacao.getAreaamazonia()).isEqualByComparingTo(UPDATED_AREAAMAZONIA);
        assertThat(testUnidadeConservacao.getAreacaatinga()).isEqualByComparingTo(UPDATED_AREACAATINGA);
        assertThat(testUnidadeConservacao.getAreacerrado()).isEqualByComparingTo(UPDATED_AREACERRADO);
        assertThat(testUnidadeConservacao.getAreamataatlantica()).isEqualByComparingTo(UPDATED_AREAMATAATLANTICA);
        assertThat(testUnidadeConservacao.getAreapampa()).isEqualByComparingTo(UPDATED_AREAPAMPA);
        assertThat(testUnidadeConservacao.getAreapantanal()).isEqualByComparingTo(UPDATED_AREAPANTANAL);
        assertThat(testUnidadeConservacao.getAreamarinha()).isEqualByComparingTo(UPDATED_AREAMARINHA);
        assertThat(testUnidadeConservacao.getBiomaid()).isEqualTo(UPDATED_BIOMAID);
    }

    @Test
    @Transactional
    void patchNonExistingUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, unidadeConservacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUnidadeConservacao() throws Exception {
        int databaseSizeBeforeUpdate = unidadeConservacaoRepository.findAll().size();
        unidadeConservacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUnidadeConservacaoMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(unidadeConservacao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UnidadeConservacao in the database
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUnidadeConservacao() throws Exception {
        // Initialize the database
        unidadeConservacaoRepository.saveAndFlush(unidadeConservacao);

        int databaseSizeBeforeDelete = unidadeConservacaoRepository.findAll().size();

        // Delete the unidadeConservacao
        restUnidadeConservacaoMockMvc
            .perform(delete(ENTITY_API_URL_ID, unidadeConservacao.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UnidadeConservacao> unidadeConservacaoList = unidadeConservacaoRepository.findAll();
        assertThat(unidadeConservacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
